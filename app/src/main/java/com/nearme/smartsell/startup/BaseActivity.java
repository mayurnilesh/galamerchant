package com.nearme.smartsell.startup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.location.MyLocationListener;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import static com.nearme.smartsell.startup.ManageSlotsActivity.login;

public class BaseActivity extends AppCompatActivity  {

    // Activity code here
    private PrefManager prefManager;
    public TextView CustIDTextView, TimeTextView;
    public ImageView imageView;
    public static String timeMSg = "";
    public static double latitude = 0.0;
    public static double longitude = 0.0;
    public static String phoneNumber = "";
    public static String name = "";
    public static String imageUrl = "";
    ImageView side_menu;
    Context curContext;
    Gson gson;
    boolean isEmp = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void FromMainActivity() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        prefManager = new PrefManager(this);
        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(v -> {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        String loginType = prefManager.getLoginType();

        CustIDTextView = navigationView.getHeaderView(0).findViewById(R.id.CustIDTextView);
        TimeTextView = navigationView.getHeaderView(0).findViewById(R.id.TimeTextView);
        imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        curContext = navigationView.getContext();
        gson = new Gson();
        login = Integer.valueOf(loginType);
        switch (loginType) {
            case "0":
                isEmp = false;
                break;
            case "1":
                isEmp = true;
                break;
        }
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {
            timeMSg = "Good Morning  ";
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            timeMSg = "Good Afternoon  ";
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            timeMSg = "Good Evening  ";
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            timeMSg = "Good Night  ";
        }
        TimeTextView.setText(timeMSg);

        if(prefManager.getMerchantID()!=null)
        CustIDTextView.setText(prefManager.getMerchantID());

        Picasso.with(curContext).load(prefManager.getMerchantLogoURI()).resize(100, 100)
                .centerInside().into(imageView);

        if (checkInternetOrGps()) {
            this.startService(new Intent(getApplicationContext(), MyLocationListener.class));
        }
    }

    public boolean checkInternetOrGps() {

        boolean isInternetandGPSStatusOK = false;
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            } else {
                isInternetandGPSStatusOK = true;
            }
        }
        return isInternetandGPSStatusOK;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }


}
