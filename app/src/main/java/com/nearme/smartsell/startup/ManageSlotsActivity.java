package com.nearme.smartsell.startup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.AnnouncementBizSendingFragment;
import com.nearme.smartsell.fragments.ApproveDealsFragment;
import com.nearme.smartsell.fragments.FragmentChat;
import com.nearme.smartsell.fragments.FragmentChatList;
import com.nearme.smartsell.fragments.FragmentContactUs;
import com.nearme.smartsell.fragments.FragmentCounter;
import com.nearme.smartsell.fragments.FragmentEvents;
import com.nearme.smartsell.fragments.FragmentFavourite;
import com.nearme.smartsell.fragments.FragmentManageSlots;
import com.nearme.smartsell.fragments.FragmentPrivacyPolicy;
import com.nearme.smartsell.fragments.FragmentQRCode;
import com.nearme.smartsell.fragments.FragmentTerms;
import com.nearme.smartsell.fragments.HistoryFragment;
import com.nearme.smartsell.fragments.NegotiationFragment;
import com.nearme.smartsell.language.LocaleManager;
import com.nearme.smartsell.menu.TokenEmpActivity;
import com.nearme.smartsell.menu.TokenHelperActivity;
import com.nearme.smartsell.menu.TokenMerchantActivity;
import com.nearme.smartsell.utility.Constant;

public class ManageSlotsActivity extends BaseActivity {

    public static Integer login = 0;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.local_english:
                setNewLocale(this, LocaleManager.ENGLISH);
                return true;
            case R.id.local_hindi:
                setNewLocale(this, LocaleManager.HINDI);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_slots);

        Toolbar toolBar = findViewById(R.id.toolBar);
        toolBar.inflateMenu(R.menu.main);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        FromMainActivity();
        if (login == 1) {
            startActivity(new Intent(ManageSlotsActivity.this, TokenEmpActivity.class));
        } else if (login == 2) {
            startActivity(new Intent(ManageSlotsActivity.this, TokenHelperActivity.class));
        } else if (login == 3) {
            startActivity(new Intent(ManageSlotsActivity.this, MerchantCounterViewerActivity.class));
        }

        SharedPreferences prefs = getSharedPreferences(Constant.appPref, 0);
        boolean isFirstLaunch = prefs.getBoolean(Constant.isFirst, true);
        if (isFirstLaunch) {
            changeFragment(new FragmentManageSlots());
            showFavouriteDialog();
        } else {
            int fav = prefs.getInt(Constant.favType, 1);
            switch (fav) {
                case 0:
                    changeFragment(new AnnouncementBizSendingFragment());
                    break;
                case 2:
                    changeFragment(new NegotiationFragment());

                    break;
                case 3:
                    changeFragment(new FragmentCounter());

                    break;
                case 4:
                    changeFragment(new FragmentChatList());
                    break;
                default:
                    changeFragment(new FragmentManageSlots());

            }
        }

        navigationSetup();

        toolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.local_english:
                        setNewLocale(ManageSlotsActivity.this, LocaleManager.ENGLISH);
                        return true;
                    case R.id.local_hindi:
                        setNewLocale(ManageSlotsActivity.this, LocaleManager.HINDI);
                        return true;

                }

                return false;
            }
        });

    }

    private void navigationSetup() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.terms_conditions) {
                    changeFragment(new FragmentTerms());
                } else if (id == R.id.qr_code) {
                    changeFragment(new FragmentQRCode());
                }else if (id == R.id.events) {
                    changeFragment(new FragmentEvents());
                } else if (id == R.id.history) {
                    changeFragment(new HistoryFragment());
                } else if (id == R.id.negotiation) {
                    changeFragment(new NegotiationFragment());
                } else if (id == R.id.approved) {
                    changeFragment(new ApproveDealsFragment());
                } else if (id == R.id.manage_slots) {
                    changeFragment(new FragmentManageSlots());
                } else if (id == R.id.chat) {
                    changeFragment(new FragmentChatList());
                } else if (id == R.id.home) {
                    changeFragment(new AnnouncementBizSendingFragment());
                } else if (id == R.id.privacy_policy) {
                    changeFragment(new FragmentPrivacyPolicy());
                } else if (id == R.id.contact_us) {
                    changeFragment(new FragmentContactUs());
                } else if (id == R.id.favourite) {
                    changeFragment(new FragmentFavourite());
                } else if (id == R.id.token_gen) {
                    if (login == 1) {
                        startActivity(new Intent(ManageSlotsActivity.this, TokenEmpActivity.class));
                    } else if (login == 2) {
                        startActivity(new Intent(ManageSlotsActivity.this, TokenHelperActivity.class));
                    } else if (login == 3) {
                        startActivity(new Intent(ManageSlotsActivity.this, MerchantCounterViewerActivity.class));
                    } else {
                        changeFragment(new FragmentCounter());
                    }
                }

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void changeFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.rlContainer, fragment);
        ft.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 25) {
            AnnouncementBizSendingFragment announcementBizSendingFragment = (AnnouncementBizSendingFragment) getSupportFragmentManager().findFragmentById(R.id.rlContainer);
            if (announcementBizSendingFragment != null) {
                announcementBizSendingFragment.setLocation(data);
            }
        }
    }

    public void getLocation() {
        Intent in = new Intent(this, MapsAdsActivity.class);
        startActivityForResult(in, 25);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    private void showFavouriteDialog() {

        SharedPreferences prefs = getSharedPreferences(Constant.appPref, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constant.isFirst, false).apply();

        final Dialog dialog = new Dialog(this, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialogue_favourite_info);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        RadioGroup radioGroup = dialog.findViewById(R.id.rgFavourite);
        TextView tvSubmit = dialog.findViewById(R.id.submitBtn);

        tvSubmit.setOnClickListener(v -> {
            RadioButton checkedRadioButton = dialog.findViewById(radioGroup.getCheckedRadioButtonId());
            int idx = radioGroup.indexOfChild(checkedRadioButton);
            Log.d("TAG", "onClick: " + idx);
            editor.putInt(Constant.favType, idx).apply();
            dialog.dismiss();
        });

    }

}
