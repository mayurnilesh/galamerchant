package com.nearme.smartsell.startup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.Cloudinary;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.firebasedata.SharedPreferenceHelper;
import com.nearme.smartsell.location.MyLocationListener;
import com.nearme.smartsell.model.User;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.MerchantPayload;
import com.nearme.smartsell.utility.AuthUtils;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopikomanduri on 05/05/18.
 * modified by sirisha samudrala on 21/07/2018.
 */

public class PhoneActivity extends Activity implements View.OnClickListener {

    OtpTextView otpTextView;
    public static String enteredName = "";
    public static Uri enteredImageUri = null;
    public static Uri enteredIDProofUri = null;
    public static String enteredImageUristrng = null;
    public static String enteredIDProofUristrng = null;
    public static String enteredNumber = "";
    public static String enteredAadhar = "";
    public static String enteredShopno = "";
    public static String enteredLocality = "";
    public static String enteredArea = "";
    public static String enteredLandmark = "";
    public static String enteredCity = "";
    public static String enteredState = "";
    public static double latitude = 0.0;
    public static double longitude = 0.0;
    public static String password;
    public static String category;
    public static Integer role;
    public static PhoneActivity myphoneActivity;
    public Cloudinary cloudinary = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");
    public AlertDialog.Builder builder;

    Button  mResendButton;
    TextView mVerifyButton;

    //  public LovelyProgressDialog waitingDialog;
    public FirebaseAuth.AuthStateListener mAuthListener;
    public static FirebaseUser user;


    private FirebaseAuth mAuth;
    private boolean isAddingOtherStore = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationId;
    private AuthUtils authUtils = new AuthUtils();
    private static final String TAG = "PhoneActivity";
    public static PhoneAuthCredential credential = null;
    public static PhoneActivity currentObj;
    public static Context baseCtx;
    TextView usrTXtview;
    public static ProgressBar waitingDialog;
    private static String countryname;
    private static String geoHash = "";
    private PrefManager prefManager;
    ImageView side_menu;
    TextView counttime,tvContact;

    CountDownTimer cdt;
    //change
    public int counter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nearme.smartsell.R.layout.activity_verifycontact);

        baseCtx = getBaseContext();
        checkInternetOrGps();
        FirebaseApp.initializeApp(baseCtx);
        builder = new AlertDialog.Builder(this);
        prefManager = new PrefManager(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isAddingOtherStore = true;
            enteredNumber = extras.getString("PhoneNumber");
            enteredName = extras.getString("Name");
        }


        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        counttime = findViewById(R.id.counttime);
        counttime.setText("02:00");

        otpTextView = findViewById(R.id.otp_view);
        tvContact = findViewById(R.id.tvContact);
        mVerifyButton = findViewById(R.id.button_verify_phone);
        mResendButton = findViewById(R.id.button_resend);
        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);
        mVerifyButton.setEnabled(false);
        mResendButton.setEnabled(false);
        authUtils.curActivity = this;

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        mAuth = FirebaseAuth.getInstance();
        waitingDialog = findViewById(R.id.progressBar1);
        waitingDialog.setVisibility(View.INVISIBLE);
        mResendButton.setEnabled(false);
        mResendButton.setVisibility(View.INVISIBLE);
        final String num = SharedPreferenceHelper.getInstance(baseCtx).getusercontact();
        try {
            List<userdetails> temp = userdetails.listAll(userdetails.class);

            if (num.length() > 0 && temp.size() > 0 && PhoneActivity.currentObj != null) {
                PhoneActivity.currentObj.startActivity(new Intent(PhoneActivity.currentObj, ManageSlotsActivity.class));
                PhoneActivity.currentObj.finish();
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Table is not there", Toast.LENGTH_LONG).show();
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                    }
                });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                if (cdt != null)
                    cdt.cancel();
                Log.d(TAG, "onVerificationCompleted:" + credential);
                PhoneActivity.credential = credential;
                counttime.setVisibility(View.INVISIBLE);
                registerUserDetails(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (cdt != null)
                    cdt.start();
                else {
                    cdt = new CountDownTimer(120000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String time = "";
                            long lefttmr = millisUntilFinished / 1000;
                            if (lefttmr > 60) {
                                time = "Resend in 01:" + String.valueOf(lefttmr - 61);
                                mResendButton.setEnabled(false);
                                mResendButton.setVisibility(View.INVISIBLE);
                            } else {
                                time = "Resend in 00:" + String.valueOf(lefttmr - 1);
                                mResendButton.setEnabled(false);
                                mResendButton.setVisibility(View.INVISIBLE);
                                if (lefttmr < 1) {
                                    time = "Resend in 00:00";
                                    mResendButton.setEnabled(true);
                                    mResendButton.setVisibility(View.VISIBLE);
                                }
                            }
                            counttime.setText(String.valueOf(time));
                            //counter++;

                        }

                        @Override
                        public void onFinish() {
                            mResendButton.setVisibility(View.VISIBLE);
                        }
                    }.start();
                }
                counttime.setVisibility(View.VISIBLE);
                currentObj.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();

//                mResendButton.setEnabled(true);
//                mResendButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {

                currentObj.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                waitingDialog.setVisibility(View.GONE);
                // Toast.makeText(getApplicationContext(),"oncodesent",Toast.LENGTH_LONG).show();

                Log.d(TAG, "onCodeSent:" + verificationId);
                cdt = new CountDownTimer(120000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        String time = "";
                        long lefttmr = millisUntilFinished / 1000;
                        if (lefttmr >= 60) {
                            time = "Resend in 01:" + String.valueOf(lefttmr - 61);
                            mResendButton.setEnabled(true);
                            mResendButton.setVisibility(View.INVISIBLE);
                        } else {
                            time = "Resend in 00:" + String.valueOf(lefttmr - 1);
                            mResendButton.setEnabled(false);
                            mResendButton.setVisibility(View.INVISIBLE);
                            if (lefttmr < 1) {
                                time = "Resend in 00:00";
                                mResendButton.setEnabled(true);
                                mResendButton.setVisibility(View.VISIBLE);
                                cdt.cancel();
                            }
                        }
                        counttime.setText(String.valueOf(time));
                        //counter++;

                    }

                    @Override
                    public void onFinish() {
                        mResendButton.setVisibility(View.VISIBLE);
                    }
                }.start();

                mVerificationId = verificationId;
                mResendToken = token;
                mVerifyButton.setEnabled(true);
            }
        };
        if (enteredNumber.equals("") == false)
            startPhoneNumberVerification(enteredNumber);
        authUtils.initFirebase();
        currentObj = this;
        baseCtx = getBaseContext();

        this.startService(new Intent(baseCtx, MyLocationListener.class));

        myphoneActivity = this;

        tvContact.setText(enteredNumber);

    }

    private void checkInternetOrGps() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            }
        }

    }


    private void registerUserDetails(PhoneAuthCredential credential) {
        authUtils.waitingDialog = waitingDialog;
        authUtils.curActivity = this;

        signInWithPhoneAuthCredential(credential);

        authUtils.createUser(enteredNumber + "@smartsell.com", enteredNumber, mAuth, waitingDialog, PhoneActivity.this);
        mAuth.signInWithCredential(EmailAuthProvider
                .getCredential(enteredNumber + "@smartsell.com",enteredNumber))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: worked");
                        } else {
                            Log.d(TAG, "onComplete: not worked");
                            Toast.makeText(PhoneActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            waitingDialog.setVisibility(View.INVISIBLE);
                            currentObj.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            authUtils.uploadUserImg(PhoneActivity.this);
                            prefManager.setMerchantFireUID(mAuth.getCurrentUser().getUid());
                            if (isAddingOtherStore && userdetails.listAll(userdetails.class).size() > 0) {
                                userdetails curDetails = new userdetails();//.listAll(userdetails.class).get(0);
                                userdetails exUserDetails = userdetails.listAll(userdetails.class).get(0);
                                curDetails.ImageUrl = exUserDetails.ImageUrl;
                                curDetails.Aadhaar = exUserDetails.Aadhaar;
                                curDetails.Shopno = exUserDetails.Shopno;
                                curDetails.Locality = exUserDetails.Locality;
                                curDetails.Area = exUserDetails.Area;
                                curDetails.Landmark = exUserDetails.Landmark;
                                curDetails.ContactNumber = enteredNumber;
                                curDetails.Name = enteredName;
                                curDetails.save();
                            }


                        } else {
                            Toast.makeText(getApplicationContext(), task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(PhoneActivity.this, "Invalid code", Toast.LENGTH_SHORT).show();
                                mVerifyButton.setEnabled(true);
                                mResendButton.setEnabled(true);

                                currentObj.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                waitingDialog.setVisibility(View.GONE);
                                mVerifyButton.setVisibility(View.VISIBLE);
                                mResendButton.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
        authUtils.createUser(enteredNumber + "@smartsell.com", enteredNumber, mAuth, waitingDialog, PhoneActivity.this);
        mAuth.signInWithCredential(EmailAuthProvider
                .getCredential(enteredNumber + "@smartsell.com",enteredNumber))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: worked");
                        } else {
                            Log.d(TAG, "onComplete: not worked");
                            Toast.makeText(PhoneActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser;

        if ((mAuth != null) && (currentUser = mAuth.getCurrentUser()) != null) {

            try {
                if (!isAddingOtherStore) {
                    List<userdetails> temp = userdetails.listAll(userdetails.class);
                    if (temp.size() > 0 && (mAuth != null && (currentUser = mAuth.getCurrentUser()) != null)) {
                        startActivity(new Intent(PhoneActivity.this, ManageSlotsActivity.class));
                        finish();
                    } else {
                        currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
//                            if (task.isSuccessful()) {
//                                startActivity(new Intent(PhoneActivity.this, UserDetailsActivity.class));
//                                finish();                            }
                            }
                        });

                    }
                }
            } catch (Exception ex) {

            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_verify_phone:
                waitingDialog.setVisibility(View.INVISIBLE);
                String code = otpTextView.getOtp();
                if (TextUtils.isEmpty(otpTextView.getOtp())) {
                    Toast.makeText(myphoneActivity, "Cannot be empty.", Toast.LENGTH_SHORT).show();
                    //  return;
                } else {
                    mVerifyButton.setVisibility(View.GONE);
                    mResendButton.setVisibility(View.GONE);
                    //  spinner.setVisibility(View.VISIBLE);
                    verifyPhoneNumberWithCode(mVerificationId, code);
                }

                break;
            case R.id.button_resend:
                mVerifyButton.setEnabled(false);
                mResendButton.setEnabled(false);
                resendVerificationCode(enteredNumber, mResendToken);
                break;
        }

    }

    public void clickResend(View view) {
        resendVerificationCode(enteredNumber, mResendToken);
    }


    public void initNewUserInfo(String mImageUrl) {
        if (user == null) {
            user = mAuth.getCurrentUser();
        }
        //else
        {
            User newUser = new User();

            newUser.contact = enteredNumber + "@nearme.com";
            newUser.name = enteredName;
            newUser.avata = mImageUrl;

            Gson gson = new Gson();
            Geocoder geocoder;
            Locale lc = Locale.getDefault();
            try {
                geocoder = new Geocoder(this, lc);
//            latitude = MyLocationListener.mCurrentLocation.getLatitude();
//
//            longitude = MyLocationListener.mCurrentLocation.getLongitude();
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    if (enteredArea.trim().length() == 0)
                        enteredArea = address.getAddressLine(0);
                    if (enteredLocality.trim().length() == 0)
                        enteredLocality = address.getLocality();

                    geoHash = com.nearme.smartsell.geohashutil.GeoHash.fromCoordinates(latitude, longitude, 5).toString();

                    countryname = address.getCountryName();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            final userdetails obj = new userdetails(enteredNumber, enteredName, PhoneActivity.enteredImageUristrng, enteredShopno, enteredAadhar, enteredLocality, enteredArea, enteredLandmark,
                    enteredCity, enteredState, String.valueOf(prefManager.getSelectedCategory()), geoHash, password, 0);

            MerchantPayload merobj = new MerchantPayload(enteredName, enteredNumber, "", enteredLocality, enteredLandmark, enteredShopno,
                    latitude, longitude, geoHash, "", String.valueOf(prefManager.getSelectedCategory()), PhoneActivity.enteredImageUristrng, enteredArea, enteredState,
                    countryname, enteredCity, password, 0, enteredAadhar, PhoneActivity.enteredIDProofUristrng);
            prefManager.setCategorySelected(true);
            String jsonConvertedlastReceivedAdStructList = gson.toJson(merobj, MerchantPayload.class);

            if (!prefManager.getPostedtomain()) {
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);

                Call<MerchantPayload> call = apiService.RegMerchant(jsonConvertedlastReceivedAdStructList);
                call.enqueue(new Callback<MerchantPayload>() {
                    @Override
                    public void onResponse(Call<MerchantPayload> call, Response<MerchantPayload> response) {
                        String adsReceived = response.body().toString();
                        Log.d(TAG, "nearme .. onResponse: " + adsReceived);
                        MerchantPayload regRes = response.body();

                        if (regRes.merchantId.equalsIgnoreCase("-1")) {
                            Toast.makeText(PhoneActivity.this, "This number is already registered", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        } else if (regRes.merchantId.equalsIgnoreCase("-2")) {
                            Toast.makeText(PhoneActivity.this, "Registration falied! Please try again later.", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        } else {
                            obj.merchantId = regRes.merchantId;
                            prefManager.setPostedToMain(true);
                            prefManager.setMerchantID(obj.merchantId);
                            obj.save();
                            callCategory(obj.merchantId);
                        }
                    }

                    @Override
                    public void onFailure(Call<MerchantPayload> call, Throwable t) {
                        Toast.makeText(currentObj, "Failedin phoneactivity . reson is " + t.getMessage(), Toast.LENGTH_LONG).show();
                        // Log error here since request failed
                        Log.e(TAG, t.toString());
                        startActivity(new Intent(PhoneActivity.this, UserDetailsActivity.class));
                        finish();
                    }
                });
            }

        }
    }

    private void callCategory(String mid) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> call = apiService.registerMyCategories(category, mid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "onResponse: pass");
                startActivity(new Intent(PhoneActivity.this, ManageSlotsActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailureConstan: " + t.getMessage());
                startActivity(new Intent(PhoneActivity.this, ManageSlotsActivity.class));
                finish();
            }
        });


    }
}

