package com.nearme.smartsell.startup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;

public class WelcomeActivity extends AppCompatActivity {
    private LinearLayout dotsLayout;
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);


        launchHomeScreen();
    }


    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        if (!prefManager.isCategorySelected()) {
            startActivity(new Intent(WelcomeActivity.this, CategorySelectionActivity.class));
        } else {
            startActivity(new Intent(WelcomeActivity.this, UserDetailsActivity.class));
        }
        finish();
    }

}
