package com.nearme.smartsell.startup;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.BillDataGenAdapter;
import com.nearme.smartsell.adapter.BillDataSerchGenAdapter;
import com.nearme.smartsell.db.billingitem;
import com.nearme.smartsell.menu.PdfViewActivity;
import com.nearme.smartsell.model.Billdata;
import com.nearme.smartsell.utility.ImageUploader;
import com.nearme.smartsell.utility.PrefManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import android.media.Image;

public class BillGenerationForItemsActivity extends AppCompatActivity {


    private PrefManager prefManager;
    FloatingActionButton adddata, savedata, sharedata;
    String TAG = "AddBillsItemActivity";
    RecyclerView recbillitem, recbillsearchviewitem;
    ArrayList<Billdata> billdataArrayList = new ArrayList<>();
    ArrayList<Billdata> searchdataArrayList = new ArrayList<>();
    ArrayList<Billdata> tmpArray = new ArrayList<>();
    BillDataGenAdapter apercuAdapter;
    BillDataSerchGenAdapter searchAdapter;
    EditText search;
    Integer quantityCal = 0;
    TextView tvsubmit;
    TextView incsubmit;
    TextView decsubmit;
    TextView quansubmit;
    Bundle bundle;
    Context context;
    Toolbar mToolbar;
    Cloudinary cloudinary = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");
    String PDF_FILE = null;
    boolean isPDFCreated = false;
    Activity curActivity;
    String billAmt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_billitem_layout);
        prefManager = new PrefManager(this);
        bundle = getIntent().getExtras();
        context = this.getApplicationContext();
        curActivity = this;
        initialization();
        clicks();
        mToolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(mToolbar);
        // mToolbar.setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_main, menu);

        MenuItem mSearch = menu.findItem(R.id.action_search);

        final SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                apercuAdapter.filter(newText);
                billdataArrayList.clear();
                if (newText.length() >= 1) {
                    //
                    for (int i = 0; i < tmpArray.size(); i++) {
                        if (tmpArray.get(i).getItemid().contains(newText) || tmpArray.get(i).getItemname().contains(newText)) {
                            billdataArrayList.add(tmpArray.get(i));
                        }
                    }
                } else {
                }
                Log.d(TAG, "onTextChanged: " + billdataArrayList.size());
                apercuAdapter.notifyDataSetChanged();
//                mSearch.sett
//                mSearchView.clea
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void clicks() {
        adddata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(BillGenerationForItemsActivity.this, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_add_bill_item_checkout);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

                tvsubmit = dialog.findViewById(R.id.tvsubmit);
                incsubmit = dialog.findViewById(R.id.increase);
                decsubmit = dialog.findViewById(R.id.decrease);
                quansubmit = dialog.findViewById(R.id.integer_number);
                final EditText editname = dialog.findViewById(R.id.editname);
                final EditText editprice = dialog.findViewById(R.id.editprice);
                final EditText editid = dialog.findViewById(R.id.editid);
                final EditText editdesc = dialog.findViewById(R.id.editdesc);
                if (quantityCal == 0) {
                    decsubmit.setEnabled(false);
                } else {
                    decsubmit.setEnabled(true);
                }
                incsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal >= 0) {
                            quantityCal++;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 0) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });

                decsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal > 0) {
                            quantityCal--;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 0) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });

                tvsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkEditText(editname, "Enter product name") && checkEditText(editprice, "Enter product price")
                                && checkEditText(editid, "Enter product id") && checkEditText(editdesc, "Enter product description")) {
                            dialog.dismiss();

                            billingitem bill = new billingitem(prefManager.getMerchantID(), editid.getText().toString()
                                    , editname.getText().toString(), editprice.getText().toString(), editdesc.getText().toString(), quantityCal, "", "");

                            long value = bill.save();
                            Log.d(TAG, "onClick: " + value);

                            Billdata billdata = new Billdata();
                            billdata.setItemdesc(editdesc.getText().toString());
                            billdata.setItemid(editid.getText().toString());
                            billdata.setItemname(editname.getText().toString());
                            billdata.setItemprice(editprice.getText().toString());
                            billdata.setQuantity(quantityCal.toString());
                            billdataArrayList.add(billdata);
                            tmpArray.add(billdata);

                            apercuAdapter.notifyDataSetChanged();
                        }
                    }
                });

            }
        });

        savedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (searchdataArrayList.size() > 0) {
                        try {
                            saveLayoutAsPDFToSdcard();
                            savedata.setVisibility(View.VISIBLE);
                            sharedata.setEnabled(true);
//                            savedata.setBackgroundResource(R.drawable.ic_share_black_24dp);
                            isPDFCreated = true;
                        } catch (Exception ex) {
//                            savedata.setBackgroundResource(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_save, null));
                        }
                    } else {
                        Toast.makeText(context, "Please at least one Product For Billing", Toast.LENGTH_LONG);
                    }
                } catch (Exception ex) {
                    Log.v("Chaitu", "====saved==");
                }
            }
        });

        sharedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (isPDFCreated) {
                        try {
//                            Bitmap bm = viewToBitmap(vv);
//                            String name = "Gala" + System.currentTimeMillis() + ".png";
                            try {
//                                FileOutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + name);
//                                bm.compress(Bitmap.CompressFormat.PNG, 100, output);
//                                output.close();
                                savedata.setVisibility(View.VISIBLE);
                                sharedata.setVisibility(View.VISIBLE);
                                uploadToCloudinary(getImageUri(context));

                                Toast.makeText(context, "Bill Shared to Customer successfully", Toast.LENGTH_SHORT).show();
//                                getActivity().finish();
                            } catch (Exception e) {
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();

                                e.printStackTrace();
                            }
                        } catch (Exception ex) {
//                            savedata.setBackgroundResource(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_save, null));
                        }
                    } else {
                        Toast.makeText(context, "Please Create Bill to Share", Toast.LENGTH_LONG);
                    }
                } catch (Exception ex) {
                    Log.v("Chaitu", "====saved==");
                }
            }
        });
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                billdataArrayList.clear();
                if (s.length() >= 1) {
                    //
                    for (int i = 0; i < tmpArray.size(); i++) {
                        if (tmpArray.get(i).getItemid().contains(s) || tmpArray.get(i).getItemname().contains(s)) {
                            billdataArrayList.add(tmpArray.get(i));
                        }
                    }
                } else {
                }
                Log.d(TAG, "onTextChanged: " + billdataArrayList.size());
                apercuAdapter.notifyDataSetChanged();
            }
        });
    }

    private Uri getImageUri(Context context) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = PDF_FILE;//MediaStore.Files..insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void uploadToCloudinary(Uri imageUri) {
        String contactToRecv = bundle.getString("contactnumber");
        String billNo = bundle.getString("billnumber");

        Object[] params = new Object[9];
        params[0] = cloudinary;
        params[1] = imageUri;
        params[2] = ObjectUtils.emptyMap();
        params[3] = curActivity;
        params[4] = 2; // condition typeReceived 1 for bills
        params[5] = prefManager.getMerchantID();
        params[6] = contactToRecv;
        params[7] = billNo;
        if (billAmt != null) {
            params[8] = billAmt;
        } else {
            params[8] = "0";
        }
        new ImageUploader(BillGenerationForItemsActivity.this).execute(params);
    }

    public static String getSdCardPath() {
        //return Environment.DIRECTORY_DOWNLOADS + "/";
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        //return App.getApp().getApplicationContext().getFilesDir().getAbsolutePath()
    }

    public PdfPTable createFirstTable(String discount, boolean isPercent) {


        // a table with three columns
        PdfPTable table = new PdfPTable(5);
        // the cell object
        PdfPCell cell;

        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("Product ID"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Product Name"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
//        cell = new PdfPCell(new Phrase("Stock"));
//        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Quantity"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Price per Unit"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total Price"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        float allItemsPrice = 0;
        float totalPrice = 0;
        for (int i = 0; i < searchdataArrayList.size(); i++) {
            if (searchdataArrayList.get(i).getQuantity() == null) {
                searchdataArrayList.get(i).setQuantity("1");
            }
            table.addCell(searchdataArrayList.get(i).getItemid());
            table.addCell(searchdataArrayList.get(i).getItemname());
            table.addCell(searchdataArrayList.get(i).getQuantity());
            table.addCell(String.valueOf(searchdataArrayList.get(i).getItemprice()) + "/-");

            totalPrice = Integer.parseInt(searchdataArrayList.get(i).getItemprice()) * Integer.parseInt(searchdataArrayList.get(i).getQuantity());
            table.addCell(String.valueOf(totalPrice) + "/-");

            allItemsPrice += totalPrice;
        }

        if (isPercent) {
            cell = new PdfPCell(new Phrase("Discount: " + discount + "%"));//0
        } else {
            cell = new PdfPCell(new Phrase("Discount: " + discount + "/-"));//0
        }

        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(" "));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        table.addCell(cell);
        float discValue = 0;
        if (isPercent) {
            discValue = (allItemsPrice * Integer.parseInt(discount)) / 100;
        } else {
            discValue = Float.parseFloat(discount);
        }
        cell = new PdfPCell(new Phrase(String.valueOf(discValue) + "/-"));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total Rs: "));//0
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(" "));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(String.valueOf(allItemsPrice - discValue) + "/-"));//3
        billAmt = String.valueOf(allItemsPrice - discValue);
        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setColspan(2);
        table.addCell(cell);

        return table;
    }

    private void saveLayoutAsPDFToSdcard() {
        PDF_FILE = null;
        try {
            Document document = new Document();
            String path = getSdCardPath();
            String curdate = new SimpleDateFormat("HH-ss").format(Calendar.getInstance().getTime());
            PDF_FILE = path + "GalaInvoice" + curdate + ".pdf";
            PdfWriter.getInstance(document, new FileOutputStream(PDF_FILE));
            document.open();
            String discount = bundle.getString("discount");
            boolean isDiscount = bundle.getBoolean("isDiscount");
            try {
                String contactToRecv = bundle.getString("contactnumber");
                String billNo = bundle.getString("billnumber");
                String address = bundle.getString("address");
                String billfromdate = bundle.getString("billdate");
                String billtodate = bundle.getString("billdateto");
                String bamount = bundle.getString("ammount");
                String cname = bundle.getString("customername");
                String cid = bundle.getString("customerid");
                String mname = bundle.getString("merchantname");
                String filename = prefManager.getMerchantLogoURI();
                Image image = Image.getInstance(filename);
                image.scaleToFit(50f, 50f);
                //document.add(image);
                //add spacing document.add("");
                PdfPTable headerTable = new PdfPTable(4);
//logo merchantname
//address
//billNo    Date
                // the cell object
                PdfPCell headercell;
                // we add a cell with colspan 3
                headercell = new PdfPCell(image);//0
                headercell.setColspan(2);
                headercell.setBorder(Rectangle.NO_BORDER);
                headerTable.addCell(headercell);

                Phrase pp = new Phrase(mname);
                Font f = new Font();
                f.setColor(BaseColor.GREEN);
                f.setSize(30f);
                f.setFamily("Arial");
                f.setStyle(Font.BOLD);
                pp.setFont(f);
                headercell = new PdfPCell(pp);//3,4
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase("Gst In: AHJNC54861166"));//0
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase(" "));//3
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);


                headercell = new PdfPCell(new Phrase(address));
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(4);//0
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase("Bill No: " + billNo));//0
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase("Validity Period: " + billfromdate + " to " + billtodate));//3
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase("Customer Id: " + cid));//0
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);

                headercell = new PdfPCell(new Phrase("Customer Name: " + cname));//3
                headercell.setBorder(Rectangle.NO_BORDER);
                headercell.setColspan(2);
                headerTable.addCell(headercell);
                //document.add(headerTable);


                document.add(headerTable);

                PdfPCell myCell = new PdfPCell(new Paragraph("   "));
                myCell.setBorder(Rectangle.BOTTOM);
                document.add(myCell);
                myCell = new PdfPCell(new Paragraph("   "));
                document.add(myCell);
                document.add(Chunk.NEWLINE);
                document.add(Chunk.NEWLINE);

            } catch (Exception mex) {

            }

            PdfPTable table = createFirstTable(discount, isDiscount);
            table.setSpacingBefore(20f);
            document.add(table);
            document.close();

            Log.v("Chaitu", "====saved==");
            //to open the saved file
            Toast.makeText(this, "Congrats GalaInvoice is Saved to PDF", Toast.LENGTH_SHORT).show();
            File filer = new File(path + "GalaInvoice" + curdate + ".pdf");
            Intent target = new Intent(Intent.ACTION_VIEW);

            target.setData(Uri.fromFile(filer));
//            target.setType("browser");

//            target.setDataAndType(Uri.fromFile(filer), "application/browser");
            PDF_FILE = filer.getAbsolutePath();
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);


            try {
//                startActivity(intent);
                Intent intent = new Intent(BillGenerationForItemsActivity.this, PdfViewActivity.class);
                intent.putExtra("PDF_FILE", Uri.fromFile(filer).toString());
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            Log.v("Chaitu", "====failed==107" + e.toString());
            e.printStackTrace();
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void initialization() {
        adddata = findViewById(R.id.adddata);
        sharedata = findViewById(R.id.sharedata);
        savedata = findViewById(R.id.savedata);
        recbillitem = findViewById(R.id.recbillitem);
        search = findViewById(R.id.search);
        recbillsearchviewitem = findViewById(R.id.recbillsearchviewitem);
        adddata.setVisibility(View.INVISIBLE);

        recbillsearchviewitem.setVisibility(View.VISIBLE);

        List<billingitem> billingitemList = billingitem.listAll(billingitem.class);
        Log.d(TAG, "initialization: " + billingitemList.size());

        for (int i = 0; i < billingitemList.size(); i++) {
            Billdata billdata = new Billdata();
            billdata.setItemdesc(billingitemList.get(i).itemdesc);
            billdata.setItemid(billingitemList.get(i).itemid);
            billdata.setItemname(billingitemList.get(i).itemname);
            billdata.setItemprice(billingitemList.get(i).itemprice);
            billdata.setQuantity(billingitemList.get(i).itemQuantity.toString());
//            billdataArrayList.add(billdata);
            tmpArray.add(billdata);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recbillitem.setLayoutManager(linearLayoutManager);
        recbillitem.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        recbillsearchviewitem.setLayoutManager(linearLayoutManager1);
        recbillsearchviewitem.setItemAnimator(new DefaultItemAnimator());

        searchAdapter = new BillDataSerchGenAdapter(searchdataArrayList, new BillDataSerchGenAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Billdata item, View v) {

            }
        });
        recbillsearchviewitem.setAdapter(searchAdapter);

        apercuAdapter = new BillDataGenAdapter(billdataArrayList, new BillDataGenAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Billdata item, View v) {

                for (int i = 0; i < tmpArray.size(); i++) {
                    if (tmpArray.get(i).getItemid().equalsIgnoreCase(item.getItemid())) {
                        tmpArray.get(i).setStatus(true);
                        break;
                    }
                }

                searchdataArrayList.add(item);
                searchAdapter.notifyDataSetChanged();
                search.setText("");

            }
        });
        recbillitem.setAdapter(apercuAdapter);

    }

    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equalsIgnoreCase("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }


}
