package com.nearme.smartsell.startup;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.nearme.smartsell.R;

public class BillItemSingleView extends Activity {
    // Declare Variables
    TextView txId;
    TextView txtmID;
    TextView txtImgUrl;
    String Id;
    String MID;
    String imgUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bills_history_listview__item_layout);
        // Retrieve data from MainActivity on item click event
        Intent i = getIntent();
        // Get the results of rank
        Id = i.getStringExtra("Id");
        // Get the results of country
        MID = i.getStringExtra("MerchantReciptID");
        // Get the results of population
        imgUrl = i.getStringExtra("ImgUrl");

        // Locate the TextViews in singleitemview.xml
        txId = findViewById(R.id.idlabel);
        txtmID = findViewById(R.id.mIdLbl);
        txtImgUrl = findViewById(R.id.ImgUrl);

        // Load the results into the TextViews
        txId.setText(Id);
        txtmID.setText(MID);
        txtImgUrl.setText(imgUrl);
    }
}