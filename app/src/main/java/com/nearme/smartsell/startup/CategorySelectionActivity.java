package com.nearme.smartsell.startup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CategoryAdaptor;
import com.nearme.smartsell.model.CategoryInfo;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.CategoriesPayLoad;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CategorySelectionActivity extends Activity {
    CategoryAdaptor simpleAdapter;
    RecyclerView androidListView;
    Button continueBtn;
    CategoryInfo[] app_info;
    Activity context;
    private PrefManager prefManager;
    ArrayList<CategoryInfo> categoryInfoArrayList = new ArrayList<>();
    ProgressBar pb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nearme.smartsell.R.layout.filter_list_activity);
        continueBtn = findViewById(R.id.ContinureBtn);
        continueBtn.setEnabled(true);
        context = this;
        prefManager = new PrefManager(this);
        androidListView = findViewById(R.id.filter_list_view);
        pb = findViewById(R.id.pb);
        androidListView.setLayoutManager(new LinearLayoutManager(this));
        checkInternetOrGps();
        getCategoriesDialog();
        continueBtn.setOnClickListener(view -> {
            int checkStateSize = 0;
            StringBuilder categorySelected = new StringBuilder();

            for (int i = 0; i < categoryInfoArrayList.size(); i++) {
                if (categoryInfoArrayList.get(i).chkSelect) {
                    categorySelected.append("1");
                    checkStateSize++;
                } else {
                    categorySelected.append("0");
                }
            }
            Log.d(TAG, "onCreate: " + categorySelected.toString());
            long bits = (Long) Long.parseLong(categorySelected.reverse().toString(), 2);
            PhoneActivity.category = String.valueOf(bits);
            if (checkStateSize > 0) {
                prefManager.setSelectedCategory(checkStateSize);
                startActivity(new Intent(CategorySelectionActivity.this, UserDetailsActivity.class));
            } else {
                prefManager.setCategorySelected(false);
                Toast.makeText(CategorySelectionActivity.this, "Please Select Category", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCategoriesDialog() {
        pb.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<CategoriesPayLoad[]> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesPayLoad[]>() {
            @Override
            public void onResponse(Call<CategoriesPayLoad[]> call, Response<CategoriesPayLoad[]> response) {
                CategoriesPayLoad[] categoriesPayLoad = response.body();
                categoryInfoArrayList.clear();
                if (categoriesPayLoad != null) {
                    for (CategoriesPayLoad aCategoriesPayLoad : categoriesPayLoad) {
                        try {
                            CategoryInfo info = new CategoryInfo();
                            info.chkSelect = false;//new CategoryInfo().chkSelect;//.setChecked(false);
                            info.imgIcon = aCategoriesPayLoad.catimg;
                            info.txtId = (aCategoriesPayLoad.catname);
                            info.txtTitle = String.valueOf(aCategoriesPayLoad.catid);
                            categoryInfoArrayList.add(info);
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                }
                pb.setVisibility(View.GONE);
                simpleAdapter = new CategoryAdaptor(context, categoryInfoArrayList);
                androidListView.setAdapter(simpleAdapter);
                simpleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<CategoriesPayLoad[]> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                pb.setVisibility(View.GONE);
                Toast.makeText(CategorySelectionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void checkInternetOrGps() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            }
        }

    }
}



