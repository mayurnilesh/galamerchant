package com.nearme.smartsell.startup;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.BillsHistoryAdapter;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.ContactSpecificInfo;
import com.nearme.smartsell.utility.PrefManager;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class BillsHistory extends Activity {

    ListView list;
    BillsHistoryAdapter adapter;
    EditText editsearch;
    private PrefManager prefManager;
    ArrayList<ContactSpecificInfo> arraylist = new ArrayList<ContactSpecificInfo>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bills_history_layout);
        prefManager = new PrefManager(this);
        // Locate the ListView in listview_main.xml
        list = findViewById(R.id.listview);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ContactSpecificInfo[]> call = apiService.getreceipthistoryResponse(prefManager.getMerchantID(),"0");
        call.enqueue(new Callback<ContactSpecificInfo[]>() {
            @Override
            public void onResponse(Call<ContactSpecificInfo[]> call, Response<ContactSpecificInfo[]> response) {
                ContactSpecificInfo ContactSpecificInfoPayLoad[] = response.body();
                if (ContactSpecificInfoPayLoad != null &&ContactSpecificInfoPayLoad.length>0) {
//                    arraylist.addAll(0,ContactSpecificInfoPayLoad);
                    for (ContactSpecificInfo aContactSpecificInfo : ContactSpecificInfoPayLoad) {
                        arraylist.add(aContactSpecificInfo);
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactSpecificInfo[]> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
        // Pass results to ListViewAdapter Class
        adapter = new BillsHistoryAdapter(this, arraylist);

        // Binds the Adapter to the ListView
        list.setAdapter(adapter);

        // Locate the EditText in listview_main.xml
        editsearch = findViewById(R.id.search);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });

    }
}
