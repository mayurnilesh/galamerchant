package com.nearme.smartsell.startup;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearme.smartsell.R;
import com.nearme.smartsell.location.LocationListener;
import com.nearme.smartsell.location.MyLocationListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng myPosition;
    TextView searched_address;
    ImageButton submit;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        searched_address = findViewById(R.id.searched_address);
        submit = findViewById(R.id.submit);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null)
        {
            myPosition = new LatLng(location.getLatitude(), location.getLongitude());
            getAddress(myPosition);
        }
        else{
            Location location1 = LocationListener.myLocation;
            if(location1!=null){
                myPosition = new LatLng(location1.getLatitude(), location1.getLongitude());
                getAddress(myPosition);
            }
        }

        mapFragment.getMapAsync(this);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                if (searched_address.getText().toString().length() == 0)
                    resultIntent.putExtra("address", "");
                else
                    resultIntent.putExtra("address", searched_address.getText().toString());

                if (myPosition == null) {
                    resultIntent.putExtra("lat", 0.0);
                    resultIntent.putExtra("lng", 0.0);
                } else {
                    resultIntent.putExtra("lat", myPosition.latitude);
                    resultIntent.putExtra("lng", myPosition.longitude);
                }
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMyLocationEnabled(true);
        mMap = googleMap;
        if(myPosition != null) {
            mMap.addMarker(new MarkerOptions().position(myPosition).title("Marker in Sydney").draggable(true));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 12.0f));
        }
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng point) {

                mMap.clear();
                myPosition = point;
                mMap.addMarker(new MarkerOptions().position(myPosition).title("Marker in Sydney").draggable(true));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 12.0f));

                getAddress(point);

            }
        });

    }

    private void getAddress(LatLng point) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            Log.d("TAG", "getAddress: " + address);
            if (address != null)
                searched_address.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
