package com.nearme.smartsell.startup;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.TokenLogAdapter;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.TokenLog;
import com.nearme.smartsell.utility.PrefManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokenLogActivity extends Activity  {
    String noofCounters;
    Spinner counterSpinner;
    Gson gson;
    ApiInterface apiService;
    RecyclerView recyclerView;
    private PrefManager prefManager;
    TokenLogAdapter cuAdapter;
    Context curContext;
    ImageView side_menu;
    List<String> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.token_log_activity);
        Bundle extras = getIntent().getExtras();
        counterSpinner=findViewById(R.id.counterSpinner);
        curContext=this.getApplicationContext();
        gson=new Gson();
        recyclerView=findViewById(R.id.recyclerView);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        prefManager = new PrefManager(this);
        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });


        Integer count=(extras.getInt("NoofCounters"));
        for (int i = 1; i <= count; i++) {
            list.add(String.valueOf(i));
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(curContext,
                android.R.layout.simple_list_item_1, list);
        counterSpinner.setAdapter(adapter2);
        counterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Call<String> getMerchantDetailscall = apiService.gettokenlogforcounterResponse(prefManager.getMerchantID(),
                        selectedItem);

                getMerchantDetailscall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                        try {
                            if (response.body() != null) {

                                Gson gson = new Gson();

                                Type type = new TypeToken<List<TokenLog>>() {
                                }.getType();
                                List<TokenLog> jsonList = gson.fromJson(response.body(), type);
//                                jsonList=jsonList.retainAll()
                                if (jsonList.size() > 0) {
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(curContext);
                                    recyclerView.setLayoutManager(linearLayoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    cuAdapter = new TokenLogAdapter(jsonList);
                                    recyclerView.setAdapter(cuAdapter);
                                } else {
                                    Toast.makeText(curContext, "No Details Found For Selected Counter. Please Try Again", Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (Exception ex) {
                        }
                    }
                    @Override
                    public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                        Toast.makeText(curContext, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                });
            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }


}
