package com.nearme.smartsell.startup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.EditInvoiceFragment;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.MerchantPayload;
import com.nearme.smartsell.rest.versionclass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class IdVerification extends AppCompatActivity {
Context context;
Button verifyBtn;
    Gson gson;
    EditText idproofTxt,mobnoTxt;
    String mobileNum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(
                R.layout.id_verification);
        context=this.getApplicationContext();
        gson = new Gson();
        verifyBtn=findViewById(R.id.verifyBtn);
        idproofTxt=findViewById(R.id.govid);
        mobnoTxt=findViewById(R.id.mobNo);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobnoTxt != null && idproofTxt != null) {
                    if (!idproofTxt.getText().toString().equals("") && !mobnoTxt.getText().toString().equals("") && mobnoTxt.getText().toString().length() == 10) {
                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);
                        mobileNum= mobnoTxt.getText().toString();
                        if(!mobileNum.contains("+91"))
                        {
                            mobileNum="+91"+mobileNum;
                        }
                        Call<String> getMerchantDetailscall = apiService.getMerchantDetails(mobileNum);

                        getMerchantDetailscall.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                                try {
                                    MerchantPayload mp = gson.fromJson(response.body(), MerchantPayload.class);//
                                   if(mp.merchantIdProof.equals(idproofTxt.getText().toString()))
                                   {
                                       Intent tIntent=new Intent(IdVerification.this, ChangePassword.class);
                                       tIntent.putExtra("mer_id", mobileNum);
                                       startActivity(tIntent);
                                   }
                                   else {
                                       Toast.makeText(getApplicationContext(), "Verification Failed. Please check your ID proof and Try Again", Toast.LENGTH_LONG).show();

                                   }
                                } catch (Exception ex) {
                                }
                            }

                            @Override
                            public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Failed Please Try Again", Toast.LENGTH_LONG).show();
                            }
                        });

//                        startActivity(new Intent(IdVerification.this, ChangePassword.class));
                    } else if (mobnoTxt.getText().toString().length() < 10) {
                        mobnoTxt.setError("Please Enter Valid Mobile Number");
                    } else if (idproofTxt.getText().toString().length() == 0) {
                        mobnoTxt.setError("Please Enter Valid Mobile Number");
                    }
                }
            }
        });

    }
//    @Override
//    public void onBackPressed()
//    {
//        // code here to show dialog
//        //super.onBackPressed();  // optional depending on your needs
//        //startActivity(new Intent(context, MainActivity.class));
//    }
}
