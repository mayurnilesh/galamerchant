package com.nearme.smartsell.startup;

import android.os.Bundle;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.InventoryBillDataAdapter;
import com.nearme.smartsell.db.billingitem;
import com.nearme.smartsell.model.Billdata;
import com.nearme.smartsell.utility.PrefManager;

import java.util.ArrayList;
import java.util.List;

//import android.media.Image;

public class InventoryActivity extends BaseActivity {


    private PrefManager prefManager;
    String TAG = "AddBillsItemActivity";
    RecyclerView recbillitem;
    ArrayList<Billdata> billdataArrayList = new ArrayList<>();
    ArrayList<Billdata> tmpArray = new ArrayList<>();
    InventoryBillDataAdapter apercuAdapter;
    EditText search;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_layout);
        prefManager = new PrefManager(this);
        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        FromMainActivity();
        initialization();
        clicks();
        mToolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(mToolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_main, menu);

        MenuItem mSearch = menu.findItem(R.id.action_search);

        final SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint("Search");

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                billdataArrayList.clear();
                if (newText.length() >= 1) {
                    //
                    for (int i = 0; i < tmpArray.size(); i++) {
                        if (tmpArray.get(i).getItemid().contains(newText) || tmpArray.get(i).getItemname().contains(newText)) {
                            billdataArrayList.add(tmpArray.get(i));
                        }
                    }
                } else {
                }
                Log.d(TAG, "onTextChanged: " + billdataArrayList.size());
                apercuAdapter.notifyDataSetChanged();
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void clicks() {

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                billdataArrayList.clear();
                if (s.length() < 1) {
                    billdataArrayList.clear();
                } else {
                    for (int i = 0; i < tmpArray.size(); i++) {
                        if (tmpArray.get(i).getItemid().contains(s) || tmpArray.get(i).getItemname().contains(s)) {
                            billdataArrayList.add(tmpArray.get(i));
                        }
                    }
                }
                apercuAdapter.notifyDataSetChanged();
            }
        });
    }


    private void initialization() {
        recbillitem = findViewById(R.id.recbillsearchviewitem);
        search = findViewById(R.id.search);

        List<billingitem> billingitemList = billingitem.listAll(billingitem.class);
        Log.d(TAG, "initialization: " + billingitemList.size());

        for (int i = 0; i < billingitemList.size(); i++) {
            Billdata billdata = new Billdata();
            billdata.setItemdesc(billingitemList.get(i).itemdesc);
            billdata.setItemid(billingitemList.get(i).itemid);
            billdata.setItemname(billingitemList.get(i).itemname);
            billdata.setItemprice(billingitemList.get(i).itemprice);
            billdata.setQuantity(billingitemList.get(i).itemQuantity.toString());
            tmpArray.add(billdata);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recbillitem.setLayoutManager(linearLayoutManager);
        recbillitem.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new InventoryBillDataAdapter(billdataArrayList, new InventoryBillDataAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Billdata item) {

            }
        });
        recbillitem.setAdapter(apercuAdapter);
    }


}
