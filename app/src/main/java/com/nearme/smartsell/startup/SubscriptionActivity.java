package com.nearme.smartsell.startup;

import android.os.Bundle;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.widget.TextView;

import com.nearme.smartsell.R;

public class SubscriptionActivity extends BaseActivity {

    TextView tv_one_month, tv_six_month, tv_one_year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        side_menu = findViewById(R.id.side_menu);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        FromMainActivity();
    }
//        tv_one_year = findViewById(R.id.tv_one_year);
//        tv_six_month = findViewById(R.id.tv_six_month);
//        tv_one_month = findViewById(R.id.tv_one_month);
//
//        tv_one_month.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                payNow(100.00);
//            }
//        });
//        tv_six_month.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                payNow(500.00);
//            }
//        });
//        tv_one_year.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                payNow(1000.00);
//            }
//        });
//
//    }
//
//    private void payNow(Double amount) {
//        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
//        //Use this to set your custom text on result screen button
//        payUmoneyConfig.setDoneButtonText("GalaApp");
//        //Use this to set your custom title for the activity
//        payUmoneyConfig.setPayUmoneyActivityTitle("GalaApp");
//        boolean isDisableExitConfirmation = false;
//        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);
//        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
//
//
//        String txnId = System.currentTimeMillis() + "";
//        //String txnId = "TXNID720431525261327973";
//        String phone = "9904405269";
//        String productName = "Subscription";
//        String firstName = "Mayur";
//        String email = "mayur@gmail.com";
//        String udf1 = "";
//        String udf2 = "";
//        String udf3 = "";
//        String udf4 = "";
//        String udf5 = "";
//        String udf6 = "";
//        String udf7 = "";
//        String udf8 = "";
//        String udf9 = "";
//        String udf10 = "";
//
//        AppEnvironment appEnvironment = ((FrescoApplication) getApplication()).getAppEnvironment();
//        builder.setAmount(amount)
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")
//                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setUdf6(udf6)
//                .setUdf7(udf7)
//                .setUdf8(udf8)
//                .setUdf9(udf9)
//                .setUdf10(udf10)
//                .setIsDebug(true)
//                .setKey("LZHOotes")
//                .setMerchantId("6750984");
//
//        try {
//            PayUmoneySdkInitializer.PaymentParam mPaymentParams = builder.build();
//            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
//            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, SubscriptionActivity.this, R.style.AppTheme_Green, true);
//            Log.d("TAG", "launchPayUMoneyFlow: " + mPaymentParams.getParams());
//        } catch (Exception e) {
//            // some exception occurred
//            Toast.makeText(SubscriptionActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
//            //  payNowButton.setEnabled(true);
//        }
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result Code is -1 send from Payumoney activity
//        Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
//            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);
//
//            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//
//                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                    //Success Transaction
//                } else {
//                    //Failure Transaction
//                }
//
//                // Response from Payumoney
//                String payuResponse = transactionResponse.getPayuResponse();
//
//                // Response from SURl and FURL
//                String merchantResponse = transactionResponse.getTransactionDetails();
////            }  else if (resultModel != null && resultModel.getError() != null) {
////                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
////            } else {
////                Log.d(TAG, "Both objects are null!");
////            }
//            }
//        }
//    }
//
//    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
//
//        StringBuilder stringBuilder = new StringBuilder();
//        HashMap<String, String> params = paymentParam.getParams();
//        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
//
//        AppEnvironment appEnvironment = ((FrescoApplication) getApplication()).getAppEnvironment();
//        stringBuilder.append("LBcmBkaXUr");
//
//        String hash = hashCal(stringBuilder.toString());
//        paymentParam.setMerchantHash(hash);
//
//        return paymentParam;
//    }
//
//    public static String hashCal(String str) {
//        byte[] hashseq = str.getBytes();
//        StringBuilder hexString = new StringBuilder();
//        try {
//            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
//            algorithm.reset();
//            algorithm.update(hashseq);
//            byte messageDigest[] = algorithm.digest();
//            for (byte aMessageDigest : messageDigest) {
//                String hex = Integer.toHexString(0xFF & aMessageDigest);
//                if (hex.length() == 1) {
//                    hexString.append("0");
//                }
//                hexString.append(hex);
//            }
//        } catch (NoSuchAlgorithmException ignored) {
//        }
//        return hexString.toString();
//    }
//


}
