package com.nearme.smartsell.startup;

import android.Manifest.permission;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.hbb20.CountryCodePicker;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.firebasedata.SharedPreferenceHelper;
import com.nearme.smartsell.fragments.AnnouncementBizSendingFragment;
import com.nearme.smartsell.network.HttpGetRequest;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.security.Permission;
import java.util.List;


public class UserDetailsActivity extends Activity implements
        View.OnClickListener {
    EditText mNameField;
    EditText mContactNumber;
    EditText mAdhar;
    EditText mShopno, mPassword;
    CountryCodePicker ccp;
    Button locationInMap;
    TextView selctedAddrsss;
    ImageButton mNextButton;
    SimpleDraweeView mBtnCapturePicture, proofUploadBtn;
    public ImageView selectedImg;
    private static final String TAG = "PhoneActivity";
    Uri imageUri = null;
    private PrefManager prefManager;

    boolean isProfile = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetails);
        prefManager = new PrefManager(this);
        ccp = findViewById(R.id.ccp);

        mNameField = findViewById(R.id.field_name);

        mContactNumber = findViewById(R.id.field_contact);

        mPassword = findViewById(R.id.password);

        mAdhar = findViewById(R.id.aadhar);

        proofUploadBtn = findViewById(R.id.uploadIDBtn);
        proofUploadBtn.setOnClickListener(this);

        mShopno = findViewById(R.id.shopno);

        locationInMap = findViewById(R.id.locationinmap);
        selctedAddrsss = findViewById(R.id.selectedaddress);
        mNextButton = findViewById(R.id.userdetails);


        mBtnCapturePicture = findViewById(R.id.btnCapturePicture);
        mNextButton.setOnClickListener(this);
        mBtnCapturePicture.setOnClickListener(this);
        locationInMap.setOnClickListener(this);
        selectedImg = findViewById(R.id.imageView);

    }

    private void checkInternetOrGps() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            }
        }

    }

    public void showMap() {
        startActivityForResult(new Intent(UserDetailsActivity.this, MapsActivity.class), 1000);
    }

    @Override
    public void onStart() {
        super.onStart();
        String num = SharedPreferenceHelper.getInstance(getBaseContext()).getusercontact();
        try {
            if (!prefManager.getMerchantID().equals("-1")) {
                List<userdetails> temp = userdetails.listAll(userdetails.class);
                if (num.length() > 0 && temp.size() > 0) {
                    startActivity(new Intent(UserDetailsActivity.this, ManageSlotsActivity.class));
                    finish();
                }
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.locationinmap:
                checkInternetOrGps();
                showMap();
                break;

            case R.id.userdetails:
                prefManager.setLoginType("0");
                if (saveuserinfo()) {
                    startActivity(new Intent(UserDetailsActivity.this, PhoneActivity.class));
                }
                break;
            case R.id.btnCapturePicture:
                isProfile = true;
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .setActivityTitle(getResources().getString(R.string.app_name))
//                        .setCropShape(CropImageView.CropShape.RECTANGLE)
//                        .setCropMenuCropButtonTitle("Done")
//                        .setRequestedSize(400, 400)
//                        .setOutputCompressQuality(50)
//                        .start(this);
                com.github.dhaval2404.imagepicker.ImagePicker.Companion.with(UserDetailsActivity.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
                break;
            case R.id.uploadIDBtn:
                isProfile = false;
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .setActivityTitle(getResources().getString(R.string.app_name))
//                        .setCropShape(CropImageView.CropShape.RECTANGLE)
//                        .setCropMenuCropButtonTitle("Done")
//                        .setRequestedSize(400, 400)
//                        .setOutputCompressQuality(50)
//                        .start(this);
                com.github.dhaval2404.imagepicker.ImagePicker.Companion.with(UserDetailsActivity.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();

                break;
        }

    }


    private boolean saveuserinfo() {

        if (mNameField.getText() == null || PhoneActivity.enteredImageUri == null ||
                PhoneActivity.enteredIDProofUri == null || selctedAddrsss.getText().length() == 0
                || PhoneActivity.latitude == 0.0 || PhoneActivity.longitude == 0.0 || mAdhar.getText().toString().length() == 0
                || mContactNumber.getText().length() == 0 || mContactNumber.getText().length() < 10 || mNameField.getText().length() == 0
                || mShopno.getText().length() == 0) {
            if (mNameField.getText() == null) {
                Toast.makeText(this, "Please Provide Name", Toast.LENGTH_SHORT).show();
            }
            if (PhoneActivity.enteredIDProofUri == null) {
                Toast.makeText(this, "Please Provide Scanned Valid ID Proof", Toast.LENGTH_SHORT).show();
            }
            if (PhoneActivity.enteredImageUri == null) {
                Toast.makeText(this, "Please Provide Profile Picture", Toast.LENGTH_SHORT).show();
            }
            if (mAdhar.getText().toString().length() == 0) {
                Toast.makeText(this, "Please enter ID ", Toast.LENGTH_SHORT).show();
            }
            if (selctedAddrsss.getText().length() == 0
                    || PhoneActivity.latitude == 0.0 || PhoneActivity.longitude == 0.0) {
                Toast.makeText(this, "Please select shop location", Toast.LENGTH_SHORT).show();
            }
            if (mContactNumber.getText().length() == 0 || mContactNumber.getText().length() < 10) {
                Toast.makeText(this, "Please enter valid contact number", Toast.LENGTH_SHORT).show();
            }
            if (mNameField.getText().length() == 0) {
                Toast.makeText(this, "Please enter shop name", Toast.LENGTH_SHORT).show();
            }
            if (mShopno.getText().length() == 0) {
                Toast.makeText(this, "Please enter valid shop number", Toast.LENGTH_SHORT).show();
            }
            return false;
        }

        PhoneActivity.enteredName = mNameField.getText().toString();
        PhoneActivity.enteredNumber = ccp.getDefaultCountryCodeWithPlus() + mContactNumber.getText().toString();
        PhoneActivity.enteredAadhar = mAdhar.getText().toString();
        PhoneActivity.enteredShopno = mShopno.getText().toString();
        PhoneActivity.password = mPassword.getText().toString();

        Toast.makeText(this, "Hello " + PhoneActivity.enteredName + " You will Receive Otp on " + PhoneActivity.enteredNumber, Toast.LENGTH_SHORT).show();
        return true;

    }

    void displaySelectedImage(Intent data) {
        imageUri = data.getData();

        PhoneActivity.enteredImageUri = imageUri;

        int width = mBtnCapturePicture.getWidth();
        int heigth = mBtnCapturePicture.getHeight();
        if (width == 0)
            width = 128;
        if (heigth == 0)
            heigth = 128;

        Picasso.with(this).load(imageUri).resize(width, heigth)
                .centerInside().into(mBtnCapturePicture);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                displaySelectedImage(data);
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                Uri _imageUri = data.getData();
                if (_imageUri != null) {
                    proofUploadBtn.setVisibility(View.VISIBLE);
                    PhoneActivity.enteredIDProofUri = _imageUri;
                    int width;
                    int heigth;
                    width = 40;
                    heigth = 40;

                    Picasso.with(this).load(_imageUri).resize(width, heigth)
                            .centerInside().into(proofUploadBtn);
                } else {
                    proofUploadBtn.setVisibility(View.INVISIBLE);
                }
            }
        } else if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                try {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    StringBuilder stBuilder = new StringBuilder();
                    String placename = String.format("%s", place.getName());
                    String latitude = String.valueOf(place.getLatLng().latitude);
                    PhoneActivity.latitude = place.getLatLng().latitude;

                    String longitude = String.valueOf(place.getLatLng().longitude);
                    PhoneActivity.longitude = place.getLatLng().longitude;
                    String address = String.format("%s", place.getAddress());
                    stBuilder.append("Name: ");
                    stBuilder.append(placename);
                    stBuilder.append("\n");
                    stBuilder.append("Address: ");
                    stBuilder.append(address);
                    selctedAddrsss.setText(stBuilder);
                } catch (Exception ex) {
                    Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
                // textView.setText(stBuilder.toString());
            }
        } else if (requestCode == 1000 && data != null) {
            try {
                selctedAddrsss.setText(data.getExtras().getString("address"));
                PhoneActivity.latitude = data.getExtras().getDouble("lat");
                PhoneActivity.longitude = data.getExtras().getDouble("lng");
            } catch (Exception e) {
                e.printStackTrace();
                selctedAddrsss.setText("");
                PhoneActivity.latitude = 0.0;
                PhoneActivity.longitude = 0.0;
            }
        } else if (data != null && data.getData() != null) {
            Uri uri = data.getData();
            if (isProfile) {
                PhoneActivity.enteredImageUri = uri;
                Picasso.with(this).load(uri).into(mBtnCapturePicture);
            } else {
                PhoneActivity.enteredIDProofUri = uri;
                Picasso.with(this).load(uri).into(proofUploadBtn);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
                    new String[]{permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE,
                            permission.ACCESS_FINE_LOCATION,
                            permission.ACCESS_COARSE_LOCATION,
                    }, 1);
    }
}



