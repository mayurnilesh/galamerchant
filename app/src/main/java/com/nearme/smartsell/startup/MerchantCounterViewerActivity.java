package com.nearme.smartsell.startup;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CounterCheckboxAdpter;
import com.nearme.smartsell.adapter.CounterDataAdapter;
import com.nearme.smartsell.model.CounterData;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.counterandhelper;
import com.nearme.smartsell.rest.counteremppayload;
import com.nearme.smartsell.utility.PrefManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MerchantCounterViewerActivity extends BaseActivity {
    ApiInterface apiService;
    PrefManager prefManager;
    String MerchantID;
    public List<counteremppayload> counteremppayloadArrayList = new ArrayList<>();

    Context curContext;
    RecyclerView countersdatalistview, reccounters;
    CounterCheckboxAdpter apercuAdapter;
    ImageView viewarrow;
    RelativeLayout linmain;
    CounterDataAdapter adapter;
    LinearLayout mainview;
    String TAG = "MerchantCounterViewerActivity";
    ImageView submit;
    MediaPlayer mediaPlayer;
    TextView backtextView,gotextView;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        setContentView(R.layout.merchant_counter_viewer_activity);

        prefManager = new PrefManager(this);
        final String merchantId = prefManager.getMerchantID();
        curContext = this.getApplicationContext();
        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        FromMainActivity();
        initialize();
        getcounters(merchantId);
        linmain.setVisibility(View.VISIBLE);
        mainview.setVisibility(View.INVISIBLE);
        viewarrow.setVisibility(View.INVISIBLE);
        submit.setVisibility(View.VISIBLE);
        backtextView.setVisibility(View.VISIBLE);
        gotextView.setVisibility(View.INVISIBLE);
    }

    private void initialize() {
        countersdatalistview = findViewById(R.id.countersDataList);
        reccounters = findViewById(R.id.reccounters);
        viewarrow = findViewById(R.id.viewarrow);
        linmain = findViewById(R.id.linmain);
        submit = findViewById(R.id.submit);
        mainview=findViewById(R.id.mainview);
        backtextView=findViewById(R.id.backtextView);
        gotextView=findViewById(R.id.textView4);
        viewarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linmain.getVisibility() == View.VISIBLE)
                    linmain.setVisibility(View.GONE);
                else
                    linmain.setVisibility(View.VISIBLE);

                linmain.setVisibility(View.VISIBLE);
                mainview.setVisibility(View.VISIBLE);
                countersdatalistview.setVisibility(View.INVISIBLE);
                viewarrow.setVisibility(View.INVISIBLE);
                submit.setVisibility(View.VISIBLE);
                backtextView.setVisibility(View.VISIBLE);
                gotextView.setVisibility(View.INVISIBLE);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linmain.setVisibility(View.GONE);
                mainview.setVisibility(View.VISIBLE);
                viewarrow.setVisibility(View.VISIBLE);
                countersdatalistview.setVisibility(View.VISIBLE);
                submit.setVisibility(View.GONE);
                backtextView.setVisibility(View.INVISIBLE);
                gotextView.setVisibility(View.VISIBLE);
                if (mTimer != null) {
                    mTimer.cancel();
                }
                startTimer();
            }
        });

    }

    public void getcounters(String merchantID) {
        MerchantID = merchantID;
        Call<String> getMerchantDetailscall = apiService.getmerchantcounterdetailsResponse(merchantID);
        getMerchantDetailscall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<counterandhelper>() {
                        }.getType();
                        counterandhelper obj = gson.fromJson(response.body(), type);
                        counteremppayloadArrayList = obj.getCounterslist();
                        Log.d("TAG", "onResponse: " + counteremppayloadArrayList.size());
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MerchantCounterViewerActivity.this);
                        reccounters.setLayoutManager(mLayoutManager);
                        reccounters.setItemAnimator(new DefaultItemAnimator());
                        apercuAdapter = new CounterCheckboxAdpter(counteremppayloadArrayList);
                        reccounters.setAdapter(apercuAdapter);

                        // getTokenStatus();


                    }
                } catch (Exception ex) {
                    Toast.makeText(getBaseContext(), "Merchant details Fetching failed. Please Try Again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
//                Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getTokenStatus() {

        Call<String> call = apiService.
                getcurrenttokenforallcounterswithmerchantid(String.valueOf(MerchantID));

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (response.body() != null) {
                        //created list of counters and their tokens !
                        Gson gson = new Gson();

                        Type type = new TypeToken<ConcurrentHashMap<Integer, String>>() {
                        }.getType();
                        if (!response.body().equals("-12")) {
                            Log.d(TAG, "onResponse: " + response.body());
                            ConcurrentHashMap<Integer, String> jsonList = gson.fromJson(response.body(), type);
                            if (jsonList.size() > 0) {
                                List<CounterData> tmpArraylist = new ArrayList<>();
                                List<CounterData> counterDataArrayList = new ArrayList<>();
                                for (Integer js : jsonList.keySet()) {
                                    CounterData cd = new CounterData();
                                    cd.setTokenID(jsonList.get(js));
                                    cd.setCounterID(js.toString());
                                    tmpArraylist.add(cd);
                                    Log.d("TAGSSSS", "onResponse: " + jsonList.get(js) + " " + js.toString());
                                }

                                for (int i = 0; i < counteremppayloadArrayList.size(); i++) {
                                    if (counteremppayloadArrayList.get(i).isStatus()) {
                                        for (int j = 0; j < tmpArraylist.size(); j++) {
                                            if (tmpArraylist.get(j).getCounterID().equalsIgnoreCase(String.valueOf(i + 1)))
                                                counterDataArrayList.add(tmpArraylist.get(j));
                                        }
                                    }
                                }

                                LinearLayoutManager lm = new LinearLayoutManager(MerchantCounterViewerActivity.this);
                                countersdatalistview.setLayoutManager(lm);
                                if(adapter != null) {
                                    if (adapter.prevhomeArrayList != null) {
                                        if (adapter.prevhomeArrayList.size() != counterDataArrayList.size()) {
                                            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);

                                            mediaPlayer.start();
                                        } else {
                                            for (int i = 0; i < counterDataArrayList.size(); i++) {
                                                if (adapter.prevhomeArrayList.get(i).getCounterID().equalsIgnoreCase(counterDataArrayList.get(i).getCounterID()) == false
                                                        || adapter.prevhomeArrayList.get(i).getTokenID().equalsIgnoreCase(counterDataArrayList.get(i).getTokenID()) == false) {
                                                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);

                                                    mediaPlayer.start();

                                                }

                                            }


                                        }
                                    } else {
                                        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);

                                        mediaPlayer.start();
                                    }

                                }
                                adapter = new CounterDataAdapter(counterDataArrayList);
                                adapter.prevhomeArrayList = new ArrayList<>();

                                for (int i = 0; i < counterDataArrayList.size(); i++) {

                                   adapter.prevhomeArrayList.addAll(counterDataArrayList);

                                }
                                countersdatalistview.setAdapter(adapter);
                                Log.d("TAG11", "onResponse: " + counterDataArrayList.size());
                            } else {
                                Toast.makeText(curContext, "No Details Found For Selected Counter. Please Try Again", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception ex) {

                    int x = 0;
                    ++x;
                   // Log.d("MainResult", ex.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
//                Toast.makeText(curContext, "Fetch Next Token Failed. Please Try Again", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
        return;
    }
    private void startTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = new Timer();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, 5000);
    }

    public class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    // Toast.makeText(TokenEmpActivity.this, "hello", Toast.LENGTH_SHORT).show();
                    getTokenStatus();
                }

            });
        }
    }
}


