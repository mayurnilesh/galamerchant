package com.nearme.smartsell.startup;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearme.smartsell.R;
import com.nearme.smartsell.location.MyLocationListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MapsAdsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng myPosition;
    TextView searched_address;
    ImageButton submit;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_maps);
        searched_address = findViewById(R.id.searched_address);
        submit = findViewById(R.id.submit);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Places.initialize(MapsAdsActivity.this, getResources().getString(R.string.api_key));
                    List<Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));
                    Intent autocompleteIntent =
                            new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                                    .build(MapsAdsActivity.this);
                    startActivityForResult(autocompleteIntent, 3);
                }
                catch(Exception e){
                    Log.d("TAG", "onClick: "+e.getMessage());
                }
            }
        });

        Location location = MyLocationListener.mCurrentLocation;

        if (location != null) {
            // Getting latitude of the current location
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            myPosition = new LatLng(latitude, longitude);
            getAddress(myPosition);
            mapFragment.getMapAsync(this);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                if (searched_address.getText().toString().length() == 0)
                    resultIntent.putExtra("address", "");
                else
                    resultIntent.putExtra("address", searched_address.getText().toString());

                if (myPosition == null) {
                    resultIntent.putExtra("lat", 0.0);
                    resultIntent.putExtra("lng", 0.0);
                } else {
                    resultIntent.putExtra("lat", myPosition.latitude);
                    resultIntent.putExtra("lng", myPosition.longitude);
                }
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        mMap = googleMap;
        mMap.addMarker(new MarkerOptions().position(myPosition).title("Marker in Sydney").draggable(true));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 12.0f));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng point) {

                mMap.clear();
                myPosition = point;
                mMap.addMarker(new MarkerOptions().position(myPosition).title("Marker in Sydney").draggable(true));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 12.0f));

                getAddress(point);

            }
        });

    }

    private void getAddress(LatLng point) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            Log.d("TAG", "getAddress: " + address);
            if (address != null)
                searched_address.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("TAG", "Place: " + place.getName());
                searched_address.setText("" + place.getName());

                if(place.getLatLng()!=null){
                    mMap.clear();
                    myPosition = place.getLatLng();
                    mMap.addMarker(new MarkerOptions().position(myPosition).title("My position").draggable(true));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 12.0f));
                }
            }else{
                Log.d("TAG", "onActivityResult: "+data.toString());
            }
        }
    }

}
