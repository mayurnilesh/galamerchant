package com.nearme.smartsell.startup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
//import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.BillDataAdapter;
import com.nearme.smartsell.db.billingitem;
import com.nearme.smartsell.model.Billdata;
import com.nearme.smartsell.utility.PrefManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddBillsItemActivity extends BaseActivity {


    private PrefManager prefManager;
    FloatingActionButton adddata, savedata,sharedata;
    String TAG = "AddBillsItemActivity";
    RecyclerView recbillitem;
    ArrayList<Billdata> billdataArrayList = new ArrayList<>();
    ArrayList<Billdata> tmpArray = new ArrayList<>();
    BillDataAdapter apercuAdapter;
    TextView tvsubmit;
    TextView incsubmit;
    TextView decsubmit;
    Integer quantityCal = 0;
    TextView quansubmit;
    EditText search;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_billitem_layout);
        prefManager = new PrefManager(this);

        initialization();
        clicks();
        sharedata=findViewById(R.id.sharedata);
        sharedata.setVisibility(View.INVISIBLE);
        side_menu = findViewById(R.id.side_menu);
        side_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        FromMainActivity();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void clicks() {
        adddata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(AddBillsItemActivity.this, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_add_bill_item);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();
                quantityCal=1;
                tvsubmit = dialog.findViewById(R.id.tvsubmit);
                incsubmit = dialog.findViewById(R.id.increase);
                decsubmit = dialog.findViewById(R.id.decrease);
                quansubmit = dialog.findViewById(R.id.integer_number);
                quansubmit.setText(quantityCal.toString());
                final EditText editname = dialog.findViewById(R.id.editname);
                final EditText editprice = dialog.findViewById(R.id.editprice);
                final EditText editid = dialog.findViewById(R.id.editid);
                final EditText editdesc = dialog.findViewById(R.id.editdesc);
                final EditText quanitytunit = dialog.findViewById(R.id.quanitytunit);
                final Spinner spenum = dialog.findViewById(R.id.spenum);
                incsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal >= 1) {
                            quantityCal++;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 1) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });

                decsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal > 1) {
                            quantityCal--;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 1) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });
                tvsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkEditText(editname, "Enter product name") && checkEditText(editprice, "Enter product price")
                                && checkEditText(editid, "Enter product id") && checkEditText(editdesc, "Enter product description")) {
                            dialog.dismiss();
                            Integer pricePerItem=0;
                            try{
                                pricePerItem = Integer.parseInt(editprice.getText().toString());
                                pricePerItem =pricePerItem/quantityCal;
                            }catch (Exception e)
                            {

                            }
                            billingitem bill = new billingitem(prefManager.getMerchantID(), editid.getText().toString()
                                    , editname.getText().toString(), pricePerItem.toString(), editdesc.getText().toString(),quantityCal,
                                    quanitytunit.getText().toString(),spenum.getSelectedItem().toString());

                            long value = bill.save();
                            Log.d(TAG, "onClick: " + value);

                            Billdata billdata = new Billdata();
                            billdata.setItemdesc(editdesc.getText().toString());
                            billdata.setItemid(editid.getText().toString());
                            billdata.setItemname(editname.getText().toString());
                            billdata.setItemprice(editprice.getText().toString());
                            billdata.setQuantity(quantityCal.toString());
                            billdata.setQuantityunit(quanitytunit.getText().toString());
                            billdata.setQuantityenum(spenum.getSelectedItem().toString());
                            billdataArrayList.add(billdata);
                            tmpArray.add(billdata);

                            apercuAdapter.notifyDataSetChanged();
                        }
                    }
                });

            }
        });

        savedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveLayoutAsPDFToSdcard();
                }
                catch (Exception ex)
                {

                }
            }
        });

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                billdataArrayList.clear();
                if (s.length() < 1) {
                    billdataArrayList.addAll(tmpArray);
                } else {
                    for (int i = 0; i < tmpArray.size(); i++) {
                        if (tmpArray.get(i).getItemid().contains(s) || tmpArray.get(i).getItemname().contains(s)) {
                            billdataArrayList.add(tmpArray.get(i));
                        }
                    }
                }
                apercuAdapter.notifyDataSetChanged();
            }
        });
    }

    public static String getSdCardPath() {
        //return Environment.DIRECTORY_DOWNLOADS + "/";
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        //return App.getApp().getApplicationContext().getFilesDir().getAbsolutePath()
    }

    public PdfPTable createFirstTable() {
        // a table with three columns
        PdfPTable table = new PdfPTable(4);
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("Product ID"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Product Name"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
//        cell = new PdfPCell(new Phrase("Stock"));
//        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Price"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Description"));
//        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
//        // now we add a cell with rowspan 2
//        LayoutInflater inflater = LayoutInflater.from(getContext());//(LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
//        LinearLayout content = (LinearLayout) inflater.inflate
//                (R.layout.fragment_one, null);
//        View vr = (View) inflater.inflate(R.layout.activity_grocerylistmaker, null, false);


        for (int i = 0; i < billdataArrayList.size(); i++) {
            table.addCell(billdataArrayList.get(i).getItemid());
            table.addCell(billdataArrayList.get(i).getItemname());
            table.addCell(String.valueOf(billdataArrayList.get(i).getItemprice()));
            table.addCell(String.valueOf(billdataArrayList.get(i).getItemdesc()));
        }
//        table.
        return table;
    }

    private void saveLayoutAsPDFToSdcard() {
        String PDF_FILE = "mnt/sdcard/invoice.pdf";
        try {
            Document document = new Document();
            String path = getSdCardPath();
            String curdate = new SimpleDateFormat("HH-ss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(document, new FileOutputStream(path + "GalaInvoice" + curdate + ".pdf"));
            document.open();
//            document.setPageSize(new Rectangle());
            document.addHeader("header", "Gala SuperMarkets Ltd." +
                    " CIS No:- L6835798345" +
                    " GSTIN: KDSFHJSHFK");

            try {
                String filename = prefManager.getMerchantLogoURI();
                Image image = Image.getInstance(filename);
                image.scaleToFit(100f, 100f);
                document.add(image);
                //add spacing document.add("");
            }
            catch (MalformedURLException mex)
            {

            }
            catch (IOException ioe)
            {

            }
            document.add(createFirstTable());
            document.close();

            Log.v("Chaitu", "====saved==  ");
            //to open the saved file
            Toast.makeText(this, "Congrats GalaInvoice is Saved to PDF", Toast.LENGTH_SHORT).show();
            File filer = new File(path + "GalaInvoice" + curdate + ".pdf");
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(filer), "application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            Log.v("Chaitu", "====failed==107" + e.toString());
            e.printStackTrace();
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @SuppressLint("RestrictedApi")
    private void initialization() {
        adddata = findViewById(R.id.adddata);
        savedata = findViewById(R.id.savedata);
        recbillitem = findViewById(R.id.recbillitem);
        search = findViewById(R.id.search);

        savedata.setVisibility(View.INVISIBLE);
        List<billingitem> billingitemList = billingitem.listAll(billingitem.class);
        Log.d(TAG, "initialization: " + billingitemList.size());

        for (int i = 0; i < billingitemList.size(); i++) {
            Billdata billdata = new Billdata();
            billdata.setItemdesc(billingitemList.get(i).itemdesc);
            billdata.setItemid(billingitemList.get(i).itemid);
            billdata.setItemname(billingitemList.get(i).itemname);
            billdata.setItemprice(billingitemList.get(i).itemprice);
            billdata.setQuantity(billingitemList.get(i).itemQuantity.toString());
            billdata.setQuantityunit(billingitemList.get(i).itemunit);
            billdata.setQuantityenum(billingitemList.get(i).itemenum);
            billdataArrayList.add(billdata);
            tmpArray.add(billdata);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recbillitem.setLayoutManager(linearLayoutManager);
        recbillitem.setItemAnimator(new DefaultItemAnimator());
        apercuAdapter = new BillDataAdapter(billdataArrayList, new BillDataAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Billdata item) {

            }
        });
        recbillitem.setAdapter(apercuAdapter);
    }

    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equalsIgnoreCase("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }


}
