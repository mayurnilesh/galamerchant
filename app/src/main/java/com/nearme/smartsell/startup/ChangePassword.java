package com.nearme.smartsell.startup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nearme.smartsell.LoginActivity;
import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.versionclass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ChangePassword extends AppCompatActivity {
Context context;
    Button verifyBtn;
    EditText newPSwdTxt,confirmPswdTxt;
    Bundle bundle;
    String mer_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(
                R.layout.password_change);
        context = this.getApplicationContext();
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mer_id = extras.getString("mer_id");
        }
        verifyBtn = findViewById(R.id.button);
        newPSwdTxt = findViewById(R.id.newpassword);
        confirmPswdTxt = findViewById(R.id.confirmPassword);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPSwdTxt.getText().toString().length() == 4 && confirmPswdTxt.getText().toString().length() == 4) {
                    if (newPSwdTxt.getText().toString().equals(confirmPswdTxt.getText().toString())) {
                        ApiInterface apiService =
                                ApiClient.getClient().create(ApiInterface.class);
                        Call<String> call = apiService.MerchantChangePwd(mer_id,confirmPswdTxt.getText().toString());
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                if (response.body().equals("1")) {

                                    startActivity(new Intent(ChangePassword.this, LoginActivity.class));
                                } else {
                                    Toast.makeText(getApplicationContext(), "Failed. Please Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                // Log error here since request failed
                                Log.e(TAG, t.toString());
                            }
                        });

                    } else {
                        confirmPswdTxt.setError("Please Enter Same password");
                    }
                } else {
                    newPSwdTxt.setError("Please Enter password of 4 Digits");
                    confirmPswdTxt.setError("Please Enter password of 4 Digits");
                }
            }
        });


    }
    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
        //startActivity(new Intent(context, MainActivity.class));
    }
}
