package com.nearme.smartsell.startup;

import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.nearme.smartsell.cache.ImagePipelineConfigFactory;
import com.nearme.smartsell.language.LocaleManager;
import com.nearme.smartsell.utility.AppEnvironment;
import com.orm.SugarApp;

/**
 * Created by 06peng on 2015/6/24.
 */
public class FrescoApplication extends SugarApp {

    AppEnvironment appEnvironment;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this, ImagePipelineConfigFactory.getImagePipelineConfig(this));

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
        MultiDex.install(this);
    }

    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }

}
