package com.nearme.smartsell.startup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearme.smartsell.R;
import com.nearme.smartsell.location.MyLocationListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChoosenLocationActivity extends Activity {
    TextView tvnodata;
    private TextView searchbox;
    double latitude;
    double longitude;
    static ChoosenLocationActivity curActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_choosenlocation);
        tvnodata = findViewById(R.id.tvnodata);

        searchbox = findViewById(R.id.searchbox);
        searchbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Places.initialize(ChoosenLocationActivity.this, "AIzaSyC-uwwT8OVmziS1kWqdL0NHnK1rhuPmynI");
                List<Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));
                Intent autocompleteIntent =
                        new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                                .build(ChoosenLocationActivity.this);
                startActivityForResult(autocompleteIntent, 3);
            }
        });
        curActivity = this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("TAG", "Place: " + place.getName());
                searchbox.setText("" + place.getName());

                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                MyLocationListener.postingLocation.setLatitude(latitude);
                MyLocationListener.postingLocation.setLongitude(longitude);

            }
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}



