package com.nearme.smartsell.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.AdHistory;
import com.nearme.smartsell.db.adshistory;
import com.nearme.smartsell.model.AdPayLoad;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by gopi.komanduri on 05/08/18.
 */

public class DataAdapter extends RecyclerView.Adapter {
    private List<AdHistory> adsList;
    private Context activity;
    private Boolean isActive;
    private HistoryFragment historyFragment;

    DataAdapter(List<AdHistory> students, RecyclerView recyclerView, Context context, Boolean isActive, HistoryFragment historyFragment) {
        adsList = students;
        this.activity = context;
        this.isActive = isActive;
        this.historyFragment = historyFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.historylayout, parent, false);
        vh = new adsViewHolder(v);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof adsViewHolder) {
            final AdHistory singleAd = adsList.get(position);
            try {

                Glide.with(activity).load(singleAd.imgUrl.replace("http", "https")).into(((adsViewHolder) holder).mAdImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
            ((adsViewHolder) holder).idValidTillHistory.setText(singleAd.tilldate + "/" + singleAd.tillmonth + "/" + singleAd.tillyear);
            ((adsViewHolder) holder).idOfferDescHistory.setText(singleAd.itemdesc);
            ((adsViewHolder) holder).idOfferCodeHistory.setText(singleAd.offercode);
            ((adsViewHolder) holder).currentAd = singleAd;
            (((adsViewHolder) holder).idLocation).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("geo:" + singleAd.lat + "," + singleAd.lng);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    activity.startActivity(mapIntent);
                }
            });

            if (isActive) {
                ((adsViewHolder) holder).ivFacebook.setVisibility(View.VISIBLE);
                ((adsViewHolder) holder).ivWhatsapp.setVisibility(View.VISIBLE);
            } else {
                ((adsViewHolder) holder).ivFacebook.setVisibility(View.GONE);
                ((adsViewHolder) holder).ivWhatsapp.setVisibility(View.GONE);
            }


            ((adsViewHolder) holder).ivWhatsapp.setOnClickListener(v -> historyFragment.whatsAppClick(singleAd.offercode,singleAd.itemdesc,singleAd.imgUrl));

            ((adsViewHolder) holder).ivFacebook.setOnClickListener(v -> historyFragment.facebookClick(singleAd.offercode,singleAd.itemdesc,singleAd.imgUrl));

            ((adsViewHolder) holder).ivInstagram.setOnClickListener(v -> historyFragment.instaClick(singleAd.offercode,singleAd.itemdesc,singleAd.imgUrl));

        }
    }

    @Override
    public int getItemCount() {
        return adsList.size();
    }

    //
    private class adsViewHolder extends RecyclerView.ViewHolder {
        private ImageView mAdImage;
        private TextView idValidTillHistory;
        private TextView idOfferDescHistory;
        private TextView idOfferCodeHistory;
        private TextView idLocation;
        private AdHistory currentAd;
        private ImageView ivWhatsapp, ivFacebook,ivInstagram;


        private adsViewHolder(View v) {
            super(v);
            idValidTillHistory = v.findViewById(R.id.idValidTillHistory);
            idOfferDescHistory = v.findViewById(R.id.idOfferDescHistory);
            idOfferCodeHistory = v.findViewById(R.id.idOfferCodeHistory);
            idLocation = v.findViewById(R.id.idLocation);
            mAdImage = v.findViewById(R.id.postedImage);
            ivFacebook = v.findViewById(R.id.ivFacebook);
            ivWhatsapp = v.findViewById(R.id.ivWhatsapp);
            ivInstagram = v.findViewById(R.id.ivInstagram);
        }
    }

}
