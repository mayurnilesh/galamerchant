package com.nearme.smartsell.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.SlotDateAdapter;
import com.nearme.smartsell.model.Merchantslot;
import com.nearme.smartsell.model.SlotData;
import com.nearme.smartsell.model.SlotPayload;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.PrefManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

public class FragmentAddSlots extends Fragment {

    private ManageSlotsActivity mActivity;
    View rv = null;
    TextView fromDate, toDate;
    private int year, month, day;
    TextView dateView;
    RecyclerView recTimeSlots;
    ArrayList<SlotData> slotDataArrayList = new ArrayList<>();
    SlotDateAdapter slotDateAdapter;
    LinearLayout linbtm;
    ArrayList<String> dateList = new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (ManageSlotsActivity) getActivity();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.activity_add_slots, container, false);
        initialization();
        clicks();
        return rv;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void clicks() {
        fromDate.setOnClickListener(v -> {
            toDate.setText("To Date");
            dateView = fromDate;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity, R.style.DateDialogTheme,
                    myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
            datePickerDialog.show();
        });

        toDate.setOnClickListener(v -> {

            if (fromDate.getText().equals("From Date")) {
                Toast.makeText(mActivity, "Please select From Date first", Toast.LENGTH_SHORT).show();
                return;
            }

            dateView = toDate;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity, R.style.DateDialogTheme,
                    myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
            datePickerDialog.show();
        });

        linbtm.setOnClickListener(v -> {
            try {
                if (slotDataArrayList.size() == 1) {
                    Toast.makeText(mActivity, "Please add slots", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (fromDate.getText().toString().equals("")) {
                    Toast.makeText(mActivity, "Please select from date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (toDate.getText().toString().equals("")) {
                    Toast.makeText(mActivity, "Please select to date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (checkDates()) {
                    Set<String> uniqueWords = new HashSet<String>(dateList);
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setTitle("Alert");
                    builder.setMessage("Please enter valid date. Below date are already registered\n " + uniqueWords.toString());
                    builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    return;
                }


                String s = fromDate.getText().toString();
                String e = toDate.getText().toString();
                PrefManager prefManager = new PrefManager(mActivity);
                List<String> fromTime = new ArrayList<>();
                List<String> toTime = new ArrayList<>();
                List<Integer> maxPeople = new ArrayList<>();

                for (int i = 1; i < slotDataArrayList.size(); i++) {
                    fromTime.add(slotDataArrayList.get(i).getFromTime());
                    toTime.add(slotDataArrayList.get(i).getToTime());
                    maxPeople.add(Integer.valueOf(slotDataArrayList.get(i).getSlotSize()));
                }

                SlotPayload slotPayload = new SlotPayload();
                slotPayload.FromDate = s;
                slotPayload.ToDate = e;
                slotPayload.MerchantID = prefManager.getMerchantID();
                slotPayload.FromTime = fromTime;
                slotPayload.ToTime = toTime;
                slotPayload.MaxToken = maxPeople;

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String slotData = new Gson().toJson(slotPayload);
                Call<Merchantslot[]> call = apiService.addSlots(slotData);
                call.enqueue(new retrofit2.Callback<Merchantslot[]>() {
                    @Override
                    public void onResponse(Call<Merchantslot[]> call, Response<Merchantslot[]> response) {
                        if (response.body() == null) {
                            Toast.makeText(mActivity, "Technical error occurred", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mActivity, "Slots registered successfully", Toast.LENGTH_SHORT).show();
                            mActivity.getSupportFragmentManager().popBackStack();
                        }
                    }

                    @Override
                    public void onFailure(Call<Merchantslot[]> call, Throwable t) {
                        Log.d("TAG", "onResponse: failure");
                    }
                });


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

    }

    private boolean checkDates() {
        for (int i = 0; i < dateList.size(); i++) {
            if (dateList.get(i).equalsIgnoreCase(fromDate.getText().toString())
                    || dateList.get(i).equalsIgnoreCase(toDate.getText().toString())) {
                return true;
            }
        }
        return false;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = (arg0, arg1, arg2, arg3) -> {
        try {
            if (dateView == toDate) {
                SimpleDateFormat sdfo = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
                Date d1 = sdfo.parse(fromDate.getText().toString());
                Date d2 = sdfo.parse((arg2 + 1) + "/" + arg3 + "/" + arg1);
                assert d1 != null;
                Log.d("TAG", ": " + d1.toString() + " " + d2.toString());
                if (d1.after(d2)) {
                    Toast.makeText(mActivity, "To date can't be before from date", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        showDate(arg1, arg2 + 1, arg3);
    };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(month).append("/")
                .append(day).append("/").append(year));
    }

    private void initialization() {

        dateList = getArguments().getStringArrayList("dateList");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        fromDate = rv.findViewById(R.id.fromDate);
        linbtm = rv.findViewById(R.id.linbtm);
        toDate = rv.findViewById(R.id.toDate);
        recTimeSlots = rv.findViewById(R.id.recTimeSlots);

        slotDataArrayList.add(new SlotData());

        fromDate.setText(new StringBuilder().append(month + 1).append("/")
                .append(day).append("/").append(year));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        recTimeSlots.setLayoutManager(linearLayoutManager);
        slotDateAdapter = new SlotDateAdapter(slotDataArrayList, new Callback() {
            @Override
            public void callBack() {
                final Dialog dialog = new Dialog(mActivity, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_add_slot);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

                LinearLayout addSlot = dialog.findViewById(R.id.addSlot);
                TextView startTime = dialog.findViewById(R.id.startTime);
                TextView endTime = dialog.findViewById(R.id.endTime);
                TextView noOfPeople = dialog.findViewById(R.id.noOfPeople);
                ImageView close = dialog.findViewById(R.id.close);
                addSlot.setOnClickListener(v -> {
                    if (startTime.getText().toString().equals("")) {
                        Toast.makeText(mActivity, "Please select start time", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (endTime.getText().toString().equals("")) {
                        Toast.makeText(mActivity, "Please select end time", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (noOfPeople.getText().toString().equals("")) {
                        Toast.makeText(mActivity, "Please enter no of peoples", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (Integer.parseInt(noOfPeople.getText().toString()) < 1) {
                        Toast.makeText(mActivity, "Please add minimum 1 people", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int sTime = Integer.parseInt(startTime.getText().toString().replace(":", ""));
                    int eTime = Integer.parseInt(endTime.getText().toString().replace(":", ""));

                    if (eTime <= sTime) {
                        Toast.makeText(mActivity, "End time must be later then start time", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!isValidate(sTime, eTime)) {
                        Toast.makeText(mActivity, "Please select correct time slot. This time slot already taken.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    SlotData slotData = new SlotData();
                    slotData.setToTime(endTime.getText().toString());
                    slotData.setFromTime(startTime.getText().toString());
                    slotData.setSlotSize(noOfPeople.getText().toString());
                    slotDataArrayList.add(slotData);
                    slotDateAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                });

                startTime.setOnClickListener(v -> {
                    final Calendar c = Calendar.getInstance();
                    @SuppressLint("SetTextI18n") TimePickerDialog timePickerDialog = new TimePickerDialog(mActivity, R.style.DateDialogTheme, (view, hourOfDay, minute) -> {
                        if (minute < 10)
                            startTime.setText(hourOfDay + ":0" + minute + ":00");
                        else
                            startTime.setText(hourOfDay + ":" + minute + ":00");
                    }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                });

                endTime.setOnClickListener(v -> {
                    final Calendar c = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(mActivity, R.style.DateDialogTheme, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            if (minute < 10)
                                endTime.setText(hourOfDay + ":0" + minute + ":00");
                            else
                                endTime.setText(hourOfDay + ":" + minute + ":00");
                        }
                    }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);

                    timePickerDialog.show();
                });

                close.setOnClickListener(v -> dialog.dismiss());
            }
        });
        recTimeSlots.setAdapter(slotDateAdapter);
    }

    public interface Callback {
        void callBack();
    }

    private boolean isValidate(int startTime, int endTime) {
        for (int i = 0; i < slotDataArrayList.size(); i++) {
            if (i == 0)
                continue;
            int sTime = Integer.parseInt(slotDataArrayList.get(i).getFromTime().replace(":", ""));
            int eTime = Integer.parseInt(slotDataArrayList.get(i).getToTime().replace(":", ""));
            if (sTime > startTime && sTime < endTime) {
                return false;
            }
            if (eTime < endTime && eTime > startTime) {
                return false;
            }
        }
        return true;
    }
    
}
