package com.nearme.smartsell.fragments;

public class list_item_base {
    public String imageUrl;
    public String validTill;
    public String shopName;
    public String offerCode;
    public String location;
    public String offerDescription;

    public list_item_base(String imageUrl, String validTill, String shopName, String offerCode, String location, String offerDescription) {
        this.imageUrl = imageUrl;
        this.validTill = validTill;
        this.shopName = shopName;
        this.offerCode = offerCode;
        this.location = location;
        this.offerDescription = offerDescription;
    }
}
