package com.nearme.smartsell.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;

import com.nearme.smartsell.R;

/**
 * Created by gopi.komanduri on 03/08/18.
 */

public class ChangeLanguageFragment extends DialogFragment {
    View rv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.layout_recylerview_list, container, false);

        return rv;

    }

}
