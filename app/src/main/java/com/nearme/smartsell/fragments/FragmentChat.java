package com.nearme.smartsell.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.ChatAdapter;
import com.nearme.smartsell.photoedit.PhotoEditImage;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.ChatMessage;
import com.nearme.smartsell.utility.PrefManager;
import com.nearme.smartsell.utility.StorageUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.collections.MapsKt;
import kotlin.jvm.functions.Function1;

public class FragmentChat extends Fragment {
    View rv = null;
    private ManageSlotsActivity mActivity;
    private final int RC_SELECT_IMAGE = 2;
    //    View view;
    private static final String TAG = "ChatFragment";
    //    Context context;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore firestore;
    ArrayList<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
    ListenerRegistration chatRegistration = null;
    String roomId = "126";
    ImageView send_btn;
    EditText edittext_chat;
    RecyclerView recyleView;
    ChatAdapter adapter;
    ImageView fab_send_image;
    private PrefManager prefManager;
    String UserName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ManageSlotsActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.chat_with_user, container, false);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        prefManager = new PrefManager(mActivity);

        Bundle bundle = getArguments();
        UserName = bundle.getString("name");
        roomId = prefManager.getMerchantID();

        firestore = FirebaseFirestore.getInstance();
        send_btn = (ImageView) rv.findViewById(R.id.button_send);
        edittext_chat = rv.findViewById(R.id.edittext_chat);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = edittext_chat.getText().toString();
                edittext_chat.setText("");
                Pair[] pairs = new Pair[]{new Pair("text", message),
                        new Pair("user", user.getUid()),
                        new Pair("timestamp", String.valueOf(System.currentTimeMillis() / 1000))};

                CollectionReference collectionReference = firestore.collection(roomId + "/" + UserName + "/messages");
                collectionReference.add(MapsKt.mapOf(pairs));
            }
        });
        fab_send_image = rv.findViewById(R.id.fab_send_image);
        fab_send_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/jpeg", "image/png"});
//                context.startActivity(intent);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), RC_SELECT_IMAGE);
            }
        });
        initList();

        return rv;
    }

    private void initList() {
        if (this.user != null) {
            recyleView = rv.findViewById(R.id.list_chat);
            recyleView.setLayoutManager(new LinearLayoutManager(mActivity));
            List chatList = this.chatMessages;
            String userID = this.user.getUid();
            adapter = new ChatAdapter(chatList, userID);
            recyleView = rv.findViewById(R.id.list_chat);
            recyleView.setAdapter(adapter);
            this.listenForChatMessages();
        }
    }

    @NotNull
    public final ArrayList getChatMessages() {
        return this.chatMessages;
    }

    private final void listenForChatMessages() {
//        this.roomId = this.getIntent().getStringExtra("INTENT_EXTRA_ROOMID");//TODO
        if (this.roomId == null) {
//            this.finish();
        } else {
            CollectionReference collreference = this.firestore.collection(roomId + "/" + UserName + "/messages");

            //main onedocument(UserName+"_"+user.getUid()).collection("messages")
            try {
                this.chatRegistration = collreference
                        .orderBy("timestamp").addSnapshotListener
                                (getEventListener());
            } catch (Exception ex) {

            }

        }
    }

    @NotNull
    private EventListener getEventListener() {

        try {
            return new EventListener() {

                // $FF: synthetic method
                // $FF: bridge method
                public void onEvent(Object var1, FirebaseFirestoreException var2) {
                    this.onEvent((QuerySnapshot) var1, var2);
                }

                public final void onEvent(@Nullable QuerySnapshot
                                                  messageSnapshot, @Nullable FirebaseFirestoreException exception) {
                    if (messageSnapshot != null && !messageSnapshot.isEmpty()) {
                        getChatMessages().clear();
                        Iterator var4 = messageSnapshot.getDocuments().iterator();
                        try {
                            while (var4.hasNext()) {
                                DocumentSnapshot messageDocument = (DocumentSnapshot) var4.next();
                                ArrayList var10000 = getChatMessages();
                                ChatMessage collreference = new ChatMessage();
                                Object var10003 = messageDocument.get("text");
                                if (var10003 == null) {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                                }

                                String textRecived = (String) var10003;
                                Object var10004 = messageDocument.get("user");
                                if (var10004 == null) {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                                }

                                String userName = (String) var10004;
                                Object timeStamp = messageDocument.get("timestamp");
                                if (timeStamp == null) {
                                    throw new TypeCastException("null cannot be cast to non-null type java.util.Date");
                                }
                                Object imgArray = messageDocument.get("image");
                                if (imgArray == null) {
//----------------TODO handle properly--------
                                } else {
                                    collreference.setImage((String) imgArray);
                                }
                                try {
                                    collreference.setText(textRecived);
                                    collreference.setUser(userName);
                                    collreference.setTimestamp(String.valueOf(timeStamp));
                                } catch (Exception ex) {

                                }
                                var10000.add(collreference);
                            }
                        } catch (Exception ex) {

                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }

                    }
                }

            };
        } catch (Exception ex) {
            return new EventListener() {
                @Override
                public void onEvent(@Nullable Object value, @Nullable FirebaseFirestoreException error) {

                }
            };
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in.
        // TODO: Add code to check if user is signed in.
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK &&
                data != null) {
            try {
                Uri selectedImagePath = data.getData();

//                Bitmap selectedImageBmp = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), selectedImagePath);
//
//                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//
//                selectedImageBmp.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
//                byte[] selectedImageBytes = outputStream.toByteArray();
//                String message = edittext_chat.getText().toString();
                edittext_chat.setText("");


                Bundle bundle = new Bundle();
                bundle.putString("image", selectedImagePath.toString());
                FragmentManager fm = mActivity.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                PhotoEditImage fragment = new PhotoEditImage(new ImageCallback(){
                    @Override
                    public void ImageResult(byte[] selectedImageBytes) {
                        storeImage(selectedImageBytes);
                    }
                });
                fragment.setArguments(bundle);
                ft.replace(R.id.rlContainer, fragment);
                ft.addToBackStack("Chat");
                ft.commit();



            } catch (Exception ex) {

            }
        }
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
    }


    public void storeImage(byte[] selectedImageBytes){
        StorageUtil.INSTANCE.uploadMessageImage(selectedImageBytes, (Function1) (new Function1() {
            // $FF: synthetic method
            // $FF: bridge method
            public Object invoke(Object var1) {
                this.invoke((String) var1);
                return Unit.INSTANCE;
            }

            public final void invoke(@NotNull String imagePath) {
                Pair[] pairs = new Pair[]{new Pair("text", ""),
                        new Pair("user", user.getUid()),
                        new Pair("timestamp", String.valueOf(System.currentTimeMillis() / 1000)),
                        new Pair("image", imagePath)};
                CollectionReference collectionReference = firestore.collection(roomId + "/" + UserName + "/messages");
//                        collectionReference.document(UserName+"_"+user.getUid()).collection("messages");
                collectionReference.add(MapsKt.mapOf(pairs));
            }
        }));
    }

   public interface ImageCallback{
        public void ImageResult(byte[] selectedImageBytes);
    }

}
