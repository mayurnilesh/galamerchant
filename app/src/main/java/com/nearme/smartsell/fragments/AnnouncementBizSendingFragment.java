package com.nearme.smartsell.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CategoryAdaptor;
import com.nearme.smartsell.adapter.ImageListAdapter;
import com.nearme.smartsell.adapter.MultiSelectorAdapter;
import com.nearme.smartsell.db.Categories;
import com.nearme.smartsell.db.SelectedCategory;
import com.nearme.smartsell.model.CategoryInfo;
import com.nearme.smartsell.model.MerchantEvents;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.CategoriesPayLoad;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.startup.MapsAdsActivity;
import com.nearme.smartsell.startup.PhoneActivity;
import com.nearme.smartsell.utility.ImageUploader;
import com.nearme.smartsell.utility.PrefManager;
import com.nearme.smartsell.utility.Utils;
import com.squareup.picasso.Picasso;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Utility;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;


public class AnnouncementBizSendingFragment extends DialogFragment {

    private AnnouncementBizSendingFragment thisObj;
    private Activity activity;
    private ImageView imgsel;
    public static String mImageUrl = "";
    public static String mOfferImageUrl = "";
    private ProgressBar mProgressBar;
    private Integer tilldate;
    private Integer tillmonth;
    private Integer tillyear;
    private EditText offercode;
    private EditText offerdesc;
    private EditText quantity;
    private EditText price;
    private Spinner ExclusivePostsSpinner;
    private CheckBox postToOtherLocChkBox;
    private CheckBox postContactChkBox;
    private EditText minBusiness;
    private TextView mapSearch, tvNegotiation, selectedCate;
    public Integer negotiate = 0;
    public Integer minBiz = 0;
    private EditText postContactEdtTxt;
    private PrefManager prefManager;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    private CheckBox cbnegotiation;
    private Gson gson;
    static Uri imageUri = null;
    private String geoHash;
    private String lat, lng;
    private String realPath = "";
    private Spinner spCategory;
    final String IMAGE_DIRECTORY = "/Gala";
    public final static int QRcodeWidth = 500;
    @SuppressLint("AuthLeak")
    Cloudinary cloudinary = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");
    private ImageView uploadVideo;
    private RecyclerView rec_image;
    private List<String> imageList = new ArrayList<>();
    private ImageListAdapter imageListAdapter;
    private List<Categories> categories;
    private List<Categories> choosedList = new ArrayList<>();
    private List<SelectedCategory> selectedCategories;
    private ArrayList<String> spinnerList = new ArrayList<>();


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE) {
                List<String> mPaths = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
                Log.d("TAG", "onActivityResult: " + mPaths.toString());
                if (mPaths.size() > 0) {
                    Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mPaths.get(0),
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    uploadVideo.setImageBitmap(thumb);
                    File file = new File(mPaths.get(0));
                    long length = file.length();
                    length = length / 1024;
                    Toast.makeText(activity, "Video size:" + length / 1024 + "MB",
                            Toast.LENGTH_LONG).show();
                }
                return;
                //Your Code
            }
            if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE) {
                List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
                if (mPaths.size() > 0) {
                    imageList.add(mPaths.get(0));
                    if (imageList.size() == 7) {
                        imageList.remove(0);
                    }
                }
                imageListAdapter.notifyDataSetChanged();
                return;
            }
            if (data != null && data.getData() != null) {
                Uri uri = data.getData();
                imageUri = uri;
                realPath = imageUri.toString();
                Picasso.with(activity).load(imageUri).resize(getScreenWidth() / 2, getScreenHeight() / 2)
                        .centerInside().into(imgsel);
            }
        } else if (resultCode == 15) {
            mapSearch.setText("");
        }

    }

    private static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    private static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public void setCategory() {
        String category = "";
        for (int i = 0; i < choosedList.size(); i++) {
            if (choosedList.get(i).isSelected()) {
                category = category + choosedList.get(i).getCatname() + ", ";
            }
        }
        selectedCate.setText(category);
    }

    public List<Categories> getSelectedCategoryArray() {
        List<Categories> tmpArray = new ArrayList();
        for (int i = 0; i < choosedList.size(); i++) {
            if (choosedList.get(i).isSelected()) {
                tmpArray.add(choosedList.get(i));
            }
        }
        return tmpArray;
    }

    private Spinner spEvents;
    private ArrayList<MerchantEvents> merchantEventsList = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.announcementsbiz, container, false);
        activity = getActivity();

        initialization(rootView);
        clicks(rootView);

        mProgressBar.setVisibility(View.INVISIBLE);
        prefManager = new PrefManager(activity.getApplicationContext());

        gson = new Gson();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);


        if (thisObj == null)
            thisObj = this;

        setImageData();
        setupCategory();
        getMerchantEvents();

        return rootView;
    }

    private void getMerchantEvents() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        String merchantId = prefManager.getMerchantID();

        apiInterface.getMerchantEvents(merchantId).enqueue(new Callback<MerchantEvents[]>() {
            @Override
            public void onResponse(Call<MerchantEvents[]> call, Response<MerchantEvents[]> response) {
                Log.d(TAG, "onResponse: events" + response.body().length);
                if (response.body() != null) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add("No event selected");
                    for (int i = 0; i < response.body().length; i++) {
                        String eventCondition = response.body()[i].EventCondition;
                        list.add(getModifiedString(eventCondition));
                        merchantEventsList.add(response.body()[i]);
                    }

                    spEvents.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, list));
                }
            }

            @Override
            public void onFailure(Call<MerchantEvents[]> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                ArrayList<String> list = new ArrayList<>();
                list.add("No event found");
                spEvents.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, list));
            }
        });
    }

    private String getModifiedString(String eventCondition) {
        if (eventCondition.contains("sex") && eventCondition.contains("1")) {
            return "Sex is male";
        } else if (eventCondition.contains("sex") && eventCondition.contains("2")) {
            return "Sex is female";
        } else if (eventCondition.contains("LIKE")) {
            return eventCondition.replace("LIKE", "like").replace("%", "").replace("'", "");
        } else if (eventCondition.contains("BETWEEN")) {
            return eventCondition.replace("BETWEEN", "between").replace("%", "").replace("'", "");
        } else
            return eventCondition.replace("'", "");

    }

    private void setupCategory() {
        selectedCategories = SelectedCategory.listAll(SelectedCategory.class);
        categories = Categories.listAll(Categories.class);
        if (categories.size() == 0) {
            getCategories();
        } else {
            if (selectedCategories.size() == 0) {
                selectedCategory();
            }
        }
        spinnerList.clear();

        if (selectedCategories.size() > 0 && categories.size() > 0) {
            choosedList.clear();
            String tmpSelectedCategory = selectedCategories.get(0).selectedValue;
            if (tmpSelectedCategory == null || tmpSelectedCategory.equalsIgnoreCase("") || tmpSelectedCategory.length() < 3) {
                selectedCate.setText(categories.get(0).catname);
                choosedList.add(categories.get(0));
                choosedList.get(0).selected = true;
                final MultiSelectorAdapter adapterTagSpinnerItem = new MultiSelectorAdapter(AnnouncementBizSendingFragment.this, activity, 0, choosedList, spCategory);
                spCategory.setAdapter(adapterTagSpinnerItem);
                return;
            }
            String bit = Long.toBinaryString(Long.parseLong(tmpSelectedCategory.substring(1, tmpSelectedCategory.length() - 1)));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(bit);
            char[] selectedCategoriesInBits = stringBuilder.reverse().toString().toCharArray();
            for (int i = 0; i < selectedCategoriesInBits.length; i++) {
                if (String.valueOf(selectedCategoriesInBits[i]).equalsIgnoreCase("1")) {
                    choosedList.add(categories.get(i));
                }
            }
            Log.d("TAG", "tempMethod: " + bit);
            // spCategory.setAdapter(new ArrayAdapter(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_dropdown_item, spinnerList));
            final MultiSelectorAdapter adapterTagSpinnerItem = new MultiSelectorAdapter(AnnouncementBizSendingFragment.this, activity, 0, choosedList, spCategory);
            selectedCate.setText(choosedList.get(0).catname);
            choosedList.get(0).selected = true;
            spCategory.setAdapter(adapterTagSpinnerItem);
        }
    }

    private void getCategories() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesPayLoad[]> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesPayLoad[]>() {
            @Override
            public void onResponse(Call<CategoriesPayLoad[]> call, Response<CategoriesPayLoad[]> response) {
                CategoriesPayLoad[] categoriesPayLoad = response.body();
                if (categoriesPayLoad != null) {
                    for (CategoriesPayLoad aCategoriesPayLoad : categoriesPayLoad) {
                        Categories categories = new Categories();
                        categories.catid = aCategoriesPayLoad.catid;
                        categories.catimg = aCategoriesPayLoad.catimg;
                        categories.catname = aCategoriesPayLoad.catname;
                        categories.save();
                    }
                }
                selectedCategory();
            }

            @Override
            public void onFailure(Call<CategoriesPayLoad[]> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    private void selectedCategory() {
        PrefManager prefManager = new PrefManager(getActivity());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.d("TAG", "onResponse1: " + prefManager.getMerchantID());
        Call<String> call = apiService.getMyCategories(prefManager.getMerchantID());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                SelectedCategory selectedCategory = new SelectedCategory();
                selectedCategory.selectedValue = response.body();
                selectedCategory.save();
                Log.d("TAG", "onResponse: pass " + response.body());
                setupCategory();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailureConstan: " + t.getMessage());
            }
        });
    }

    private void setImageData() {
        imageList.clear();
        imageList.add("");
        GridLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 3);
        rec_image.setLayoutManager(linearLayoutManager);
        rec_image.setItemAnimator(new DefaultItemAnimator());
        imageListAdapter = new ImageListAdapter(imageList, activity);
        rec_image.setAdapter(imageListAdapter);
    }

    private void initialization(View rootView) {
        rec_image = rootView.findViewById(R.id.rec_image);
        spEvents = rootView.findViewById(R.id.spEvents);
        uploadVideo = rootView.findViewById(R.id.uploadVideo);
        spCategory = rootView.findViewById(R.id.spCategory);
        imgsel = rootView.findViewById(R.id.btnCapturePicture_offer);
        cbnegotiation = rootView.findViewById(R.id.cbnegotiation);
        postContactEdtTxt = rootView.findViewById(R.id.postContactEdtTxt);
        mapSearch = rootView.findViewById(R.id.MapSearch);
        tvNegotiation = rootView.findViewById(R.id.tvNegotiation);
        offercode = rootView.findViewById(R.id.offer_code);
        offerdesc = rootView.findViewById(R.id.offer_desc);
        price = rootView.findViewById(R.id.price);
        quantity = rootView.findViewById(R.id.quantity);
        mProgressBar = rootView.findViewById(R.id.progress);
        dateView = rootView.findViewById(R.id.exp_date);
        minBusiness = rootView.findViewById(R.id.MinimumbusinessValue);
        postContactChkBox = rootView.findViewById(R.id.postContactChkBox);
        ExclusivePostsSpinner = rootView.findViewById(R.id.ExclusivePostsSpinner);
        postToOtherLocChkBox = rootView.findViewById(R.id.PostToOtherLoc);//PostExclusive ExclusivePostsSpinner
        selectedCate = rootView.findViewById(R.id.selectedCate);//PostExclusive ExclusivePostsSpinner

        rootView.findViewById(R.id.selectedCate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spCategory.performClick();
            }
        });

    }

    private void clicks(View rootView) {

        uploadVideo.setOnClickListener(v -> {
//            new VideoPicker.Builder(activity)
//                    .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                    .directory(VideoPicker.Directory.DEFAULT)
//                    .extension(VideoPicker.Extension.MP4)
//                    .enableDebuggingMode(true)
//                    .build()
        });

        ((CheckBox) rootView.findViewById(R.id.negotiatechkbox)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                minBusiness.setVisibility(View.VISIBLE);
                negotiate = 1;
            } else {
                minBusiness.setVisibility(View.GONE);
                negotiate = 0;
            }
        });
        ((CheckBox) rootView.findViewById(R.id.PostExclusive)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ExclusivePostsSpinner.setVisibility(View.VISIBLE);
                negotiate = 1;
            } else {
                ExclusivePostsSpinner.setVisibility(View.GONE);
                negotiate = 0;
            }
        });
        postContactChkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                postContactEdtTxt.setVisibility(View.VISIBLE);
            } else {
                postContactEdtTxt.setVisibility(View.GONE);
            }
        });
        postToOtherLocChkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ((ManageSlotsActivity) getActivity()).getLocation();
                cbnegotiation.setChecked(false);
                cbnegotiation.setEnabled(false);
                cbnegotiation.setAlpha(0.5f);
                tvNegotiation.setAlpha(0.5f);
            } else {
                mapSearch.setText("");
                cbnegotiation.setEnabled(true);
                cbnegotiation.setAlpha(1f);
                tvNegotiation.setAlpha(1f);
            }

        });
        rootView.findViewById(R.id.dateImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                        myDateListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
        imgsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .setActivityTitle(getResources().getString(R.string.app_name))
//                        .setCropShape(CropImageView.CropShape.RECTANGLE)
//                        .setCropMenuCropButtonTitle("Done")
//                        .setRequestedSize(400, 400)
//                        .setOutputCompressQuality(50)
//                        .start(getContext(), AnnouncementBizSendingFragment.this);
                com.github.dhaval2404.imagepicker.ImagePicker.Companion.with(AnnouncementBizSendingFragment.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(1024)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });

        rootView.findViewById(R.id.buttonUploadImage).setOnClickListener(view -> {
            if (realPath.length() <= 0) {
                Toast.makeText(activity.getBaseContext(), "Please select Image to send", Toast.LENGTH_LONG).show();
            } else if (getSelectedCategoryArray().size() == 0) {
                Toast.makeText(activity.getBaseContext(), "Please select at least one category", Toast.LENGTH_LONG).show();
            } else {
                postAd();
            }
        });
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    tillyear = arg1;
                    tillmonth = arg2 + 1;
                    tilldate = arg3;
                    showDate(arg1, arg2 + 1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(getString(R.string.exp)).append(day).append("/")
                .append(month).append("/").append(year));
    }

    public void setLocation(Intent data) {
        try {
            mapSearch.setText(data.getStringExtra("address"));
            lat = String.valueOf(data.getExtras().getDouble("lat"));
            lng = String.valueOf(data.getExtras().getDouble("lng"));
            geoHash = com.nearme.smartsell.geohashutil.GeoHash.fromCoordinates(data.getExtras().getDouble("lat"), data.getExtras().getDouble("lng"), 5).toString();
        } catch (Exception e) {
            lat = null;
            lng = null;
            geoHash = null;
        }
    }

    private void postAd() {

        if (offercode.getText().toString().trim().equalsIgnoreCase("")) {
            offercode.setError("Enter Offer Code");
            offercode.requestFocus();
            return;
        }

        if (offerdesc.getText().toString().trim().equalsIgnoreCase("")) {
            offerdesc.setError("Enter Offer Description");
            offerdesc.requestFocus();
            return;
        }

        if (quantity.getText().toString().trim().equalsIgnoreCase("")) {
            quantity.setError("Enter Quantity");
            quantity.requestFocus();
            return;
        }

        if (price.getText().toString().trim().equalsIgnoreCase("")) {
            price.setError("Enter Quantity");
            price.requestFocus();
            return;
        }

        if (!postContactChkBox.isChecked()) {
            uploadImg();
        } else {
            String merchantId = prefManager.getMerchantID();
            if (merchantId != null) {
                Object[] params = new Object[9];
                params[0] = cloudinary;
                params[1] = imageUri;
                params[2] = ObjectUtils.emptyMap();
                params[3] = getActivity();
                params[4] = 2; // condition typeReceived 1 for bills
                params[5] = merchantId;
                params[6] = postContactEdtTxt.getText().toString();
                params[7] = 0;
                params[8] = 0;
                new ImageUploader(getContext()).execute(params);
            } else {
                Toast.makeText(activity.getApplicationContext(), "Failed to Send to Contact", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadImg() {
        String merchantId = prefManager.getMerchantID();// temp.get(0).merchantId;// +"_"+temp.get(0).ContactNumber;

        Object[] params = new Object[21];

        if (spEvents.getSelectedItemPosition() == 0) {
            params[18] = -1;
        } else {
            params[18] = merchantEventsList.get(spEvents.getSelectedItemPosition() - 1).EventID;
        }

        params[19] = quantity.getText().toString();
        params[20] = price.getText().toString();

        params[0] = cloudinary;
        params[1] = imageUri;
        params[2] = "";
        params[3] = getActivity();
        params[4] = 1; // condition typeReceived
        if (tilldate == null) {
            calendar = Calendar.getInstance();
            tillyear = calendar.get(Calendar.YEAR);

            tillmonth = calendar.get(Calendar.MONTH) + 1;
            tilldate = calendar.get(Calendar.DAY_OF_MONTH);
        }
        params[5] = tilldate; // offer valid till
        params[6] = tillmonth; // offer valid till
        params[7] = tillyear; // offer valid till
        String value = Utils.getCategoryDecimal(categories, getSelectedCategoryArray());
        Log.d(TAG, "clicks: " + value);

        params[8] = value; // category
        params[9] = offercode.getText().toString(); //offercode
        params[10] = offerdesc.getText().toString(); //item description
        params[11] = merchantId;
        params[12] = negotiate;
        params[13] = minBiz;

        if (cbnegotiation.isChecked()) {
            params[14] = 1;
        } else {
            params[14] = 0;
        }

        if (negotiate == 1) {
            try {
                params[13] = Integer.valueOf(minBusiness.getText().toString());
            } catch (NumberFormatException e) {
                params[13] = 0;
            }
//            params[13] = Integer.valueOf(minBusiness.getText().toString());
        }

        if (postToOtherLocChkBox.isChecked()) {

            if (lat == null || lng == null || geoHash == null) {
                Toast.makeText(activity, "please select post to location", Toast.LENGTH_SHORT).show();
                return;
            }
            params[2] = "yes";
            params[15] = lat;
            params[16] = lng;
            params[17] = geoHash;
        } else {
            params[2] = "no";
        }


        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        mProgressBar.setVisibility(View.VISIBLE);
        new ImageUploader(getContext()).execute(params);
    }

}

