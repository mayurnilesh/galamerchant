package com.nearme.smartsell.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.gson.Gson;
import com.nearme.smartsell.BuildConfig;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.AdHistory;
import com.nearme.smartsell.db.Categories;
import com.nearme.smartsell.db.SelectedCategory;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.CategoriesPayLoad;
import com.nearme.smartsell.rest.HistoryResponsePayload;
import com.nearme.smartsell.rest.LastReceivedAdStruct;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class HistoryFragment extends DialogFragment {
    private RecyclerView mRecycleView;
    private List<AdHistory> postedAds = new ArrayList<>();
    private final List<AdHistory> tmpArray = new ArrayList<>();
    private DataAdapter historyAdapter;
    private TextView tvEmptyView;
    List<AdHistory> req;
    private boolean isCurrent = true;
    View rv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.layout_recylerview_list, container, false);
        mRecycleView = rv.findViewById(R.id.recyclerview);
        CheckBox isActive = rv.findViewById(R.id.isActive);
        tvEmptyView = rv.findViewById(R.id.empty_view);

        setupRecyclerView();

        isActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            isCurrent = isChecked;
            currentAds();
            historyAdapter.notifyDataSetChanged();
        });
        return rv;
    }

    private void setupRecyclerView() {
        setAdapter();

        req = AdHistory.find(AdHistory.class,
                null, null, "geo", null, null);
        setupCategory();
    }

    private void setAdapter() {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecycleView.setLayoutManager(layoutManager);
        postedAds = AdHistory.listAll(AdHistory.class);
        currentAds();
        historyAdapter = new DataAdapter(tmpArray, mRecycleView, getActivity(), isCurrent, this);
        mRecycleView.setAdapter(historyAdapter);

        if (postedAds.isEmpty()) {
            mRecycleView.setVisibility(View.GONE);
            tvEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRecycleView.setVisibility(View.VISIBLE);
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    private void currentAds() {
        tmpArray.clear();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date d = new Date();
            for (int i = 0; i < postedAds.size(); i++) {
                Date strDate = sdf.parse(sdf.format(d));
                Date endDate;
                if (postedAds.get(i).tilldate == null) {
                    endDate = strDate;
                } else {
                    endDate = sdf.parse(postedAds.get(i).tilldate + "/" + postedAds.get(i).tillmonth + "/" + postedAds.get(i).tillyear);
                }
                if (isCurrent) {
                    if (!endDate.before(strDate)) {
                        tmpArray.add(postedAds.get(i));
                    }
                } else {
                    if (endDate.before(strDate)) {
                        tmpArray.add(postedAds.get(i));
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setupCategory() {
        List<SelectedCategory> selectedCategories = SelectedCategory.listAll(SelectedCategory.class);
        List<Categories> categories = Categories.listAll(Categories.class);
        if (categories.size() == 0) {
            getCategories();
        } else {
            if (selectedCategories.size() == 0) {
                selectedCategory();
            } else {
                getHistory();
            }
        }

    }

    private void getCategories() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesPayLoad[]> call = apiService.getCategories();
        call.enqueue(new Callback<CategoriesPayLoad[]>() {
            @Override
            public void onResponse(Call<CategoriesPayLoad[]> call, Response<CategoriesPayLoad[]> response) {
                CategoriesPayLoad[] categoriesPayLoad = response.body();
                if (categoriesPayLoad != null) {
                    for (CategoriesPayLoad aCategoriesPayLoad : categoriesPayLoad) {
                        Categories categories = new Categories();
                        categories.catid = aCategoriesPayLoad.catid;
                        categories.catimg = aCategoriesPayLoad.catimg;
                        categories.catname = aCategoriesPayLoad.catname;
                        categories.save();
                    }
                }
                selectedCategory();
            }

            @Override
            public void onFailure(Call<CategoriesPayLoad[]> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    private void selectedCategory() {
        PrefManager prefManager = new PrefManager(getActivity());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.d("TAG", "onResponse1: " + prefManager.getMerchantID());
        Call<String> call = apiService.getMyCategories(prefManager.getMerchantID());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                SelectedCategory selectedCategory = new SelectedCategory();
                selectedCategory.selectedValue = response.body();
                selectedCategory.save();
                Log.d("TAG", "onResponse: pass " + response.body());
                setupCategory();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailureConstan: " + t.getMessage());
            }
        });
    }


    private void getHistory() {
        Log.d("+++++++++++++++++", "getHistory: history api called");
        List<Categories> choosedList = new ArrayList<>();
        List<SelectedCategory> selectedCategories = SelectedCategory.listAll(SelectedCategory.class);
        List<Categories> categories = Categories.listAll(Categories.class);

        if (selectedCategories.size() > 0 && categories.size() > 0) {
            String tmpSelectedCategory = selectedCategories.get(0).selectedValue != null ? selectedCategories.get(0).selectedValue : "";
            if (tmpSelectedCategory.equalsIgnoreCase("") || tmpSelectedCategory.length() < 3) {
                choosedList.add(categories.get(0));
            } else {
                String bit = Long.toBinaryString(Long.parseLong(tmpSelectedCategory.substring(1, tmpSelectedCategory.length() - 1)));
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(bit);
                char[] selectedCategoriesInBits = stringBuilder.reverse().toString().toCharArray();
                for (int i = 0; i < selectedCategoriesInBits.length; i++) {
                    if (String.valueOf(selectedCategoriesInBits[i]).equalsIgnoreCase("1")) {
                        choosedList.add(categories.get(i));
                    }
                }
            }
        }

        List<LastReceivedAdStruct> adStructs = new ArrayList<>();
        List<String> geoHashes = new ArrayList<String>();
        geoHashes.add(new PrefManager(getActivity()).getMerchantGeoHash());
        for (int i = 0; i < choosedList.size(); i++) {
            LastReceivedAdStruct lastReceivedAdStruct = new LastReceivedAdStruct();
            lastReceivedAdStruct.geoHash = geoHashes;
            lastReceivedAdStruct.lastReceivedAdId = getLastId(choosedList.get(i).catid);
            lastReceivedAdStruct.Category = (choosedList.get(i).catid - 1);
            adStructs.add(lastReceivedAdStruct);
        }

        String geoHashData = new Gson().toJson(adStructs);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<HistoryResponsePayload[]> getAppsHistory = apiService.getAdsHistory(geoHashData, new PrefManager(getActivity()).getMerchantID());

        getAppsHistory.enqueue(new Callback<HistoryResponsePayload[]>() {
            @Override
            public void onResponse(Call<HistoryResponsePayload[]> call, Response<HistoryResponsePayload[]> response) {
                if (response.body() != null) {
                    HistoryResponsePayload[] historyAds = response.body();
                    for (HistoryResponsePayload historyAd : historyAds) {
                        AdHistory adHistory = new AdHistory();
                        adHistory.adId = historyAd.Id;
                        adHistory.geo = historyAd.geo;
                        adHistory.lat = historyAd.lat;
                        adHistory.lng = historyAd.lng;
                        adHistory.merchantid = historyAd.merchantid;
                        adHistory.cat = historyAd.cat;
                        adHistory.tilldate = historyAd.tilldate;
                        adHistory.tillmonth = historyAd.tillmonth;
                        adHistory.tillyear = historyAd.tillyear;
                        adHistory.fromdate = historyAd.fromdate;
                        adHistory.frommonth = historyAd.frommonth;
                        adHistory.fromyear = historyAd.fromyear;
                        adHistory.offercode = historyAd.offercode;
                        adHistory.itemdesc = historyAd.itemdesc;
                        adHistory.imgUrl = historyAd.imgUrl;
                        adHistory.shopname = historyAd.shopname;
                        adHistory.shopDp = historyAd.shopDp;
                        adHistory.negotiate = historyAd.negotiate;
                        adHistory.minBusiness = historyAd.minBusiness;
                        adHistory.mindiscount = historyAd.mindiscount;
                        adHistory.discdesc = historyAd.discdesc;
                        adHistory.maxdiscount = historyAd.maxdiscount;
                        adHistory.save();
                    }

                }
                setAdapter();
            }

            @Override
            public void onFailure(Call<HistoryResponsePayload[]> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
            }
        });

    }

    private int getLastId(int catId) {
        int maxId = -1;
        for (int i = 0; i < postedAds.size(); i++) {
            String cat = postedAds.get(i).getCat();
            String bit = Long.toBinaryString(Long.parseLong(cat));
            String mainBit = new StringBuilder(bit).reverse().toString();
            if (mainBit.length() >= catId) {
                if (Integer.parseInt(String.valueOf(mainBit.charAt(catId - 1))) == 1) {
                    if (postedAds.get(i).getAdId() > maxId) {
                        maxId = postedAds.get(i).getAdId();
                    }
                }
            }
        }
        return maxId;
    }

    public void whatsAppClick(String title, String description,String imageUrl) {

        Picasso.with(getActivity()).load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Uri imageUri = getBitmapFromView(bitmap);
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, title + "\n" + description + "\n" + "https://play.google.com/store/apps/details?id=com.nearme.smartbuy");
                whatsappIntent.setType("image/*");
                whatsappIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    getActivity().startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Log.d("TAG", "onClick: " + ex.getMessage());
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });


    }

    public void instaClick(String title, String description,String imageUrl){
        Picasso.with(getActivity()).load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Uri imageUri = getBitmapFromView(bitmap);
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.instagram.android");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, title + "\n" + description + "\n" + "https://play.google.com/store/apps/details?id=com.nearme.smartbuy");
                whatsappIntent.setType("image/*");
                whatsappIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    getActivity().startActivity(whatsappIntent);
                } catch (Exception ex) {
                    Log.d("TAG", "onClick: " + ex.getMessage());
                    Toast.makeText(getActivity(), "Instagram app not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    public void facebookClick(String title, String description,String imageUrl) {
        Picasso.with(getActivity()).load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Uri imageUri = getBitmapFromView(bitmap);
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                whatsappIntent.setType("image/*");
                whatsappIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, title + "\n" + description + "\n" + "https://play.google.com/store/apps/details?id=com.nearme.smartbuy");
                whatsappIntent.setPackage("com.instagram.android");
                whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    getActivity().startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Log.d("TAG", "onClick: " + ex.getMessage());
                    Toast.makeText(getActivity(), "Facebook app not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private Uri getBitmapFromView(Bitmap bitmap) {
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            Boolean  isTrue = bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            return FileProvider.getUriForFile(getActivity(), "com.nearme.smartsell.provider", file);
        } catch (Exception e) {
            Log.d(TAG, "getBitmapFromView: " + e.getMessage());
        }
        return null;
    }

}
