package com.nearme.smartsell.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.NegotiationAdapter;
import com.nearme.smartsell.db.negotationrequests;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.NegotationPayLoad;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.DateTime;
import com.nearme.smartsell.utility.PrefManager;
import com.nearme.smartsell.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopi.komanduri on 03/08/18.
 */

public class NegotiationFragment extends DialogFragment {
    private  ManageSlotsActivity mActivity;
    View rv = null;
    Context context;
    RecyclerView rec_job;
    List<negotationrequests> negotationrequestsList = new ArrayList<>();
    List<negotationrequests> tmpList = new ArrayList<>();
    String TAG = "NegotiationFragment";
    ProgressBar pb;
    PrefManager prefManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this.getContext());
        mActivity = (ManageSlotsActivity) getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.fragment_negotiation, container, false);
        pb = rv.findViewById(R.id.pb);

        mainInit();
        fetchNegotiations();

        return rv;
    }


    private void mainInit() {
        tmpList = negotationrequests.listAll(negotationrequests.class);
        Log.d(TAG, "onCreateView: " + tmpList.size());
        negotationrequestsList.clear();

        for (int i = 0; i < tmpList.size(); i++) {
            if (checkDuplicate(tmpList.get(i).idnegotations))
                negotationrequestsList.add(tmpList.get(i));
        }
        initialize();
    }

    private boolean checkDuplicate(Integer idnegotations) {

        for (int i = 0; i < negotationrequestsList.size(); i++) {
            if (negotationrequestsList.get(i).idnegotations.equals(idnegotations))
                return false;
        }
        return true;
    }

    private void initialize() {
        rec_job = rv.findViewById(R.id.rec_job);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_job.setLayoutManager(linearLayoutManager);
        rec_job.setItemAnimator(new DefaultItemAnimator());
        NegotiationAdapter apercuAdapter = new NegotiationAdapter(negotationrequestsList);
        rec_job.setAdapter(apercuAdapter);

        rec_job.addOnItemTouchListener(new RecyclerTouchListener(mActivity, rec_job, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                ImageView thumsup = view.findViewById(R.id.thumsup);
                ImageView thumsdown = view.findViewById(R.id.thumsdown);

                thumsup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!negotationrequestsList.get(position).getResponded().equals("-1")) {
                            Toast.makeText(mActivity, "already responded", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        final Dialog dialog = new Dialog(mActivity, R.style.Theme_CustomDialog);
                        dialog.setContentView(R.layout.dialogue_discount);
                        dialog.show();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.setCancelable(true);

                        dialog.show();

                        dialog.findViewById(R.id.tvsubmit).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                negotationrequests _nego = negotationrequestsList.get(position);
                                _nego.responded = "1";
                                _nego.save();

                                if (((EditText) dialog.findViewById(R.id.edtdiscount)).getText().toString().equalsIgnoreCase("")
                                        || Integer.parseInt(((EditText) dialog.findViewById(R.id.edtdiscount)).getText().toString()) < 0) {
                                    ((EditText) dialog.findViewById(R.id.edtdiscount)).setError("Enter advance amount");
                                    return;
                                }

                                pb.setVisibility(View.VISIBLE);

                                NegotationPayLoad npl = new NegotationPayLoad();

                                npl.customercontact = negotationrequestsList.get(position).getCustomercontact();
                                npl.merchantid = "";
                                npl.geohash = "";
                                npl.minamount = negotationrequestsList.get(position).getMinamount();
                                npl.maxamount = negotationrequestsList.get(position).getMaxamount();
                                npl.DiscountExpectation = negotationrequestsList.get(position).getDiscountexpectation();
                                npl.ShoppingProbableDates = negotationrequestsList.get(position).getShoppingprobabledates();
                                npl.description = negotationrequestsList.get(position).getDescription();
                                npl.delieverd = 0;
                                npl.response = 1;
                                npl.notificationid = negotationrequestsList.get(position).getNotificationid();
                                npl.idnegotations = negotationrequestsList.get(position).getIdnegotations();
                                npl.advance = Integer.valueOf(((EditText) dialog.findViewById(R.id.edtdiscount)).getText().toString());
                                npl.respondedOn = "";

                                sendNegotiationResponse(1, negotationrequestsList.get(position).getIdnegotations(), npl);
                                dialog.dismiss();
                                mainInit();
                            }
                        });


                    }
                });

                thumsdown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!negotationrequestsList.get(position).getResponded().equals("-1")) {
                            Toast.makeText(mActivity, "already responded", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        pb.setVisibility(View.VISIBLE);

                        negotationrequests _nego = negotationrequestsList.get(position);
                        _nego.responded = "0";
                        _nego.save();

                        NegotationPayLoad npl = new NegotationPayLoad();

                        npl.customercontact = negotationrequestsList.get(position).getCustomercontact();
                        npl.merchantid = "";
                        npl.geohash = "";
                        npl.minamount = negotationrequestsList.get(position).getMinamount();
                        npl.maxamount = negotationrequestsList.get(position).getMaxamount();
                        npl.DiscountExpectation = negotationrequestsList.get(position).getDiscountexpectation();
                        npl.ShoppingProbableDates = negotationrequestsList.get(position).getShoppingprobabledates();
                        npl.description = negotationrequestsList.get(position).getDescription();
                        npl.delieverd = 0;
                        npl.response = 2;
                        npl.notificationid = negotationrequestsList.get(position).getNotificationid();
                        npl.idnegotations = negotationrequestsList.get(position).getIdnegotations();
                        npl.advance = 0; //
                        npl.respondedOn = "";

                        sendNegotiationResponse(1, negotationrequestsList.get(position).getIdnegotations(), npl);
                        mainInit();
                    }
                });

            }

            @Override
            public void onLongClick(View view, final int pos) {
            }
        }));
    }

    private void sendNegotiationResponse(Integer responseVal, final Integer negotoationid, final NegotationPayLoad npl) {

        String merchantId = prefManager.getMerchantID();// +"_"+temp.get(0).ContactNumber;
        String geoHash = prefManager.getMerchantGeoHash();

        npl.merchantid = merchantId;
        npl.geohash = geoHash;

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String nplStr = new Gson().toJson(npl);

        Call<String> call = apiService.negotiationresponse(merchantId, geoHash, String.valueOf(negotoationid), nplStr);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(mActivity, "Negotiation responded", Toast.LENGTH_LONG).show();
                try {
                    String qry = "update negotationrequests set response=" + npl.response + ",advance=" + npl.advance + " where idnegotations=" + negotoationid + " ;";
                    negotationrequests.executeQuery(qry);
                } catch (Exception ex) {
                    int f = 90;
                    ++f;
                }
                pb.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(mActivity, "Negotation Failed. Please re-send", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }
        });


    }


    public void fetchNegotiations() {
        prefManager = new PrefManager(getContext());
        String Mer_geoHash = prefManager.getMerchantGeoHash();
        String Mer_Id = prefManager.getMerchantID();
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Integer maxidnegotations = -1;

        try {
            List<negotationrequests> req = negotationrequests.find(negotationrequests.class,
                    "idnegotations = (select MAX(idnegotations) from NEGOTATIONREQUESTS)",
                    null, null, null, null);


            if (req.size() > 0)
                maxidnegotations = req.get(0).idnegotations;
        } catch (Exception ex) {

            int r = 50;
            ++r;
        }
        List<userdetails> temp = userdetails.listAll(userdetails.class);
        Log.d(TAG, " : " + Mer_Id + " " + Mer_geoHash + " " + maxidnegotations.toString());
        if (!Mer_Id.equals("-1") && !Mer_geoHash.equals("-1")) {

            Call<NegotationPayLoad[]> call = apiService.fetchNegotiations(Mer_Id,
                    Mer_geoHash, maxidnegotations.toString());
            call.enqueue(new Callback<NegotationPayLoad[]>() {
                @Override
                public void onResponse(Call<NegotationPayLoad[]> call, Response<NegotationPayLoad[]> response) {
                    if (response == null || response.body() == null)
                        return;
                    NegotationPayLoad[] npl = response.body();
                    Log.d(TAG, "fetchNegotiation: " + npl.length);

                    for (int i = 0; i < npl.length; i++) {
                        try {
                            negotationrequests obj = new negotationrequests(npl[i].customercontact, npl[i].merchantid,
                                    npl[i].geohash, npl[i].minamount, npl[i].maxamount, npl[i].DiscountExpectation,
                                    npl[i].ShoppingProbableDates, npl[i].description, npl[i].notificationid,
                                    npl[i].idnegotations, DateTime.getCurrentTime(),
                                    0, npl[i].adObj.imgUrl);
                            obj.save();
                        } catch (Exception ex) {
                            int x = 30;
                        }
                    }
                    mainInit();
                }

                @Override
                public void onFailure(Call<NegotationPayLoad[]> call, Throwable t) {
                    int z = 50;
                    ++z;
                    mainInit();
                }
            });
        }

    }

}
