package com.nearme.smartsell.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.nearme.smartsell.R;
import com.nearme.smartsell.model.MerchantCriteria;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.utility.PrefManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentEvents extends Fragment {

    View view;
    Context context;
    private PrefManager prefManager;
    ArrayList<String> events = new ArrayList<>();
    ArrayList<String> eventsDataType = new ArrayList<>();
    ArrayList<String> operations = new ArrayList<>();
    Spinner spEvents, spOperation;
    TextView btnCreateEvent;
    ProgressBar pb;
    LinearLayout linerString, linearDate;
    RadioGroup radioGroup;
    EditText value1, value2;
    TextView date1, date2;
    private int year, month, day;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_events, container, false);
        context = container.getContext();
        prefManager = new PrefManager(context);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        initialize();
        grabApiData();
        clicks();

        return view;
    }

    private void clicks() {
        btnCreateEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (events.size() == 0) {
                    Toast.makeText(context, "Events not found", Toast.LENGTH_LONG).show();
                    return;
                }
                if (operations.size() == 0) {
                    Toast.makeText(context, "Operations not found", Toast.LENGTH_LONG).show();
                    return;
                }

                postEvent();
            }
        });
    }

    private void initialize() {
        spEvents = view.findViewById(R.id.spCriteria);
        spOperation = view.findViewById(R.id.spCondition);
        btnCreateEvent = view.findViewById(R.id.btnCreateEvent);
        pb = view.findViewById(R.id.pb);

        linearDate = view.findViewById(R.id.linearDate);
        linerString = view.findViewById(R.id.linearString);
        radioGroup = view.findViewById(R.id.radioGroupBool);

        value1 = view.findViewById(R.id.value1);
        value2 = view.findViewById(R.id.value2);

        date1 = view.findViewById(R.id.date1);
        date2 = view.findViewById(R.id.date2);

        spEvents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (events.get(position).equalsIgnoreCase("sex")) {
                    operations.clear();
                    linerString.setVisibility(View.GONE);
                    linearDate.setVisibility(View.GONE);
                    radioGroup.setVisibility(View.VISIBLE);
                    operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.sexOperation)));
                    spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));

                } else if (eventsDataType.get(position).equalsIgnoreCase("String")) {
                    operations.clear();
                    operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.stringOperation)));
                    spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));
                    linerString.setVisibility(View.VISIBLE);
                    linearDate.setVisibility(View.GONE);
                    radioGroup.setVisibility(View.GONE);
                } else if (eventsDataType.get(position).equalsIgnoreCase("date")) {
                    operations.clear();
                    operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.dateOperation)));
                    spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));
                    linerString.setVisibility(View.GONE);
                    linearDate.setVisibility(View.VISIBLE);
                    radioGroup.setVisibility(View.GONE);
                } else if (eventsDataType.get(position).equalsIgnoreCase("boolean")) {
                    operations.clear();
                    operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.boolOperation)));
                    spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));
                    linerString.setVisibility(View.GONE);
                    linearDate.setVisibility(View.GONE);
                    radioGroup.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spOperation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                if (operations.get(position).equalsIgnoreCase("Equal")) {
                    (view.findViewById(R.id.value2)).setVisibility(View.GONE);
                    (view.findViewById(R.id.date2)).setVisibility(View.GONE);
                } else if (operations.get(position).equalsIgnoreCase("Like")) {
                    (view.findViewById(R.id.value2)).setVisibility(View.GONE);
                    (view.findViewById(R.id.date2)).setVisibility(View.GONE);
                } else if (operations.get(position).equalsIgnoreCase("In")) {
                    (view.findViewById(R.id.value2)).setVisibility(View.VISIBLE);
                    (view.findViewById(R.id.date2)).setVisibility(View.VISIBLE);
                } else if (operations.get(position).equalsIgnoreCase("Between")) {
                    (view.findViewById(R.id.value2)).setVisibility(View.VISIBLE);
                    (view.findViewById(R.id.date2)).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        date1.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                    (view, year, month, dayOfMonth) -> {
                        date1.setText(new StringBuilder().append(dayOfMonth).append("/")
                                .append(month + 1).append("/").append(year));
                    }, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        });

        date2.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                    (view, year, month, dayOfMonth) -> {
                        date2.setText(new StringBuilder().append(dayOfMonth).append("/")
                                .append(month + 1).append("/").append(year));
                    }, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        });
    }

    private void grabApiData() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiInterface.getMerchantEventCriteria().enqueue(new Callback<MerchantCriteria[]>() {
            @Override
            public void onResponse(Call<MerchantCriteria[]> call, Response<MerchantCriteria[]> response) {
                Log.d("TAG", "onResponse: " + response.body().length);
                if (response.body() != null) {
                    events.clear();
                    MerchantCriteria[] responseData = response.body();
                    for (MerchantCriteria responseDatum : responseData) {
                        events.add(responseDatum.EventCriteria);
                        eventsDataType.add(responseDatum.CriteriaDataType);
                    }
                    spEvents.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, events));

                    if (eventsDataType.get(0).equalsIgnoreCase("String")) {
                        operations.clear();
                        operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.stringOperation)));
                        spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));

                    } else if (eventsDataType.get(0).equalsIgnoreCase("DateTime")) {
                        operations.clear();
                        operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.dateOperation)));
                        spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));
                    } else if (eventsDataType.get(0).equalsIgnoreCase("boolean")) {
                        operations.clear();
                        operations.addAll(Arrays.asList(context.getResources().getStringArray(R.array.boolOperation)));
                        spOperation.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, operations));
                    }
                }
            }

            @Override
            public void onFailure(Call<MerchantCriteria[]> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
            }
        });

    }

    private void postEvent() {
        if (!validation()) {
            return;
        }
        pb.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        String eventCondition = events.get(spEvents.getSelectedItemPosition()) + " "
                + getOperationValue() + " " + getValue();
        Log.d("TAG", "postEvent: " + eventCondition);

        String merchantId = prefManager.getMerchantID();

        apiInterface.registerMerchantEvent(eventCondition, merchantId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                pb.setVisibility(View.GONE);
                Log.d("TAG", "onResponse: " + response.body());
                if (response.body() != null) {
                    Toast.makeText(context, "Event registered successfully", Toast.LENGTH_SHORT).show();
                    spOperation.setSelection(0);
                    spEvents.setSelection(0);
                    value1.setText("");
                    value2.setText("");
                    date1.setText("Select Date");
                    date2.setText("Select Date");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
                pb.setVisibility(View.GONE);
                Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getOperationValue() {
        if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Equal")) {
            return "=";
        } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("In")) {
            return "IN";
        } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Like")) {
            return "LIKE";
        } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Between")) {
            return "BETWEEN";
        }

        return "";
    }

    private boolean validation() {
        if (events.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("sex")) {
            return true;
        }
        if (eventsDataType.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("String")) {
            if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Equal")) {
                if (value1.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value1", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("In")) {
                if (value1.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value1", Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (value2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value2", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Like")) {
                if (value1.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value1", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Between")) {
                if (value1.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value1", Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (value2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(context, "Enter value2", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        } else if (eventsDataType.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("date")) {
            if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Equal")) {
                if (date1.getText().equals("Select Date")) {
                    Toast.makeText(context, "Select date1", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Between")) {
                if (date1.getText().equals("Select Date")) {
                    Toast.makeText(context, "Select date1", Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (date2.getText().equals("Select Date")) {
                    Toast.makeText(context, "Select date2", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }

        return true;
    }

    private String getValue() {
        if (events.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("sex")) {
            if (radioGroup.getCheckedRadioButtonId() == R.id.rbMale) {
                return "'1'";
            } else {
                return "'2'";
            }
        }
        if (eventsDataType.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("String")) {
            if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Equal")) {
                if (checkSex(value1))
                    return "'" + getSexValue(value1) + "'";
                else
                    return "'" + value1.getText().toString() + "'";
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("In")) {
                if (checkSex(value1) && checkSex(value2))
                    return "'" + getSexValue(value1) + "," + getSexValue(value2) + "'";
                else
                    return "'" + value1.getText().toString() + "," + value2.getText().toString() + "'";
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Like")) {
                if (checkSex(value1))
                    return "'%" + getSexValue(value1) + "%'";
                else
                    return "'%" + value1.getText().toString() + "%'";
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Between")) {
                return "'" + value1.getText().toString() + "," + value2.getText().toString() + "'";
            }
        } else if (eventsDataType.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("date")) {
            if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Equal")) {
                return date1.getText().toString();
            } else if (operations.get(spOperation.getSelectedItemPosition()).equalsIgnoreCase("Between")) {
                return date1.getText().toString() + "," + date2.getText().toString();
            }
        } else if (eventsDataType.get(spEvents.getSelectedItemPosition()).equalsIgnoreCase("boolean")) {
            RadioButton radio = view.findViewById(radioGroup.getCheckedRadioButtonId());
            return radio.getText().toString();
        }

        return "";
    }

    private boolean checkSex(EditText et) {
        return et.getText().toString().toLowerCase().contains("male") || et.getText().toString().toLowerCase().contains("female");
    }

    private String getSexValue(EditText et) {
        if (et.getText().toString().toLowerCase().contains("female"))
            return "2";
        else if (et.getText().toString().toLowerCase().contains("male"))
            return "1";
        return "-";
    }
}
