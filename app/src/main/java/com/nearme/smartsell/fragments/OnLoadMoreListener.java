package com.nearme.smartsell.fragments;

/**
 * Created by gopi.komanduri on 05/08/18.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}