package com.nearme.smartsell.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nearme.smartsell.R;
import com.nearme.smartsell.menu.GenerateBillActivity;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.startup.BillGenerationForItemsActivity;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.ImageFilePath;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class EditInvoiceFragment extends Fragment {
    View curView;
    public static SimpleDraweeView imgsel;
    EditText merchantname, billnumber, customerid, customername, ammount, billdate,billdateto, address, contactnumber,discount;
    static Uri imageUri = null;
    Uri finBitmapUri=null;
    String TokenId="-1";
    static String realPath = "";
    public static Bitmap img;
    Activity activity;
    PrefManager prefManager;
    ApiInterface apiService;
    private Calendar calendar;
    CheckBox isDiscountChkBx;
    private int year, month, day;
    public Integer tilldate,tilldateto;
    public Integer tillmonth,tillmonthto;
    public Integer tillyear,tillyearto;
    Button itemSelButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        curView = inflater.inflate(R.layout.invoice_edit_fragment, container, false);
        imgsel = curView.findViewById(R.id.btnCapturePicture);
        merchantname = curView.findViewById(R.id.merchantname);
        billnumber = curView.findViewById(R.id.billnumber);
        customerid = curView.findViewById(R.id.customerid);
        customername = curView.findViewById(R.id.customername);
        ammount = curView.findViewById(R.id.ammount);
        billdate = curView.findViewById(R.id.billdate);
        isDiscountChkBx=curView.findViewById(R.id.isDiscountChkBx);
        billdateto=curView.findViewById(R.id.billdateto);
        address = curView.findViewById(R.id.address);
        prefManager = new PrefManager(curView.getContext());
        contactnumber = curView.findViewById(R.id.contactnumber);
        discount=curView.findViewById(R.id.discount);
//        itemSelButton=curView.findViewById(R.id.itemSelButton);
        merchantname.setEnabled(false);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        activity = getActivity();
        if(!prefManager.getMerchantLogoURI().equals("-1") && prefManager!=null) {
            imageUri=Uri.parse(prefManager.getMerchantLogoURI());
            Picasso.with(curView.getContext()).load(prefManager.getMerchantLogoURI()).resize(100, 100)
                    .centerInside().into(imgsel);
        }
        getNextReciptID();
        imgsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, 11);
            }
        });

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);
        billdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new DatePickerDialog(getContext(), R.style.DateDialogTheme, myDateListener, year, month, day).show();
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                        myDateListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
        billdateto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                        myDateListenerto, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                //new DatePickerDialog(getContext(), R.style.DateDialogTheme, myDateListenerto, year, month, day).show();
            }
        });
        curView.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    startActivity(new Intent(curView.getContext(), ManageSlotsActivity.class));
                    return true;
                }
                return false;
            }
        } );
        if (curView != null) {
            //getMerchantAddress
            //getNextReciptID();

            merchantname.setText(prefManager.getMerchantName());
            address.setText(prefManager.getMerchantAddress());
            TextView bb = curView.findViewById(R.id.submitBtn);
            Button itemsSelBtn = curView.findViewById(R.id.itemSelButton);
            itemsSelBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //opens up a layout to add items
                    Bundle bundle = new Bundle();
                    bundle.putString("merchantlogoUri", imageUri.toString());
                    bundle.putString("merchantname", merchantname.getText().toString());
                    bundle.putString("billnumber", TokenId);
                    bundle.putString("customerid", customerid.getText().toString());
                    bundle.putString("customername", customername.getText().toString());
                    bundle.putString("ammount", ammount.getText().toString());
                    bundle.putString("billdate", billdate.getText().toString());
                    bundle.putString("billdateto", billdateto.getText().toString());
                    bundle.putString("address", address.getText().toString());
                    bundle.putString("contactnumber", contactnumber.getText().toString());
                    bundle.putString("discount", discount.getText().toString());
                    bundle.putBoolean("isDiscount",isDiscountChkBx.isChecked());
                    Intent billact=new Intent(curView.getContext(), BillGenerationForItemsActivity.class);
                    billact.putExtras(bundle);
                    if(!customerid.getText().toString().equals("") &&
                            !customername.getText().toString().equals("") &&
                            !contactnumber.getText().toString().equals("") &&  contactnumber.getText().length()>=10) {
                        startActivity(billact);
                    }
                    else {
                        if (customerid.getText().toString().equals("")) {
                            customerid.setError("Please Enter CustomerID");
                        }
                        if (customername.getText().toString().equals("")) {
                            customername.setError("Please Enter CustomerName");
                        }
                        if (contactnumber.getText().toString().equals("") || contactnumber.getText().length()>=10) {
                            contactnumber.setError("Please Enter Customer Mobile Number");
                        }
                    }
                }
                });
            bb.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //saveLayoutAsPDFToSdcard();
                    if (imageUri!=null && checkEditTet(merchantname, " Enter merchant name") && checkEditTet(billnumber, "Enter bill number") &&
                            checkEditTet(customerid, "Enter customer id") && checkEditTet(customername, "Enter customer name") &&
                            checkEditTet(ammount, "Enter amount") && checkEditTet(billdate, "Enter date") &&
                            checkEditTet(address, "Enter address") && checkEditTet(contactnumber, "Enter Contact number")) {

                        Bundle bundle = new Bundle();
                        bundle.putString("merchantlogoUri", imageUri.toString());
                        bundle.putString("merchantname", merchantname.getText().toString());
//                        bundle.putString("billnumber", billnumber.getText().toString());
                        bundle.putString("customerid", customerid.getText().toString());
                        bundle.putString("customername", customername.getText().toString());
                        bundle.putString("ammount", ammount.getText().toString());
                        bundle.putString("billdate", billdate.getText().toString());
                        bundle.putString("billdateto", billdateto.getText().toString());
                        bundle.putString("address", address.getText().toString());
                        bundle.putString("contactnumber", contactnumber.getText().toString());

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Fragment fragment = new GenerateBillActivity();
                        fragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.addToBackStack("");
                        fragmentTransaction.commit();
                    }
                    else
                    {
                        if(imageUri==null)
                        {
                            Toast.makeText(curView.getContext(), "Please Select Logo!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
        return curView;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    tillyear = arg1;
                    tillmonth = arg2 + 1;
                    tilldate = arg3;
                    showDate(arg1, arg2 + 1, arg3);
                }
            };


    private void showDate(int year, int month, int day) {
        billdate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private DatePickerDialog.OnDateSetListener myDateListenerto = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    tillyearto = arg1;
                    tillmonthto = arg2 + 1;
                    tilldateto = arg3;
                    showDateto(arg1, arg2 + 1, arg3);
                }
            };

    private void getNextReciptID() {
        if(prefManager==null)
        {
            prefManager = new PrefManager(curView.getContext());
        }
        String merchID = prefManager.getMerchantID();
        if (merchID != null) {
            Call<String> call = apiService.getnextreceiptidResponse(merchID);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.body() != null) {
                            TokenId = response.body();//
                        }
                    } catch (Exception ex) {
                        Log.d("MainResult", "onActivityResult: no token obtained");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                }
            });
        }
    }
    private void showDateto(int year, int month, int day) {
        billdateto.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

//        Toast.makeText(activity, "Hello", Toast.LENGTH_SHORT).show();

        if (resultCode == RESULT_OK) {
            imageUri = data.getData();
            try {
                realPath = ImageFilePath.getPath(activity, data.getData());
                //  realPath = imageUri.getPath();
                //   realPath = ImageFilePath.getUriRealPath(activity, data.getData());
                final InputStream imagestream = activity.getContentResolver().openInputStream(imageUri);
                img = BitmapFactory.decodeStream(imagestream);
                Picasso.with(activity).load(imageUri).resize(getScreenWidth() / 2, getScreenHeight() / 2)
                        .centerInside().into(imgsel);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else
        {
            Log.d("MainResult", "onActivityResult: not ok");
        }

    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    private boolean checkEditTet(EditText et, String msg) {

        if (et.getText().toString().equalsIgnoreCase("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }

        return true;
    }

}
