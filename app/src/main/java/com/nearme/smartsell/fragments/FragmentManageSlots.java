package com.nearme.smartsell.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.SlotAdapter;
import com.nearme.smartsell.model.Merchantslot;
import com.nearme.smartsell.model.SlotItem;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.MerchantPayload;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnExpDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthScrollListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;

public class FragmentManageSlots extends Fragment {

    private ManageSlotsActivity mActivity;
    View rv = null;
    Context context;
    RecyclerView recList;
    SlotAdapter slotAdapter;
    ArrayList<SlotItem> slotItemArrayList = new ArrayList<>();
    ArrayList<SlotItem> tmpArrayList = new ArrayList<>();
    private ExpCalendarView expCalendarView;
    private DateData selectedDate;
    private ImageView expandIV;
    TextView tv_year, tv_date;
    List<Merchantslot> merchantslotList = new ArrayList<>();
    ArrayList<String> stringArrayList = new ArrayList<>();
    public static Integer login = 0;
    private PrefManager prefManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (ManageSlotsActivity) getActivity();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.fragment_manage_slots, container, false);
        initialization();
        clicks();
        return rv;
    }

    @SuppressLint("SetTextI18n")
    private void calender() {
        Calendar calendar = Calendar.getInstance();
        selectedDate = new DateData(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        if (tv_year != null)
            tv_year.setText(calendar.get(Calendar.YEAR) + " " + getMonthName(calendar.get(Calendar.MONTH) + 1));
        if (tv_date != null)
            tv_date.setText(selectedDate.getYear() + "/" + selectedDate.getMonth() + "/" + selectedDate.getDay());

        if (expandIV != null)
            expandIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ifExpand) {
                        CellConfig.Month2WeekPos = CellConfig.middlePosition;
                        CellConfig.ifMonth = false;
                        expandIV.setImageResource(R.drawable.icon_arrow_down);
                        CellConfig.weekAnchorPointDate = selectedDate;
                        expCalendarView.shrink();
                    } else {
                        CellConfig.Week2MonthPos = CellConfig.middlePosition;
                        CellConfig.ifMonth = true;
                        expandIV.setImageResource(R.drawable.icon_arrow_up);
                        expCalendarView.expand();
                    }
                    ifExpand = !ifExpand;
                }
            });

        if (expCalendarView != null)
            expCalendarView.setOnDateClickListener(new OnExpDateClickListener()).setOnMonthScrollListener(new OnMonthScrollListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onMonthChange(int year, int month) {
                    tv_year.setText(year + " " + getMonthName(month));
                }

                @Override
                public void onMonthScroll(float positionOffset) {
//                Log.i("listener", "onMonthScroll:" + positionOffset);
                }
            });

    }

    @SuppressLint("SetTextI18n")
    private void clicks() {
        rv.findViewById(R.id.linbtm).setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("dateList", stringArrayList);//

            FragmentManager fm = mActivity.getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            FragmentAddSlots fragment = new FragmentAddSlots();
            fragment.setArguments(bundle);
            ft.replace(R.id.rlContainer, fragment);
            ft.addToBackStack("slot");
            ft.commit();
        });
        expCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                selectedDate = date;
                tmpArrayList.clear();
                String _date = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
                Log.d("TAG", "onDateClick: " + _date);

                for (int i = 0; i < slotItemArrayList.size(); i++) {
                    if (slotItemArrayList.get(i).getDate().equalsIgnoreCase(_date)) {
                        tmpArrayList.add(slotItemArrayList.get(i));
                    }
                }
                slotAdapter.notifyDataSetChanged();
                Calendar calendar = Calendar.getInstance();
                if (selectedDate.getDay() == calendar.get(Calendar.DAY_OF_MONTH)
                        && selectedDate.getMonth() == (calendar.get(Calendar.MONTH) + 1)
                        && selectedDate.getYear() == calendar.get(Calendar.YEAR)) {
                    tv_date.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    tv_date.setTextColor(getResources().getColor(R.color.colorPrimary));
                }

                tv_date.setText(selectedDate.getYear() + "/" + selectedDate.getMonth() + "/" + selectedDate.getDay());
            }
        });
    }

    private String getMonthName(int number) {
        switch (number) {
            case 1:
                return "JAN";

            case 2:
                return "FEB";

            case 3:
                return "MAR";

            case 4:
                return "APR";

            case 5:
                return "MAY";

            case 6:
                return "JUN";

            case 7:
                return "JUL";

            case 8:
                return "AUG";

            case 9:
                return "SEP";

            case 10:
                return "OCT";

            case 11:
                return "NOV";

            case 12:
                return "DEC";

            default:
                return "";
        }
    }

    private boolean ifExpand = false;

    private void initialization() {
        recList = rv.findViewById(R.id.recList);
        tv_year = rv.findViewById(R.id.tv_year);
        tv_date = rv.findViewById(R.id.tv_date);
        expandIV = rv.findViewById(R.id.main_expandIV);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        recList.setLayoutManager(linearLayoutManager);

        slotAdapter = new SlotAdapter(tmpArrayList);
        recList.setAdapter(slotAdapter);
        expCalendarView = rv.findViewById(R.id.calendar_exp);

        expCalendarView.shrink();
        CellConfig.ifMonth = false;
        calender();

    }

    private void apiCall() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String date = dateFormat.format(calendar.getTime()) + " 00:00:00";

        calendar.add(Calendar.MONTH, 1);
        String afterDate = dateFormat.format(calendar.getTime()) + " 23:59:59";
        Log.d("TAG", "apiCall: " + date + " " + afterDate);

        PrefManager prefManager = new PrefManager(mActivity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.d("TAG", "apiCall: " + prefManager.getMerchantID());
        Call<Merchantslot[]> call = apiService.getSlots(prefManager.getMerchantID(), date, afterDate);

        call.enqueue(new Callback<Merchantslot[]>() {
            @Override
            public void onResponse(Call<Merchantslot[]> call, Response<Merchantslot[]> response) {
                Log.d("TAG", "onResponse: successs");
                Merchantslot[] data = response.body();
                merchantslotList.clear();
                if (data != null) {
                    merchantslotList.addAll(Arrays.asList(data));
                    manageData();
                }
            }

            @Override
            public void onFailure(Call<Merchantslot[]> call, Throwable t) {
                Log.d("TAG", "onResponse: failure " + t.getMessage());
            }
        });
    }

    private void manageData() {
        slotItemArrayList.clear();
        stringArrayList.clear();
        for (int i = 0; i < merchantslotList.size(); i++) {
            Date fromDate = new Date(Long.parseLong(merchantslotList.get(i).FromTime) * 1000L);
            Date toDate = new Date(Long.parseLong(merchantslotList.get(i).ToTime) * 1000L);
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy", Locale.getDefault());
            SimpleDateFormat sdf2 = new SimpleDateFormat("MM", Locale.getDefault());
            SimpleDateFormat sdf3 = new SimpleDateFormat("dd", Locale.getDefault());
            SimpleDateFormat sdf4 = new SimpleDateFormat("HH", Locale.getDefault());
            SimpleDateFormat sdf5 = new SimpleDateFormat("mm", Locale.getDefault());
            sdf1.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf2.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf3.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf4.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            sdf5.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            int year = Integer.parseInt(sdf1.format(fromDate));
            int month = Integer.parseInt(sdf2.format(fromDate));
            int day = Integer.parseInt(sdf3.format(fromDate));
            int hour = Integer.parseInt(sdf4.format(fromDate));
            int minute = Integer.parseInt(sdf5.format(fromDate));
            int hour_ = Integer.parseInt(sdf4.format(toDate));
            int minute_ = Integer.parseInt(sdf5.format(toDate));
//
            expCalendarView.markDate(new DateData(year, month, day)
                    .setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, Color.GREEN)));

            SlotItem slotItem = new SlotItem();
            slotItem.setMaxToken(String.valueOf(merchantslotList.get(i).MaxToken));
            slotItem.setFromTime(hour + ":" + (minute < 10 ? "0" + minute : minute));
            slotItem.setToTime(hour_ + ":" + (minute_ < 10 ? "0" + minute_ : minute_));
            slotItem.setDate(year + "-" + month + "-" + day);
            slotItemArrayList.add(slotItem);
            stringArrayList.add(month + "/" + day + "/" + year);
        }
        tmpArrayList.clear();
        String _date = selectedDate.getYear() + "-" + selectedDate.getMonth() + "-" + selectedDate.getDay();
        for (int j = 0; j < slotItemArrayList.size(); j++) {
            if (slotItemArrayList.get(j).getDate().equalsIgnoreCase(_date)) {
                tmpArrayList.add(slotItemArrayList.get(j));
            }
        }
        slotAdapter.notifyDataSetChanged();

    }

    @Override
    public void onResume() {
        super.onResume();
        prefManager = new PrefManager(mActivity);
        getMerchantPreferenceData();
    }

    private void getMerchantPreferenceData() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        String mid = prefManager.getMerchantID();
        if (!mid.equalsIgnoreCase("-1")) {
            Call<String> getMerchantDetailscall = apiService.getMerchantDetails(mid);

            getMerchantDetailscall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                    try {
                        MerchantPayload mp = new Gson().fromJson(response.body(), MerchantPayload.class);
                        prefManager.setMerchantName(mp.merchantName);
                        ManageSlotsActivity.phoneNumber = mp.merchantPhn;
                        prefManager.setMerchantID(mp.merchantId);
                        prefManager.setMerchantContact(mp.merchantPhn);
                        mActivity.CustIDTextView.setText(mp.merchantId);

                        if (mp.geoHash != null) {
                            prefManager.setMerchantGeoHash(mp.geoHash);
                        }
                        ManageSlotsActivity.longitude = mp.longitude;
                        ManageSlotsActivity.latitude = mp.latitude;
                        prefManager.setLatitude(String.valueOf(ManageSlotsActivity.latitude));
                        prefManager.setLongitude(String.valueOf(ManageSlotsActivity.longitude));
                        prefManager.setMerchantAddress(getAddress());
                        ManageSlotsActivity.name = mp.merchantName;
                        ManageSlotsActivity.imageUrl = mp.imgurl;
                        prefManager.setMerchantLogoURI(mp.imgurl);
//                        imageView.setImage
                        Picasso.with(mActivity).load(ManageSlotsActivity.imageUrl).resize(100, 100)
                                .centerInside().into(mActivity.imageView);
                        mActivity.FromMainActivity();
                        apiCall();
                        expandIV.performClick();
                        expandIV.performClick();
                    } catch (Exception ex) {

                    }
                }

                @Override
                public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                    Toast.makeText(mActivity, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private String getAddress() {
        String finalAddress = "";
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(mActivity, Locale.getDefault());

            addresses = geocoder.getFromLocation(mActivity.latitude, mActivity.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            finalAddress = addresses.get(0).getAddressLine(0);// + city + state + country + postalCode;
        } catch (Exception ex) {

        }
        return finalAddress;
    }

}
