package com.nearme.smartsell.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CounterAdapter;
import com.nearme.smartsell.model.Countermodel;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.counterandhelper;
import com.nearme.smartsell.rest.counteremppayload;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.PrefManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCounter extends Fragment implements View.OnClickListener{

    private ManageSlotsActivity mActivity;
    View rv = null;
    Button createCounterBtn, tokenlogBtn;
    EditText countersTxt;
    EditText helpersTxt;
    TextInputLayout tiCounterText, tihelperText;
    Context curContext;
    TextView counterTxtvw, hlprTxtview;
    RecyclerView recyclerView;
    ListView pendingTokensListView;
    ApiInterface apiService;
    PrefManager prefManager;
    CounterAdapter cuAdapter;
    int count = 1;
    Spinner empOptions;
    ImageView side_menu;
    ProgressBar pb;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ManageSlotsActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.token_merchant_layout, container, false);
        createCounterBtn = rv.findViewById(R.id.createCounterBtn);
        createCounterBtn.setOnClickListener(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        curContext = mActivity;
        countersTxt = rv.findViewById(R.id.countersTxt);
        tihelperText = rv.findViewById(R.id.tihelperText);
        tiCounterText = rv.findViewById(R.id.tiCounterText);
        helpersTxt = rv.findViewById(R.id.helpersTxt);
        recyclerView = rv.findViewById(R.id.counetrsGrid);
        counterTxtvw = rv.findViewById(R.id.counterTxtview);
        hlprTxtview = rv.findViewById(R.id.hlprTxtview);
        prefManager = new PrefManager(mActivity);
        tokenlogBtn = rv.findViewById(R.id.tokenlogBtn);
        pb = rv.findViewById(R.id.pb);
        tokenlogBtn.setOnClickListener(this);
        pendingTokensListView = rv.findViewById(R.id.pendingTokensList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(curContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        pb.setVisibility(View.VISIBLE);
        init();
        return rv;
    }

    private void init(){
        Call<String> getMerchantDetailscall = apiService.getmerchantcounterdetailsResponse(prefManager.getMerchantID());
        getMerchantDetailscall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                try {
                    if (response.body() != null) {
                        count = 1;
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<counteremppayload>>() {
                        }.getType();
                        List<counteremppayload> jsonList = gson.fromJson(response.body(), type);
                        List<Countermodel> counterEmployees = new ArrayList<>();
//                                int count=1;
                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter(String.valueOf(count));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());
                            counterEmployees.add(cm);
                            count++;
                        }
                        cuAdapter = new CounterAdapter(counterEmployees);
                        recyclerView.setAdapter(cuAdapter);
                    }
                    createCountersCall(prefManager.getMerchantID());
                } catch (Exception ex) {
                    createCountersCall(prefManager.getMerchantID());
                }
            }

            @Override
            public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                Toast.makeText(curContext, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                createCountersCall(prefManager.getMerchantID());
            }
        });

        empOptions = rv.findViewById(R.id.empOptionsSpinner);
        empOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String msupplier = empOptions.getSelectedItem().toString();
                switch (msupplier) {
                    case "-- Select --":
                        tokenlogBtn.setText("Select Action to Perform");
                        break;
                    case "Create Counters and Helpers":
                        tiCounterText.setVisibility(View.VISIBLE);
                        tihelperText.setVisibility(View.VISIBLE);
                        tokenlogBtn.setText(msupplier);
                        break;
                    default:
                        tiCounterText.setVisibility(View.GONE);
                        tihelperText.setVisibility(View.GONE);
                        tokenlogBtn.setText(msupplier);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void createCountersCall(String merchantId) {
        if(!countersTxt.getText().toString().equals("")) {
            if (Integer.parseInt(countersTxt.getText().toString()) > 10) {
                Toast.makeText(mActivity, "More then 10 counters are not allowed", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if(!helpersTxt.getText().toString().equals("")) {
            if (Integer.parseInt(helpersTxt.getText().toString()) > 10) {
                Toast.makeText(mActivity, "More then 10 helpers are not allowed", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Call<String> call = apiService.createCountersresponse(merchantId, String.valueOf(countersTxt.getText()),
                String.valueOf(helpersTxt.getText()));

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        int helpCounter = 1;
                        count = 1;
                        // Type type = new TypeToken<List<counteremppayload>>() {}.getType();
                        Type type = new TypeToken<counterandhelper>() {
                        }.getType();
                        counterandhelper obj = gson.fromJson(response.body(), type);
                        List<counteremppayload> jsonList = obj.getCounterslist();
                        List<Countermodel> counterEmployees = new ArrayList<>();
//                        int count=1;
                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter("Counter : " + String.valueOf(count));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());

                            counterEmployees.add(cm);
                            count++;
                        }

                        jsonList = obj.getHelperslist();

                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter("Helper : " + String.valueOf(helpCounter));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());

                            counterEmployees.add(cm);
                            helpCounter++;
                        }
                        recyclerView.setVisibility(View.VISIBLE);
                        cuAdapter = new CounterAdapter(counterEmployees);
                        recyclerView.setAdapter(cuAdapter);
                        counterTxtvw.setText("Total No of counters Available are : " + (count - 1));
                        hlprTxtview.setText("Total No of Helpers Available are : " + (helpCounter - 1));
                        countersTxt.setText("0");
                        helpersTxt.setText("0");
//                                Toast.makeText(curContext, "counetrs Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                    pb.setVisibility(View.GONE);
                } catch (Exception ex) {
                    pb.setVisibility(View.GONE);
                    int x = 50;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tokenlogBtn) {// TODO Auto-generated method stub
            pendingTokensListView.setVisibility(View.INVISIBLE);
            String msupplier = empOptions.getSelectedItem().toString();
            String merchantId = prefManager.getMerchantID();
            switch (msupplier) {
                case "-- Select --":
                    break;
                case "Create Counters and Helpers":
                case "Opened Counters":
                    createCountersCall(merchantId);
                    break;
                case "Close Counters":
                    Call<String> call2 = apiService.closecountersResponse(merchantId);

                    call2.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Toast.makeText(curContext, "Counter closed", Toast.LENGTH_LONG).show();
                            recyclerView.setVisibility(View.INVISIBLE);
                            countersTxt.setText("0");
                            helpersTxt.setText("0");
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(curContext, "Technical error occured", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
                case "Pending Tokens":
                    Call<String[]> call3 = apiService.pendingtokensResponse(merchantId);

                    call3.enqueue(new Callback<String[]>() {
                        @Override
                        public void onResponse(Call<String[]> call, Response<String[]> response) {
                            recyclerView.setVisibility(View.INVISIBLE);
                            pendingTokensListView.setVisibility(View.VISIBLE);
                            String[] items = response.body();
                            if (items != null && items.length > 0) {
                                ArrayAdapter itemsAdapter =
                                        new ArrayAdapter(curContext, R.layout.simplelistitem, R.id.textView, items);
                                pendingTokensListView.setAdapter(itemsAdapter);
                            } else {
                                Toast.makeText(curContext, "No Pending Tokens ", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String[]> call, Throwable t) {
                            Toast.makeText(curContext, "pending Tokens Not found", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;
            }
            Log.e("Selected item : ", msupplier);
        }
    }
}
