package com.nearme.smartsell.fragments;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.adshistory;
import com.nearme.smartsell.utility.EndlessRecyclerViewScrollListener;

import java.util.List;

/**
 * Created by gopi.komanduri on 03/08/18.
 */

public class HistoryRecyclerViewAdapter extends RecyclerView.Adapter<HistoryRecyclerViewAdapter.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private EndlessRecyclerViewScrollListener onLoadMoreListener;

    public HistoryRecyclerViewAdapter(List<adshistory> postedAds, RecyclerView recyclerView) {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final SimpleDraweeView mImageView;
        public final TextView validTill;
        public final LinearLayout mLayoutItem;
        public final TextView offerCode;
      //  public final ImageButton mImageViewWishlist;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.postedImage);
            validTill = view.findViewById(R.id.idValidTillHistory);
            offerCode = view.findViewById(R.id.idOfferCodeHistory);
            mLayoutItem = view.findViewById(R.id.layout_item);
           // mImageViewWishlist = (ImageButton) view.findViewById(R.id.ic_wishlist);
        }
    }
}
