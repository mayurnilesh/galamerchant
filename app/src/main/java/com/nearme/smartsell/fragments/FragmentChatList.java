package com.nearme.smartsell.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.ChatListAdapter;
import com.nearme.smartsell.model.ChatInfo;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.PrefManager;

import java.util.ArrayList;

public class FragmentChatList extends Fragment {
    View rv = null;
    ChatListAdapter simpleAdapter;
    RecyclerView androidListView;
    Activity context;
    ProgressBar pb;
    TextView tvEmpty;
    private PrefManager prefManager;
    ArrayList<ChatInfo> ChatInfoArrayList = new ArrayList<>();
    private ManageSlotsActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ManageSlotsActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.chat_list_activity, container, false);
        prefManager = new PrefManager(mActivity);
        androidListView = rv.findViewById(R.id.chat_list_view);
        pb = rv.findViewById(R.id.pb);
        tvEmpty = rv.findViewById(R.id.tvEmpty);
        androidListView.setLayoutManager(new LinearLayoutManager(mActivity));
        pb.setVisibility(View.VISIBLE);
        getChatDialog();
        return rv;
    }

    private void getChatDialog() {
        String merchant_id = prefManager.getMerchantID();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("MerchantUsers/" + merchant_id);

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    ChatInfoArrayList.clear();
                    for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                        ChatItem _chatItem = dataSnapshot.child(String.valueOf(i)).getValue(ChatItem.class);
                        if (_chatItem != null) {
                            ChatInfo info = new ChatInfo();
                            info.imgIcon = _chatItem.getImgref();
                            info.txtId = _chatItem.getGuid();
                            info.txtTitle = _chatItem.getName();
                            ChatInfoArrayList.add(info);
                            simpleAdapter = new ChatListAdapter(mActivity, context, ChatInfoArrayList);
                            androidListView.setAdapter(simpleAdapter);
                            simpleAdapter.notifyDataSetChanged();
                        }
                    }

                } else {
                    tvEmpty.setVisibility(View.VISIBLE);
                }
                pb.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pb.setVisibility(View.GONE);
            }
        });

    }

    public static class ChatItem {

        public String imageref;
        public String name;
        public String guid;

        public ChatItem() {
        }

        public String getImgref() {
            return imageref;
        }

        public void setImgref(String imgref) {
            this.imageref = imgref;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }


    }

}
