package com.nearme.smartsell.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.ApprovalAdapter;
import com.nearme.smartsell.db.negotationrequests;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopi.komanduri on 03/08/18.
 */

public class ApproveDealsFragment extends DialogFragment {
    View rv = null;
    Context context;
    RecyclerView rec_job;
    List<negotationrequests> negotationrequestsList = new ArrayList<>();
    String TAG = "ApproveDealsFragment";
    TextView tvNoData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.fragment_approval, container, false);

        List<negotationrequests> tmplist = negotationrequests.listAll(negotationrequests.class);
        negotationrequestsList.clear();
        for (int i = 0; i < tmplist.size(); i++) {
            if (tmplist.get(i).getResponse() == 1) {
                if (checkDuplicate(tmplist.get(i).idnegotations))
                    negotationrequestsList.add(tmplist.get(i));
            }
        }
        Log.d(TAG, "onCreateView: " + negotationrequestsList.size());
        initialize();

        return rv;
    }

    private void initialize() {
        rec_job = rv.findViewById(R.id.rec_job);
        tvNoData = rv.findViewById(R.id.tvNoData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rec_job.setLayoutManager(linearLayoutManager);
        rec_job.setItemAnimator(new DefaultItemAnimator());
        ApprovalAdapter apercuAdapter = new ApprovalAdapter(negotationrequestsList);
        rec_job.setAdapter(apercuAdapter);

        if(negotationrequestsList.size() == 0){
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkDuplicate(int a) {
        for (int i = 0; i < negotationrequestsList.size(); i++) {
            if (negotationrequestsList.get(i).idnegotations == a)
                return false;
        }
        return true;
    }


}
