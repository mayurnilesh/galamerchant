package com.nearme.smartsell.fragments;

public class list_item_commercial {
    public String imageUrl;
    public String validTill;
    public String shopName;
    public String offerCode;
    public String location;

    public list_item_commercial(String imageUrl, String validTill, String shopName, String offerCode, String location) {
        this.imageUrl = imageUrl;
        this.validTill = validTill;
        this.shopName = shopName;
        this.offerCode = offerCode;
        this.location = location;
    }
}
