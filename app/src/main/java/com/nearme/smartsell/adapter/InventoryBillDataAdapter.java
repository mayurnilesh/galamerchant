package com.nearme.smartsell.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.model.Billdata;

import java.util.List;


public class InventoryBillDataAdapter extends RecyclerView.Adapter<InventoryBillDataAdapter.Holder> {
    List<Billdata> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    String TAG = "BillDataAdapter";
    Integer quantityCal = 0;
    TextView tvsubmit;
    TextView incsubmit;
    TextView decsubmit;
    TextView quansubmit;

    //    private final OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(Billdata item);
    }

    private final InventoryBillDataAdapter.OnItemClickListener listener;

    public InventoryBillDataAdapter(List<Billdata> homeArrayList, OnItemClickListener listener) {
        this.homeArrayList = homeArrayList;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_inventory_billdata, parent, false);
        return new Holder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.bind(homeArrayList.get(position), listener);
        holder.price.setText(String.valueOf(homeArrayList.get(position).getItemprice()));
        holder.itemid.setText(String.valueOf(homeArrayList.get(position).getItemid()) + "/" + homeArrayList.get(position).getItemname());
        holder.desc.setText(String.valueOf(homeArrayList.get(position).getItemdesc()));

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.lgrey));
        }

    }

    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equalsIgnoreCase("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView price, desc, itemid;
        CardView cardview;


        public Holder(View itemView) {
            super(itemView);

            price = itemView.findViewById(R.id.price);
            desc = itemView.findViewById(R.id.desc);
            itemid = itemView.findViewById(R.id.itemid);
            cardview = itemView.findViewById(R.id.cardview);


        }

        public void bind(final Billdata item, final InventoryBillDataAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
