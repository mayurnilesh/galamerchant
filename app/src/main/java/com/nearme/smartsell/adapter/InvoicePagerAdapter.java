package com.nearme.smartsell.adapter;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nearme.smartsell.fragments.EditInvoiceFragment;
import com.nearme.smartsell.menu.GenerateBillActivity;

public class InvoicePagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public InvoicePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                EditInvoiceFragment tab1 = new EditInvoiceFragment();
                return tab1;
            case 1:
                GenerateBillActivity tab2 = new GenerateBillActivity();
                return tab2;
//            case 2:
//                TabFragment3 tab3 = new TabFragment3();
//                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}