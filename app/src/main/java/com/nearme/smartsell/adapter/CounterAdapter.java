package com.nearme.smartsell.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.OnLoadMoreListener;
import com.nearme.smartsell.model.Countermodel;

import java.util.List;


public class CounterAdapter extends RecyclerView.Adapter<CounterAdapter.Holder> {
    private final List<Countermodel> homeArrayList;

    private Context mContext;

    public CounterAdapter(List<Countermodel> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.counter_row_layout, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        if (homeArrayList.get(position).getCounter() == null || homeArrayList.get(position).getCounter().isEmpty()) {
            holder.merchantcounter.setVisibility(View.GONE);
        } else {
            holder.merchantcounter.setVisibility(View.VISIBLE);
        }

        holder.merchantcounter.setText(homeArrayList.get(position).getCounter());
        holder.merchantname.setText("Username : " + homeArrayList.get(position).getMerchantid());
        holder.token.setText("Password : " + homeArrayList.get(position).getPassword());

    }


    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        CardView cardview;
        ImageView iv;
        TextView merchantname, token, merchantcounter;

        Holder(View itemView) {
            super(itemView);
            cardview = itemView.findViewById(R.id.cardview);
            iv = itemView.findViewById(R.id.iv);
            merchantname = itemView.findViewById(R.id.merchantname);
            token = itemView.findViewById(R.id.token);
            merchantcounter = itemView.findViewById(R.id.merchantcounter);
        }
    }
}
