package com.nearme.smartsell.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.OnLoadMoreListener;
import com.nearme.smartsell.model.CounterData;

import java.util.List;


public class counterDataAdaptor extends RecyclerView.Adapter<com.nearme.smartsell.adapter.counterDataAdaptor.Holder> {
    List<CounterData> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;

    public counterDataAdaptor(List<CounterData> homeArrayList, RecyclerView recyclerView) {
        this.homeArrayList = homeArrayList;
        if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {

            final StaggeredGridLayoutManager linearLayoutManager = (StaggeredGridLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();


                            int[] lastVisibleItemPositions = linearLayoutManager.findLastVisibleItemPositions(null);
                            // get maximum element within the list''
                            lastVisibleItem = getLastVisibleItem(lastVisibleItemPositions);

                            if (totalItemCount <= (lastVisibleItem)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
//                                loading = true;
                            }
                        }
                    });
        }
    }

    public int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    @Override
    public com.nearme.smartsell.adapter.counterDataAdaptor.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.counterdata_row_layout, parent, false);
        return new com.nearme.smartsell.adapter.counterDataAdaptor.Holder(view);
    }

    @Override
    public void onBindViewHolder(com.nearme.smartsell.adapter.counterDataAdaptor.Holder holder, final int position) {

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        holder.tokenno.setText(String.format("%s", homeArrayList.get(position).getTokenID()));
        holder.counterno.setText(String.format("%s", homeArrayList.get(position).getCounterID()));


    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CardView cardview;
        ImageView iv;
        TextView counterno, tokenno;

        public Holder(View itemView) {
            super(itemView);
            cardview = itemView.findViewById(R.id.cardview);
            counterno = itemView.findViewById(R.id.counterno);
            tokenno = itemView.findViewById(R.id.token);
        }
    }
}
