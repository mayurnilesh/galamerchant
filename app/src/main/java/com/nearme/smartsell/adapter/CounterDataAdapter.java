package com.nearme.smartsell.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.model.CounterData;

import java.util.List;


public class CounterDataAdapter extends RecyclerView.Adapter<CounterDataAdapter.Holder> {
   public List<CounterData> homeArrayList;
   public List<CounterData> prevhomeArrayList = null;

    Context mContext;
    LayoutInflater layoutInflater;

    public CounterDataAdapter(List<CounterData> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.counterdata_row_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.token.setText("Token : "+homeArrayList.get(position).getTokenID());
        holder.counterno.setText("Counter : "+homeArrayList.get(position).getCounterID());

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView counterno,token;

        public Holder(View itemView) {
            super(itemView);
            token = itemView.findViewById(R.id.token);
            counterno = itemView.findViewById(R.id.counterno);
        }
    }

}
