package com.nearme.smartsell.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.FragmentAddSlots;
import com.nearme.smartsell.model.SlotData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class SlotDateAdapter extends RecyclerView.Adapter<SlotDateAdapter.Holder> {
    ArrayList<SlotData> slotDateList;
    Context mContext;
    LayoutInflater layoutInflater;
    int lastPosition = -1;
    FragmentAddSlots.Callback callback;

    public SlotDateAdapter(ArrayList<SlotData> slotDateList, FragmentAddSlots.Callback callback) {
        this.slotDateList = slotDateList;
        this.callback = callback;
    }

    @NotNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_slot_date, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull Holder holder, final int position) {
        if (position == 0) {
            holder.linadd.setVisibility(View.VISIBLE);
            holder.linmain.setVisibility(View.INVISIBLE);
            holder.close.setVisibility(View.GONE);
        } else {
            holder.linadd.setVisibility(View.INVISIBLE);
            holder.linmain.setVisibility(View.VISIBLE);
            holder.close.setVisibility(View.VISIBLE);
        }
        holder.linadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.callBack();
            }
        });

        holder.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slotDateList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.slotPeople.setText(slotDateList.get(position).getSlotSize()+" People");
        holder.slotTime.setText(slotDateList.get(position).getFromTime() + " to " + slotDateList.get(position).getToTime());

    }

    @Override
    public int getItemCount() {
        return slotDateList.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        LinearLayout linadd, linmain;
        ImageView close;
        TextView slotPeople, slotTime;

        public Holder(View itemView) {
            super(itemView);
            linmain = itemView.findViewById(R.id.linmain);
            close = itemView.findViewById(R.id.close);
            linadd = itemView.findViewById(R.id.linadd);
            slotPeople = itemView.findViewById(R.id.slotPeople);
            slotTime = itemView.findViewById(R.id.slotTime);
        }
    }
}
