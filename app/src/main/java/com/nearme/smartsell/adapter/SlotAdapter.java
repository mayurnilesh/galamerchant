package com.nearme.smartsell.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.db.negotationrequests;
import com.nearme.smartsell.model.SlotItem;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.Holder> {
    ArrayList<SlotItem> slotArrayLIst;
    Context mContext;
    LayoutInflater layoutInflater;
    int lastPosition = -1;

    public SlotAdapter(ArrayList<SlotItem> slotArrayLIst) {
        this.slotArrayLIst = slotArrayLIst;
    }

    @NotNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_slot_item, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull Holder holder, final int position) {
        SlotItem slotItem = slotArrayLIst.get(position);
        holder.tvTime.setText(slotItem.getFromTime()+" - "+slotItem.getToTime());
        holder.tvSlot.setText(slotItem.getMaxToken()+"People");
    }

    @Override
    public int getItemCount() {
        return slotArrayLIst.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        TextView tvTime, tvSlot;

        public Holder(View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvSlot = itemView.findViewById(R.id.tvSlot);
        }
    }
}
