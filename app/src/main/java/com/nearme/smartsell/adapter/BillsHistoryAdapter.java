package com.nearme.smartsell.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.ContactSpecificInfo;
import com.nearme.smartsell.startup.BillItemSingleView;

public class BillsHistoryAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<ContactSpecificInfo> billshistoryList = null;
    private ArrayList<ContactSpecificInfo> arraylist;

    public BillsHistoryAdapter(Context context, List<ContactSpecificInfo> billshistoryList) {
        mContext = context;
        this.billshistoryList = billshistoryList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<ContactSpecificInfo>();
        this.arraylist.addAll(billshistoryList);
    }

    public class ViewHolder {
        TextView Id;
        TextView merchantreceiptid;
        TextView imgURL;
    }

    @Override
    public int getCount() {
        return billshistoryList.size();
    }

    @Override
    public ContactSpecificInfo getItem(int position) {
        return billshistoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.bills_history_listview__item_layout, null);
            // Locate the TextViews in listview_item.xml
            holder.Id = view.findViewById(R.id.idlabel);
            holder.merchantreceiptid = view.findViewById(R.id.mIdLbl);
            holder.imgURL = view.findViewById(R.id.ImgUrl);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.Id.setText("ID : "+billshistoryList.get(position).getId().toString());
        holder.merchantreceiptid.setText("Receipt ID "+billshistoryList.get(position).getMerchantReceiptId());
        holder.imgURL.setText(billshistoryList.get(position).getimgurl());

        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, BillItemSingleView.class);
                // Pass all data rank
                intent.putExtra("Id",("ID : "+billshistoryList.get(position).getId()));
                // Pass all data country
                intent.putExtra("MerchantReciptID",("Receipt ID "+billshistoryList.get(position).getMerchantReceiptId()));
                // Pass all data population
                intent.putExtra("ImgUrl",(billshistoryList.get(position).getimgurl()));
                // Pass all data flag
                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        billshistoryList.clear();
        if (charText.length() == 0) {
            billshistoryList.addAll(arraylist);
        }
        else
        {
            for (ContactSpecificInfo wp : arraylist)
            {
//                if (wp.getCountry().toLowerCase(Locale.getDefault()).contains(charText))
//                {
//                    billshistoryList.add(wp);
//                }
            }
        }
        notifyDataSetChanged();
    }

}