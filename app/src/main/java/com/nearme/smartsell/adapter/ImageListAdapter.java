package com.nearme.smartsell.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartsell.R;
import com.squareup.picasso.Picasso;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;


public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {

    public List<String> catValues;
    public Activity ctx;

    public ImageListAdapter(List<String> items, Activity context) {
        catValues = items;
        ctx = context;
    }

    @NonNull
    @Override
    public ImageListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageListAdapter.ViewHolder holder, final int position) {

        if (catValues.get(position).equals("")) {
            holder.addImage.setVisibility(View.VISIBLE);
            holder.image.setVisibility(View.GONE);
        } else {
            holder.addImage.setVisibility(View.GONE);
            holder.image.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(catValues.get(position));

            Bitmap bitmap = BitmapFactory.decodeFile(catValues.get(position));
            holder.image.setImageBitmap(bitmap);

        }

        holder.addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new ImagePicker.Builder(ctx)
//                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
//                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
//                        .directory(ImagePicker.Directory.DEFAULT)
//                        .allowMultipleImages(false)
//                        .enableDebuggingMode(true)
//                        .build();
            }
        });

    }

    @Override
    public int getItemCount() {
        return catValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView addImage, image;


        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            addImage = view.findViewById(R.id.addImage);
        }
    }
}
