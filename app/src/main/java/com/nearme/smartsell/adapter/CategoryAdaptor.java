package com.nearme.smartsell.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CategoryAdaptor.AppInfoHolder;
import com.nearme.smartsell.fragments.DataAdapter;
import com.nearme.smartsell.model.CategoryInfo;

import java.util.ArrayList;

public class CategoryAdaptor extends RecyclerView.Adapter<CategoryAdaptor.AppInfoHolder> {
    public SparseBooleanArray mCheckStates;

    Context mContext;
    ArrayList<CategoryInfo> data;

    public CategoryAdaptor(Context context, ArrayList<CategoryInfo> data) {
        this.mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AppInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.filter_listview_activity, parent, false);
        return new AppInfoHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(@NonNull AppInfoHolder holder, int position) {
        holder.txtId.setText(data.get(position).txtId);
        holder.txtTitle.setText(data.get(position).txtTitle);
        Glide.with(mContext).load(data.get(position).imgIcon).into(holder.imgIcon);

        holder.chkSelect.setOnCheckedChangeListener(null);

        if (data.get(position).chkSelect)
            holder.chkSelect.setSelected(true);
        else
            holder.chkSelect.setSelected(false);

        holder.chkSelect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            data.get(position).chkSelect = isChecked;
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class AppInfoHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtId;
        CheckBox chkSelect;

        public AppInfoHolder(@NonNull View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.listview_image);
            chkSelect = itemView.findViewById(R.id.listview_checkBox);
            txtId = itemView.findViewById(R.id.listview_item_title);
            txtTitle = itemView.findViewById(R.id.listview_item_short_description);
        }
    }
}

