package com.nearme.smartsell.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.TokenLog;

import java.util.List;


public class TokenLogAdapter extends RecyclerView.Adapter<TokenLogAdapter.Holder> {
    List<TokenLog> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public TokenLogAdapter(List<TokenLog> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.counter_row_token_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }

        holder.merchantcounter.setText(String.format("%s",homeArrayList.get(position).gettoken()));
        holder.counterno.setText(String.format("%s",homeArrayList.get(position).getcounter()));
        holder.timeServed.setText(String.format("%s",(homeArrayList.get(position).gettimeServed()/1000)));
        holder.startTime.setText(homeArrayList.get(position).getstartTime());
        holder.endTime.setText(homeArrayList.get(position).getendTime());


    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        CardView cardview;
        ImageView iv;
        TextView  counterno,merchantcounter,timeServed,startTime,endTime;

        public Holder(View itemView) {
            super(itemView);
            cardview = itemView.findViewById(R.id.cardview);
            iv = itemView.findViewById(R.id.iv);
            counterno = itemView.findViewById(R.id.counterno);
            timeServed = itemView.findViewById(R.id.timeServed);
            merchantcounter=itemView.findViewById(R.id.merchantcounter);
            startTime=itemView.findViewById(R.id.startTime);
            endTime=itemView.findViewById(R.id.endTime);
        }
    }
}
