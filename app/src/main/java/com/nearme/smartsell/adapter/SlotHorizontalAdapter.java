package com.nearme.smartsell.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.model.SlotData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class SlotHorizontalAdapter extends RecyclerView.Adapter<SlotHorizontalAdapter.Holder> {
    ArrayList<SlotData> slotDataArrayLIst;
    Context mContext;
    LayoutInflater layoutInflater;

    public SlotHorizontalAdapter(ArrayList<SlotData> slotDataArrayLIst) {
        this.slotDataArrayLIst = slotDataArrayLIst;
    }

    @NotNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_slot_horizontal_data, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull Holder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return slotDataArrayLIst.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }
    }
}
