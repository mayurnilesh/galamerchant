package com.nearme.smartsell.adapter;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.db.billingitem;
import com.nearme.smartsell.model.Billdata;

import java.util.List;


public class BillDataAdapter extends RecyclerView.Adapter<BillDataAdapter.Holder> {
    List<Billdata> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    String TAG = "BillDataAdapter";
    Integer quantityCal = 0;
    TextView tvsubmit;
    TextView incsubmit;
    TextView decsubmit;
    TextView quansubmit;

    //    private final OnItemClickListener listener;
    public interface OnItemClickListener {
        void onItemClick(Billdata item);
    }

    private final BillDataAdapter.OnItemClickListener listener;

    public BillDataAdapter(List<Billdata> homeArrayList, OnItemClickListener listener) {
        this.homeArrayList = homeArrayList;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_billdata, parent, false);
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.bind(homeArrayList.get(position), listener);
        holder.name.setText(homeArrayList.get(position).getItemname());
        holder.price.setText(String.valueOf(homeArrayList.get(position).getItemprice()));
        holder.itemid.setText(String.valueOf(homeArrayList.get(position).getItemid()));
        holder.desc.setText(String.valueOf(homeArrayList.get(position).getItemdesc()));
        holder.quan.setText(String.valueOf(homeArrayList.get(position).getQuantity()));
        holder.quanenum.setText(String.valueOf(homeArrayList.get(position).getQuantityenum()));
        holder.quanunit.setText(String.valueOf(homeArrayList.get(position).getQuantityunit()));

        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.lgrey));
        }

        holder.ivedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final billingitem bill = billingitem.findById(billingitem.class, position + 1);
                Log.d(TAG, "onClick: " + bill.itemname);

                final Dialog dialog = new Dialog(mContext, R.style.Theme_CustomDialog);
                dialog.setContentView(R.layout.dialogue_add_bill_item_checkout);
                dialog.show();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

                TextView tvsubmit = dialog.findViewById(R.id.tvsubmit);
                incsubmit = dialog.findViewById(R.id.increase);
                decsubmit = dialog.findViewById(R.id.decrease);
                quansubmit = dialog.findViewById(R.id.integer_number);
                final EditText editname = dialog.findViewById(R.id.editname);
                final EditText editprice = dialog.findViewById(R.id.editprice);
                final EditText editid = dialog.findViewById(R.id.editid);
                final EditText editdesc = dialog.findViewById(R.id.editdesc);
                final EditText quanitytunit = dialog.findViewById(R.id.quanitytunit);
                final Spinner spenum = dialog.findViewById(R.id.spenum);

                editname.setText(homeArrayList.get(position).getItemname());
                editdesc.setText(homeArrayList.get(position).getItemdesc());
                editid.setText(homeArrayList.get(position).getItemid());
                editprice.setText(homeArrayList.get(position).getItemprice());
                quanitytunit.setText(homeArrayList.get(position).getQuantityunit());
                quansubmit.setText(homeArrayList.get(position).getQuantity());
                try {
                    quantityCal = Integer.parseInt(homeArrayList.get(position).getQuantity());
                } catch (NumberFormatException nex) {
                    quantityCal = 0;
                }
                if (quantityCal == 1) {
                    decsubmit.setEnabled(false);
                } else {
                    decsubmit.setEnabled(true);
                }
                incsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal >= 1) {
                            quantityCal++;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 1) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });

                decsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantityCal > 1) {
                            quantityCal--;
                            quansubmit.setText(quantityCal.toString());
                        }
                        if (quantityCal == 1) {
                            decsubmit.setEnabled(false);
                        } else {
                            decsubmit.setEnabled(true);
                        }
                    }
                });

                tvsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkEditText(editname, "Enter product name") && checkEditText(editprice, "Enter product price")
                                && checkEditText(editid, "Enter product id") && checkEditText(editdesc, "Enter product description")
                                && checkEditText(quanitytunit, "Enter unit")) {
                            dialog.dismiss();

                            bill.itemname = editname.getText().toString();
                            bill.itemdesc = editdesc.getText().toString();
                            bill.itemid = editid.getText().toString();
                            bill.itemprice = editprice.getText().toString();
                            bill.itemQuantity = quantityCal;
                            bill.itemunit = quanitytunit.getText().toString();
                            bill.itemenum = spenum.getSelectedItem().toString();
                            bill.save();

                            homeArrayList.get(position).setItemprice(editprice.getText().toString());
                            homeArrayList.get(position).setItemname(editname.getText().toString());
                            homeArrayList.get(position).setItemid(editid.getText().toString());
                            homeArrayList.get(position).setItemdesc(editdesc.getText().toString());
                            homeArrayList.get(position).setQuantityenum(spenum.getSelectedItem().toString());
                            homeArrayList.get(position).setQuantityunit(quanitytunit.getText().toString());

                            notifyDataSetChanged();
                        }
                    }
                });

            }
        });


    }

    private boolean checkEditText(EditText et, String msg) {
        if (et.getText().toString().equalsIgnoreCase("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price, desc, itemid, quan, quanunit, quanenum;
        CardView cardview;
        ImageView ivedit;


        public Holder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            ivedit = itemView.findViewById(R.id.ivedit);
            desc = itemView.findViewById(R.id.desc);
            itemid = itemView.findViewById(R.id.itemid);
            quan = itemView.findViewById(R.id.quan);
            quanunit = itemView.findViewById(R.id.quanunit);
            quanenum = itemView.findViewById(R.id.quanenum);
            cardview = itemView.findViewById(R.id.cardview);


        }

        public void bind(final Billdata item, final BillDataAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
