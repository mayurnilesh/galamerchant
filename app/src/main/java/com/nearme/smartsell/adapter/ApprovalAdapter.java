package com.nearme.smartsell.adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearme.smartsell.R;
import com.nearme.smartsell.db.negotationrequests;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.Holder> {
    List<negotationrequests> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;

    public ApprovalAdapter(List<negotationrequests> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_approval, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.contact.setText(homeArrayList.get(position).getCustomercontact());
        holder.minsale.setText(String.valueOf(homeArrayList.get(position).getMinamount()));
        holder.maxsale.setText(String.valueOf(homeArrayList.get(position).getMaxamount()));
        holder.disc.setText(String.valueOf(homeArrayList.get(position).getDiscountexpectation()) + "%");
        holder.desc.setText(homeArrayList.get(position).getDescription());
        holder.date.setText(homeArrayList.get(position).getShoppingprobabledates());
        holder.advance.setText("Rs." + String.valueOf(homeArrayList.get(position).getAdvance()));

        if (homeArrayList.get(position).getAdvancepayment() == 0)
            holder.advancestatus.setText("Pending");
        else
            holder.advancestatus.setText("Done");


        if (position % 2 == 0) {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.lgrey));
        } else {
            holder.cardview.setCardBackgroundColor(mContext.getResources().getColor(R.color.grey));
        }


    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView contact;
        TextView minsale;
        TextView maxsale;
        TextView disc;
        TextView desc;
        TextView date;
        TextView advance;
        TextView advancestatus;
        CardView cardview;
        ImageView negotiatedimg;


        public Holder(View itemView) {
            super(itemView);
            contact = itemView.findViewById(R.id.contact);
            minsale = itemView.findViewById(R.id.minsale);
            maxsale = itemView.findViewById(R.id.maxsale);
            disc = itemView.findViewById(R.id.disc);
            desc = itemView.findViewById(R.id.desc);
            date = itemView.findViewById(R.id.date);
            advance = itemView.findViewById(R.id.advance);
            advancestatus = itemView.findViewById(R.id.advancestatus);
            cardview = itemView.findViewById(R.id.cardview);
        }
    }
}
