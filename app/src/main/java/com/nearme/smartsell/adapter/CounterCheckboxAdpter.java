package com.nearme.smartsell.adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.counteremppayload;

import java.util.List;


public class CounterCheckboxAdpter extends RecyclerView.Adapter<CounterCheckboxAdpter.Holder> {
    List<counteremppayload> homeArrayList;

    Context mContext;
    LayoutInflater layoutInflater;
    SQLiteDatabase sqLiteDatabase;

    public CounterCheckboxAdpter(List<counteremppayload> homeArrayList) {
        this.homeArrayList = homeArrayList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_counter_cb, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.name.setText(homeArrayList.get(position).getUsername());

        holder.name.setOnCheckedChangeListener(null);
        holder.name.setChecked(homeArrayList.get(position).isStatus());

        holder.name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                homeArrayList.get(position).setStatus(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        CheckBox name;

        public Holder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
        }
    }

}
