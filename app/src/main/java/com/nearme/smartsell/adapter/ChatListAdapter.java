package com.nearme.smartsell.adapter;

import android.content.Context;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.FragmentChat;
import com.nearme.smartsell.model.ChatInfo;
import com.nearme.smartsell.startup.ManageSlotsActivity;

import java.util.ArrayList;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.AppInfoHolder> {
    Context mContext;
    ArrayList<ChatInfo> data;
    ManageSlotsActivity mActivity;

    public ChatListAdapter(ManageSlotsActivity activity, Context context, ArrayList<ChatInfo> data) {
        this.mContext = context;
        this.data = data;
        mActivity = activity;
    }

    @NonNull
    @Override
    public AppInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.row_chat_listview, parent, false);

        return new AppInfoHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull AppInfoHolder holder, int position) {
        holder.txtId.setText(data.get(position).txtId);

        holder.txtTitle.setText(data.get(position).txtTitle);
        holder.txtId.setVisibility(View.INVISIBLE);

        Glide.with(mActivity).load(data.get(position).imgIcon)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.imgIcon);
        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("title", data.get(position).txtTitle);//
            bundle.putString("name", data.get(position).txtId);//
            bundle.putBoolean("add", false);//add

            FragmentManager fm = mActivity.getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            FragmentChat fragment = new FragmentChat();
            fragment.setArguments(bundle);
            ft.replace(R.id.rlContainer, fragment);
            ft.addToBackStack("Chat");
            ft.commit();

        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class AppInfoHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtId;

        public AppInfoHolder(@NonNull View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.listview_image);
            txtId = itemView.findViewById(R.id.listview_item_short_description);
            txtTitle = itemView.findViewById(R.id.listview_item_title);
        }
    }
}

