package com.nearme.smartsell.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nearme.smartsell.R;
import com.nearme.smartsell.adapter.CounterAdapter;
import com.nearme.smartsell.model.Countermodel;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.counterandhelper;
import com.nearme.smartsell.rest.counteremppayload;
import com.nearme.smartsell.startup.BaseActivity;
import com.nearme.smartsell.startup.TokenLogActivity;
import com.nearme.smartsell.utility.PrefManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TokenMerchantActivity extends BaseActivity implements View.OnClickListener {

    Button createCounterBtn, tokenlogBtn;
    EditText countersTxt;
    EditText helpersTxt;
    TextInputLayout tiCounterText, tihelperText;
    Context curContext;
    TextView counterTxtvw, hlprTxtview;
    RecyclerView recyclerView;
    ListView pendingTokensListView;
    ApiInterface apiService;
    PrefManager prefManager;
    CounterAdapter cuAdapter;
    int count = 1;
    Spinner empOptions;
    ImageView side_menu;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.token_merchant_layout);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        FromMainActivity();
        createCounterBtn = findViewById(R.id.createCounterBtn);
        createCounterBtn.setOnClickListener(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        curContext = this.getApplicationContext();
        countersTxt = findViewById(R.id.countersTxt);
        tihelperText = findViewById(R.id.tihelperText);
        tiCounterText = findViewById(R.id.tiCounterText);
        helpersTxt = findViewById(R.id.helpersTxt);
        recyclerView = findViewById(R.id.counetrsGrid);
        counterTxtvw = findViewById(R.id.counterTxtview);
        hlprTxtview = findViewById(R.id.hlprTxtview);
        prefManager = new PrefManager(this);
        tokenlogBtn = findViewById(R.id.tokenlogBtn);
        pb = findViewById(R.id.pb);
        tokenlogBtn.setOnClickListener(this);
        pendingTokensListView = findViewById(R.id.pendingTokensList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(curContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        changeStatusBarColor();
        pb.setVisibility(View.VISIBLE);
        Call<String> getMerchantDetailscall = apiService.getmerchantcounterdetailsResponse(prefManager.getMerchantID());

        getMerchantDetailscall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                try {
                    if (response.body() != null) {
                        count = 1;
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<counteremppayload>>() {
                        }.getType();
                        List<counteremppayload> jsonList = gson.fromJson(response.body(), type);
                        List<Countermodel> counterEmployees = new ArrayList<>();
//                                int count=1;
                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter(String.valueOf(count));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());
                            counterEmployees.add(cm);
                            count++;
                        }
                        cuAdapter = new CounterAdapter(counterEmployees);
                        recyclerView.setAdapter(cuAdapter);
                    }
                    createCountersCall(prefManager.getMerchantID());
                } catch (Exception ex) {
                    createCountersCall(prefManager.getMerchantID());
                }
            }

            @Override
            public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                Toast.makeText(curContext, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                createCountersCall(prefManager.getMerchantID());
            }
        });

        empOptions = findViewById(R.id.empOptionsSpinner);
        empOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String msupplier = empOptions.getSelectedItem().toString();
                switch (msupplier) {
                    case "-- Select --":
                        tokenlogBtn.setText("Select Action to Perform");
                        break;
                    case "Create Counters and Helpers":
                        tiCounterText.setVisibility(View.VISIBLE);
                        tihelperText.setVisibility(View.VISIBLE);
                        tokenlogBtn.setText(msupplier);
                        break;
                    default:
                        tiCounterText.setVisibility(View.GONE);
                        tihelperText.setVisibility(View.GONE);
                        tokenlogBtn.setText(msupplier);
                        break;
                }
                Log.e("Selected item : ", msupplier);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void createCountersCall(String merchantId) {
        if(!countersTxt.getText().toString().equals("")) {
            if (Integer.parseInt(countersTxt.getText().toString()) > 10) {
                Toast.makeText(curContext, "More then 10 counters are not allowed", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if(!helpersTxt.getText().toString().equals("")) {
            if (Integer.parseInt(helpersTxt.getText().toString()) > 10) {
                Toast.makeText(curContext, "More then 10 helpers are not allowed", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Call<String> call = apiService.createCountersresponse(merchantId, String.valueOf(countersTxt.getText()),
                String.valueOf(helpersTxt.getText()));

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (response.body() != null) {
                        Toast.makeText(TokenMerchantActivity.this, "Counter Opened", Toast.LENGTH_SHORT).show();
                        Gson gson = new Gson();
                        int helpCounter = 1;
                        count = 1;
                        // Type type = new TypeToken<List<counteremppayload>>() {}.getType();
                        Type type = new TypeToken<counterandhelper>() {
                        }.getType();
                        counterandhelper obj = gson.fromJson(response.body(), type);
                        List<counteremppayload> jsonList = obj.getCounterslist();
                        List<Countermodel> counterEmployees = new ArrayList<>();
//                        int count=1;
                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter("Counter : " + String.valueOf(count));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());

                            counterEmployees.add(cm);
                            count++;
                        }

                        jsonList = obj.getHelperslist();

                        for (counteremppayload cn : jsonList) {
                            Countermodel cm = new Countermodel();
                            cm.setCounter("Helper : " + String.valueOf(helpCounter));
                            cm.setMerchantid(cn.getUsername());
                            cm.setPassword(cn.getPassword());

                            counterEmployees.add(cm);
                            helpCounter++;
                        }
                        recyclerView.setVisibility(View.VISIBLE);
                        cuAdapter = new CounterAdapter(counterEmployees);
                        recyclerView.setAdapter(cuAdapter);
                        counterTxtvw.setText("Total No of counters Available are : " + (count - 1));
                        hlprTxtview.setText("Total No of Helpers Available are : " + (helpCounter - 1));
                        countersTxt.setText("0");
                        helpersTxt.setText("0");
//                                Toast.makeText(curContext, "counetrs Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                    pb.setVisibility(View.GONE);
                } catch (Exception ex) {
                    pb.setVisibility(View.GONE);
                    int x = 50;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
                pb.setVisibility(View.GONE);
            }
        });
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tokenlogBtn: {
                // TODO Auto-generated method stub
                pendingTokensListView.setVisibility(View.INVISIBLE);
                String msupplier = empOptions.getSelectedItem().toString();
                String merchantId = prefManager.getMerchantID();
                switch (msupplier) {
                    case "-- Select --":
                        break;
                    case "Create Counters and Helpers":
                    case "Opened Counters":
                        createCountersCall(merchantId);
                        break;
                    case "Close Counters":
                        Call<String> call2 = apiService.closecountersResponse(merchantId);

                        call2.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Toast.makeText(curContext, "Counter closed", Toast.LENGTH_LONG).show();
                                recyclerView.setVisibility(View.INVISIBLE);
                                countersTxt.setText("0");
                                helpersTxt.setText("0");
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Toast.makeText(curContext, "Technical error occured", Toast.LENGTH_LONG).show();
                            }
                        });
                        break;
                    case "Pending Tokens":
                        Call<String[]> call3 = apiService.pendingtokensResponse(merchantId);

                        call3.enqueue(new Callback<String[]>() {
                            @Override
                            public void onResponse(Call<String[]> call, Response<String[]> response) {
                                recyclerView.setVisibility(View.INVISIBLE);
                                pendingTokensListView.setVisibility(View.VISIBLE);
                                String[] items = response.body();
                                if (items != null && items.length > 0) {
                                    ArrayAdapter itemsAdapter =
                                            new ArrayAdapter(curContext, R.layout.simplelistitem, R.id.textView, items);
                                    pendingTokensListView.setAdapter(itemsAdapter);
                                } else {
                                    Toast.makeText(curContext, "No Pending Tokens ", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<String[]> call, Throwable t) {
                                Toast.makeText(curContext, "pending Tokens Not found", Toast.LENGTH_LONG).show();
                            }
                        });
                        break;
                    case "Get Counters Log":
                        startActivity(new Intent(TokenMerchantActivity.this, TokenLogActivity.class).putExtra("NoofCounters", count - 1));
                        break;
                }
                Log.e("Selected item : ", msupplier);
            }
            break;
//            case R.id.tokenlogBtn:
//                startActivity(new Intent(TokenMerchantActivity.this, TokenLogActivity.class).putExtra("NoofCounters", count - 1));
//                break;
        }
    }
}
