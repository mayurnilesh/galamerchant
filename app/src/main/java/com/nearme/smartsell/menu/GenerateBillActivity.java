package com.nearme.smartsell.menu;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.utility.ImageUploader;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenerateBillActivity extends Fragment {

    Context context;
    View content;
    Bundle bundle;
    PrefManager prefManager;
    String TokenId="-1";
    String contactToRecv,billNo,BillAmt;
    Gson gson;
    public static SimpleDraweeView imgsel;
    Uri imageUri;
    ApiInterface apiService;
    Cloudinary cloudinary = new Cloudinary("cloudinary://648251253437633:T4ZnOgkQ1yzhWTIFF8FHlqWCkLE@locator");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = inflater.inflate(R.layout.gym_bill_t_layout, container, false);
        context = container.getContext();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        gson=new Gson();
        getNextReciptID();
        prefManager = new PrefManager(context);
        final View vv = content.findViewById(R.id.cd);
        imgsel = content.findViewById(R.id.btnCapturePicture);
        bundle = getArguments();
        if (!bundle.isEmpty()) {

            try {
                imageUri = Uri.parse(bundle.getString("merchantlogoUri"));
                final InputStream imagestream = getActivity().getContentResolver().openInputStream(imageUri);
                Bitmap img = BitmapFactory.decodeStream(imagestream);
                Picasso.with(getActivity()).load(imageUri).resize(getScreenWidth() / 2, getScreenHeight() / 2)
                        .centerInside().into(imgsel);
            } catch (Exception ex) {

            }
            contactToRecv=bundle.getString("contactnumber");
            billNo=bundle.getString("billnumber");
            BillAmt=bundle.getString("ammount");
//            ((TextView) content.findViewById(R.id.contact)).setText(contactToRecv);
            ((TextView) content.findViewById(R.id.address)).setText(bundle.getString("address"));
            ((TextView) content.findViewById(R.id.date)).setText("From "+ bundle.getString("billdate")+" To "+ bundle.getString("billdateto"));
            ((TextView) content.findViewById(R.id.bamount)).setText(bundle.getString("ammount"));
            ((TextView) content.findViewById(R.id.cname)).setText(bundle.getString("customername"));
            ((TextView) content.findViewById(R.id.cid)).setText(bundle.getString("customerid"));
//            ((TextView) content.findViewById(R.id.bnumber)).setText(bundle.getString("billnumber"));
            ((TextView) content.findViewById(R.id.mname)).setText(bundle.getString("merchantname"));

        }
        vv.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    startActivity(new Intent(context, ManageSlotsActivity.class));
                    return true;
                }
                return false;
            }
        } );
        content.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        TextView bb = content.findViewById(R.id.submitBtn);
        bb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Bitmap bm = viewToBitmap(vv);
                String name = "Gala" + System.currentTimeMillis() + ".png";
                try {
                    FileOutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + name);
                    bm.compress(Bitmap.CompressFormat.PNG, 100, output);
                    output.close();
                    uploadToCloudinary(getImageUri(context,bm));

                    Toast.makeText(context, "Bill saved in your device successfully", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                } catch (FileNotFoundException e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (IOException e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }
            }
        });
        return content;
    }
    private void getNextReciptID() {
        if(prefManager==null)
        {
            prefManager = new PrefManager(context);
        }
        String merchID = prefManager.getMerchantID();
        if (merchID != null) {
            Call<String> call = apiService.getnextreceiptidResponse(merchID);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.body() != null) {
                            TokenId = response.body();
//                        billnumber.setText(TokenId);
                            ((TextView) content.findViewById(R.id.bnumber)).setText(TokenId);
                        }
                    } catch (Exception ex) {
                        Log.d("MainResult", "onActivityResult: no token obtained");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                }
            });
        }
    }
    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    public void uploadToCloudinary(Uri imageUri)
    {
        Object[] params = new Object[9];
        params[0] = cloudinary;
        params[1] = imageUri;
        params[2] = ObjectUtils.emptyMap();
        params[3] = getActivity();
        params[4] = 2; // condition typeReceived 1 for bills
        params[5]=prefManager.getMerchantID();
        params[6]=contactToRecv;
        params[7]=TokenId;
        params[8]=BillAmt;
        new ImageUploader(getContext()).execute(params);
    }


    public void uploadtoContact(String contact) {
        Call<String> call = apiService.
                posttospecificcontactResponse(prefManager.getMerchantID(),contact,"","0",null,null);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    if (response.body() != null) {

                    }
                } catch (Exception ex) {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }


    public Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
}
