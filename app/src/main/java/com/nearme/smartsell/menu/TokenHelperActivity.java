package com.nearme.smartsell.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.nearme.smartsell.R;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.startup.BaseActivity;
import com.nearme.smartsell.utility.PrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopi.komanduri on 14/11/18.
 */

public class TokenHelperActivity extends AppCompatActivity implements View.OnClickListener {
    EditText contactnumber;
    Button registerForTokensbtn;
    PrefManager prefManager ;
    ApiInterface apiService;
    Context curContext;
    TextView merchTxtVw;
    boolean isValidStringOrMobileNum=false;
ImageView side_menu;

    public static ProgressBar waitingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.token_helper_layout);
        contactnumber = findViewById(R.id.numbertoadd);
        registerForTokensbtn = findViewById(R.id.RegisterForToken);

        waitingDialog = findViewById(R.id.registerfortokenbar);
        waitingDialog.setVisibility(View.INVISIBLE);
        prefManager = new PrefManager(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        curContext = this.getApplicationContext();
        changeStatusBarColor();
        merchTxtVw=findViewById(R.id.merchTxtVw);
        if(prefManager.getMerchantID()!=null) {
            String merchTxt="Hello "+ prefManager.getMerchantID();
            merchTxtVw.setText(merchTxt);
        }
        contactnumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                CheckIfValidStringOrMobileNum();
            }
        });

        registerForTokensbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String merchantId = prefManager.getMerchantID();

//                waitingDialog.setVisibility(View.VISIBLE);
                registerForTokensbtn.setEnabled(false);
                final String contact = contactnumber.getText().toString();
                if (isValidStringOrMobileNum) {
                    waitingDialog.setVisibility(View.VISIBLE);
                    Call<String> call = apiService
                            .registerfortokenthruhelper(merchantId, contact);

                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            try {
                                waitingDialog.setVisibility(View.INVISIBLE);
                                if (response.body() != null) {
                                    String Message = "Registered " + contact + "with token " + response.body();
                                    Toast.makeText(curContext, Message, Toast.LENGTH_LONG).show();
                                    registerForTokensbtn.setEnabled(true);
                                    contactnumber.setText("");
//                                Toast.makeText(curContext,"registered "+contact+"with token "+response.body().toString(),Toast.LENGTH_LONG);
                                }
                            } catch (Exception ex) {
                                String Message = "Unable to Register " + contact + "with token " + response.body();
                                registerForTokensbtn.setEnabled(true);
                                Toast.makeText(curContext, Message, Toast.LENGTH_LONG).show();
//                            Toast.makeText(curContext,"unable to register "+contact+" with token "+response.body().toString(),Toast.LENGTH_LONG);
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            registerForTokensbtn.setEnabled(true);
                            waitingDialog.setVisibility(View.INVISIBLE);
                            Toast.makeText(curContext, "unable to register " + contact + " with token ", Toast.LENGTH_LONG);
                        }
                    });
                } else {
                    contactnumber.setError("Please enter a Valid Mobile Number or Valid Name");
                    registerForTokensbtn.setEnabled(true);
                    waitingDialog.setVisibility(View.INVISIBLE);
                }
            }
        });

    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    private void CheckIfValidStringOrMobileNum() {
        isValidStringOrMobileNum = false;
        String regexStr = "^[1-9][0-9]*$";
        String stringRegex="0*";
        if (contactnumber.getText().toString().trim().matches(regexStr)) {
            if (contactnumber.getText().toString().length() == 10) {
                isValidStringOrMobileNum = true;
            }
        } else {
// entered a string
            if (!contactnumber.getText().toString().trim().matches(stringRegex)) {
                isValidStringOrMobileNum = true;
            }
        }
    }
    @Override
    public void onClick(View view) {
        String merchantId =    prefManager.getMerchantID();
        switch (view.getId()) {
            case R.id.RegisterForToken:

                final String contact = contactnumber.getText().toString();

                Call<String> call = apiService
                        .registerfortokenthruhelper(merchantId,contact);

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        try {
                            if (response.body() != null) {
                                Toast.makeText(curContext,"registered "+contact+"with token "+ response.body(),Toast.LENGTH_LONG);
                            }
                        } catch (Exception ex) {
                            Toast.makeText(curContext,"unable to register "+contact+" with token "+ response.body(),Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        waitingDialog.setVisibility(View.INVISIBLE);
                        Toast.makeText(curContext,"unable to register "+contact+" with token ",Toast.LENGTH_LONG);
                    }
                });
        }


    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
        return;
    }
}

