package com.nearme.smartsell.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.gson.Gson;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.MerchantPayload;
import com.nearme.smartsell.startup.BaseActivity;
import com.nearme.smartsell.utility.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TokenEmpActivity extends BaseActivity implements View.OnClickListener {

    TextView CounterTxt,merchIDTxtVw;
    Button DoneBtn, NextBtn, RenewBtn;
    ApiInterface apiService;
    Context curContext;
    PrefManager prefManager;
    java.util.Date startdate = Calendar.getInstance().getTime();
    // = df.format();
    java.util.Date enddate = Calendar.getInstance().getTime();
    // = df.parse("");
    public static ProgressBar waitingDialog;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    private Timer mNextTimer = null;
    Switch notifySwitch;
    boolean isAutoNotifyOn=true;
    MediaPlayer mediaPlayer;
    Gson gson;
    TextView CustIDTextView, CustNoTextView;
    ImageView imageView;
    ImageView side_menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nearme.smartsell.R.layout.token_emp_layout);
        side_menu = findViewById(R.id.side_menu);
        if(side_menu!=null) {
            side_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DrawerLayout drawer = findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if(drawer!=null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            FromMainActivity();
        }

        CounterTxt = findViewById(R.id.curTokenTxt);
        DoneBtn = findViewById(R.id.DoneTokenbtn);
        DoneBtn.setOnClickListener(this);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        NextBtn = findViewById(R.id.nextTokenBtn);
        NextBtn.setOnClickListener(this);
        RenewBtn = findViewById(R.id.Renew);
        RenewBtn.setOnClickListener(this);
        curContext = this.getApplicationContext();
//        merchIDTxtVw=findViewById(R.id.merchIDTxtVw);
        gson = new Gson();
        CustIDTextView = findViewById(R.id.Customerid);
        CustNoTextView = findViewById(R.id.Customername);
        imageView = findViewById(R.id.imageView);

//        RenewBtn.setEnabled(false);
        waitingDialog = findViewById(R.id.progressBar1);
        waitingDialog.setVisibility(View.INVISIBLE);
        prefManager = new PrefManager(this);
        notifySwitch=findViewById(R.id.notifySwitch);
        notifySwitch.setChecked(true);
        isAutoNotifyOn=notifySwitch.isChecked();
//        changeStatusBarColor();


        startMoveNextTimer();
        notifySwitch.setOnClickListener(this);
        if(prefManager.getMerchantID()!=null) {
            UpdateMerchantDetails(prefManager.getMerchantID());
//            merchIDTxtVw.setText(merchTxt);
        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onClick(View view) {
        String merchantId = prefManager.getMerchantID();
        switch (view.getId()) {
            case R.id.nextTokenBtn:
                if (mNextTimer != null) {
                    mNextTimer.cancel();
                    mNextTimer=null;
                }
                waitingDialog.setVisibility(View.VISIBLE);
                NextBtn.setEnabled(false);
                enddate = Calendar.getInstance().getTime();
                double diff = enddate.getTime() - startdate.getTime();
                Call<String> call = apiService.
                        GenerateNextTokenresponse(merchantId, prefManager.getCounterNumber(), Integer.valueOf(CounterTxt.getText().toString()), diff,
                                startdate.toString(), enddate.toString());

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        try {
                            if (response.body() != null) {
                                waitingDialog.setVisibility(View.INVISIBLE);
                                RenewBtn.setEnabled(true);
//                                if (response.body().equalsIgnoreCase("-1")) {
//                                    RenewBtn.setEnabled(true);
//                                    startTimer();
//                                    Toast.makeText(curContext, "No customers waiting.", Toast.LENGTH_LONG).show();
//                                }
                                try {
                                    if (!response.body().equals("-1")) {
                                        NextBtn.setEnabled(true);
                                        Integer.parseInt(response.body());
                                        CounterTxt.setText(response.body());
                                    } else {
                                        CounterTxt.setText("0");
                                        RenewBtn.setEnabled(false);
                                        startTimer();
                                        Toast.makeText(curContext, "No customers waiting.", Toast.LENGTH_LONG).show();
                                    }
                                } catch (NumberFormatException e) {
                                    //System.out.println("This is not an integer.");
                                    CounterTxt.setText("-1");
                                    NextBtn.setEnabled(true);
//                                    return false;
                                }

                                startdate = Calendar.getInstance().getTime();
//                                NextBtn.setEnabled(true);
//                                waitingDialog.setVisibility(View.INVISIBLE);
                            } else {
                                waitingDialog.setVisibility(View.INVISIBLE);
                                NextBtn.setEnabled(true);
                                Toast.makeText(curContext, "Unable to Fetch Next Token. Please Try Again", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ex) {
                            NextBtn.setEnabled(true);
                            Log.d("MainResult", ex.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        waitingDialog.setVisibility(View.INVISIBLE);
                        NextBtn.setEnabled(true);
                        Toast.makeText(curContext, "Fetch Next Token Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                });
                startMoveNextTimer();
                break;
            case R.id.Renew:
                waitingDialog.setVisibility(View.VISIBLE);
                RenewBtn.setEnabled(false);
                Call<String> call2 = apiService.renewtokenResponse(merchantId, String.valueOf(CounterTxt.getText()));

                call2.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        try {
                            if (response.body() != null) {
                                waitingDialog.setVisibility(View.INVISIBLE);
                                Toast.makeText(curContext, "Successfully Renewed.Press Next Token.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ex) {
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        waitingDialog.setVisibility(View.INVISIBLE);
                        RenewBtn.setEnabled(true);
                        Toast.makeText(curContext, "Renewal Failed. Please Try Again", Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case R.id.DoneTokenbtn:


                break;
            case R.id.notifySwitch:
                isAutoNotifyOn = notifySwitch.isChecked();

                break;
        }
    }

    // added as an instance method to an Activity
    boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }

    @Override
    public void onBackPressed() {
        // code here to show dialog
        //super.onBackPressed();  // optional depending on your needs
        return;
    }

    private void startTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer=null;
        } else {
            // recreate new
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, 5000);
        }
        // schedule task

    }


    private void startMoveNextTimer() {
        if (mNextTimer != null) {
            mNextTimer.cancel();
            mNextTimer=null;
        } else {
            // recreate new
            mNextTimer = new Timer();
            mNextTimer.scheduleAtFixedRate(new TimeDisplayMoveToNextTask(), 0, 90000);
        }
    }
    public class TimeDisplayMoveToNextTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
//                    Toast.makeText(TokenEmpActivity.this, "hello", Toast.LENGTH_SHORT).show();
                    if(isAutoNotifyOn && NextBtn.isEnabled() && isNetworkConnectionAvailable()) {
//                        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//                        if(alert == null){
//                            // alert is null, using backup
//                            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                            if(alert != null){  // I can't see this ever being null (as always have a default notification) but just incase
//                                // alert backup is null, using 2nd backup
//                                mediaPlayer = MediaPlayer.create(getApplicationContext(), alert);
//                            }
//                            else
//                            {
//                                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);
//
//                            }
//                        }
//                        else
//                        {
                            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);
//                        }
//                        mediaPlayer = MediaPlayer.create(getApplicationContext(), alert);
                        mediaPlayer.start();


                    }
                }

            });
        }
    }



    public class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
//                    Toast.makeText(TokenEmpActivity.this, "hello", Toast.LENGTH_SHORT).show();
                    if (isNetworkConnectionAvailable()) {
                        callApi();
                    }
                }

            });
        }
    }

    public void UpdateMerchantDetails(String mid)
    {
        if (!mid.equalsIgnoreCase("-1")) {
            Call<String> getMerchantDetailscall = apiService.getMerchantDetails(mid);

            getMerchantDetailscall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                    try {
                        MerchantPayload mp = gson.fromJson(response.body(), MerchantPayload.class);
//                                               PhoneActivity
                        userdetails.deleteAll(userdetails.class);
                        final userdetails obj = new userdetails(mp.merchantPhn, mp.merchantName,
                                mp.imgurl, mp.shopNo,"",mp.locality,mp.area,mp.landMark,
                                mp.city,mp.state,String.valueOf(prefManager.getSelectedCategory()),mp.geoHash,mp.password,mp.role);
                        obj.save();
                        //mp.merchantId
                        //mp.merchantName;
//                        String nameStr="Hello " + mp.merchantName +" and Your ID is "+ mp.merchantId;
                        CustNoTextView.setText(mp.merchantName);
                        CustIDTextView.setText(mp.merchantId);
                        //mp.imgurl;
                        Picasso.with(curContext).load(mp.imgurl).resize(100, 100)
                                .centerInside().into(imageView);
                    } catch (Exception ex) {

                    }
                }

                @Override
                public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                    Toast.makeText(curContext, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    private void callApi() {
        String merchantId = prefManager.getMerchantID();
        Call<String> call = apiService.getanypendingtokens(merchantId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String data = response.body();
                if (data!=null && data.equalsIgnoreCase("-1")) {
                    //do something
                } else if (data.equalsIgnoreCase("1")) {
                    NextBtn.setEnabled(true);
                    RenewBtn.setEnabled(true);
                    Toast.makeText(TokenEmpActivity.this, "Please press next", Toast.LENGTH_SHORT).show();
                    if(mTimer!=null) {
                        mTimer.cancel();
                    }
                    mTimer=null;
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.notify);
                    if(mediaPlayer!=null)
                        mediaPlayer.start();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

}
