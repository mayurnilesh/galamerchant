package com.nearme.smartsell.menu;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;


import com.nearme.smartsell.R;

import java.io.File;
import java.net.URI;
import java.util.List;

public class PdfViewActivity extends Activity {
//    private static final String TAG = PdfViewActivity.class.getSimpleName();
//    public static final String SAMPLE_FILE = "android_tutorial.pdf";
//    //PDFView pdfView;
//    Integer pageNumber = 0;
//    String pdfFileName;
//    File fg;//

    @Override    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Make us non-modal, so that others can receive touch events.
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
//
//        // ...but notify us that it happened.
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
//
//        setContentView(R.layout.pdf_viewer);
//        this.setFinishOnTouchOutside(true);
//
//        Bundle  bundle = getIntent().getExtras();
//        pdfView= findViewById(R.id.pdfView);
//        if(bundle != null && bundle.containsKey("PDF_FILE")) {
//            displayFromAsset(bundle.getString("PDF_FILE"));
//        }
//    }
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        // If we've received a touch notification that the user has touched
//        // outside the app, finish the activity.
//        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
//            finish();
//            return true;
//        }
//
//        // Delegate everything else to Activity.
//        return super.onTouchEvent(event);
//    }
//    private void displayFromAsset(String assetFileName) {
//        pdfFileName = assetFileName;
//        try {
//            pdfFileName = new URI(pdfFileName).getRawPath();
//        }
//        catch(Exception e){
//
//        }
//        fg=new File(pdfFileName);
//        if(fg.exists()) {
////            pdfFileName = fg.getAbsolutePath();
//            try {
////                pdfView.fromAsset(pdfFileName)
//                pdfView.fromUri(Uri.parse(assetFileName))
//                        .defaultPage(pageNumber)
//                        .enableSwipe(true)
//                        .swipeHorizontal(false)
//                        .onPageChange(this)
//                        .enableAnnotationRendering(true)
//                        .onLoad(this)
//                        .scrollHandle(new DefaultScrollHandle(this))
//                        .load();
//            }
//            catch(Exception x){
//
//            }
//        }
//    }
//
//
//    @Override    public void onPageChanged(int page, int pageCount) {
//        pageNumber = page;
//        if (fg != null) {
//            setTitle(String.format("%s %s / %s", fg.getName(), page + 1, pageCount));
//        }
//    }
//
//
//    @Override    public void loadComplete(int nbPages) {
//        PdfDocument.Meta meta = pdfView.getDocumentMeta();
//        printBookmarksTree(pdfView.getTableOfContents(), "-");
//
//    }
//
//    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
//        for (PdfDocument.Bookmark b : tree) {
//
//            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
//
//            if (b.hasChildren()) {
//                printBookmarksTree(b.getChildren(), sep + "-");
//            }
//        }
    }

}