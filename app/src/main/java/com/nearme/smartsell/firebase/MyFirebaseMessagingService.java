package com.nearme.smartsell.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nearme.smartsell.R;
import com.nearme.smartsell.fragments.AnnouncementBizSendingFragment;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

//    @Override
//    public final void handleIntent(Intent intent) {
//
//        this.onMessageReceived(builder.build());
//
//    }

    /*
id:"666666"
FD:"14"
FM:07"
FY:"18"
TD:"29"
TM:"07"
TY:"18"
cat:"17"
"area":"kjnkjibjhbjhbjbhghgvhgvhgvghvhgvhgvhvhhf"
lat:"12.5678"
lng:"77.888"
url:"v1530819189/dcf63pcqzzfm161qrrll.jpg"
off:"10%"
sid:"abcsddsdvsd123"
des:"kjnkjnkjnjhbjhbhjbvhgvhgvhg lknkjnkjlnjk klnkljnkljnjknkjnkjnbhbhqjfbewqghfvqhgvqr
lknqwfbqbveb njwenfkjwenfkjf1n  kjnkjnkjnjhbjhbhjbvhgvhgvhg lknkjnkjlnjk
klnkljnkljnjknkjnkjnbhbhqjfbewqghfvqhgvqr lknqwfbqbveb njwenfkjwenfkjf1n kjnkjnkjnjhbjhbhjbvhgvhgvhg
lknkjnkjlnjkr"
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        final String data = remoteMessage.getData().toString();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                sendNotification(data);
            }
        });
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }


    private void sendNotification(String message) {
        Intent intent = new Intent(this, AnnouncementBizSendingFragment.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.arrow_shape)
                .setContentTitle("Firebase Push Notification")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
