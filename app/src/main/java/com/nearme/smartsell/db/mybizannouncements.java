package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class mybizannouncements extends SugarRecord {

    public String notificationid;
    public String timeStamp;
    public String validFromD;
    public String validFromM;
    public String validFromY;
    public String validTillD;
    public String validTillM;
    public String validTillY;
    public Integer category;
    public String areaname;
    public String imgurl;
    public String imghash;
    public String offer;
    public Long shopid;
    public double lat;
    public double lng;
    public String offerDescription;

    public mybizannouncements(String notificationid,String timeStamp,String validFromD,
                              String validFromM, String validFromY, String validTillD,
                              String validTillM, String validTillY, Integer category,
                              String areaname, String imgurl, String imghash,
                              String offer, Long shopid, String offerDescription,
                              double lat, double lng) {
        this.notificationid = notificationid;
        this.timeStamp =timeStamp;
        this.validFromD = validFromD;
        this.validFromM = validFromM;
        this.validFromY = validFromY;
        this.validTillD = validTillD;
        this.validTillM = validTillM;
        this.validTillY = validTillY;
        this.category = category;
        this.areaname = areaname;
        this.imgurl = imgurl;
        this.imghash = imghash;
        this.offer = offer;
        this.shopid = shopid;
        this.offerDescription = offerDescription;
        this.lat = lat;
        this.lng = lng;
    }

    public mybizannouncements() {
    }
}
