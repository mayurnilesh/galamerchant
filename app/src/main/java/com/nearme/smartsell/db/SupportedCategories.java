package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class SupportedCategories extends SugarRecord {
    Integer CategoryId;
    String CategoryName;

    public SupportedCategories(String categoryName, Integer CategoryId) {
        CategoryName = categoryName;
        this.CategoryId = CategoryId;
    }
}

