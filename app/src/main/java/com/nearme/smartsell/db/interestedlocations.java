package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class interestedlocations extends SugarRecord {
    String timestamp;
    String latitude;
    String longitude;
    String areaName;
    String geoHash;
}
