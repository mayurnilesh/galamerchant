package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class billingitem extends SugarRecord {
    public String merchantid;
    public String itemid;
    public String itemname;
    public String itemprice;
    public String itemdesc;
    public String itemunit;
    public String itemenum;
    public  Integer itemQuantity;
    public billingitem(){

    }

    public billingitem(String merchantid, String itemid, String itemname, String itemprice, String itemdesc,Integer itemQuantity
    ,String itemunit,String itemenum) {
        this.merchantid = merchantid;
        this.itemid = itemid;
        this.itemname = itemname;
        this.itemprice = itemprice;
        this.itemdesc = itemdesc;
        this.itemQuantity=itemQuantity;
        this.itemunit=itemunit;
        this.itemenum=itemenum;
    }
}
