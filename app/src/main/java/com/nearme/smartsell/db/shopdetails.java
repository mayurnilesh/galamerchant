package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class shopdetails extends SugarRecord{
    public Long shopid;
    public String lat;
    public String lng;
    public String shopName;
    public String shopContactNum;
    public String timestamp;

    public shopdetails(Long shopid, String shopName, String shopContactNum, String lat, String lng, String timestamp) {
        this.shopid = shopid;
        this.lat = lat;
        this.lng = lng;
        this.timestamp = timestamp;
        this.shopName = shopName;
        this.shopContactNum = shopContactNum;
    }

    public shopdetails() {

    }
}
