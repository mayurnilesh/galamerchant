package com.nearme.smartsell.db;

import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 04/08/18.
 */

public class adshistory extends SugarRecord {
    public String offerCode;
    public String offerDesc;
    public String expireDate;
    public String imageUrl;
    public String offerScanImgUrl;
    public String postedOn;
    public String notificationId;
    public String totalViwes;
    public String totalUtilizers;
    public String reachedTo;

    public adshistory(String offerCode, String offerDesc, String expireDate, String imageUrl, String offerScanImgUrl, String postedOn, String notificationId, String totalViwes, String totalUtilizers, String reachedTo) {
        this.offerCode = offerCode;
        this.offerDesc = offerDesc;
        this.expireDate = expireDate;
        this.imageUrl = imageUrl;
        this.offerScanImgUrl = offerScanImgUrl;
        this.postedOn = postedOn;
        this.notificationId = notificationId;
        this.totalViwes = totalViwes;
        this.totalUtilizers = totalUtilizers;
        this.reachedTo = reachedTo;
    }

    public adshistory() {
    }
}
