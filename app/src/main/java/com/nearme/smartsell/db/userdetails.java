package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class userdetails extends SugarRecord {
   public String ContactNumber;
    public String Name;
    public String ImageUrl;
    public String Shopno;
    public String Aadhaar;
    public String Locality;
    public String Area;
    public String Landmark;
    public String City;
    public String State;
    public String Category;
    public String merchantId;
    public String geoHash;
    public String password;
    public Integer role;

    public userdetails(String contactNumber, String name, String imageUrl,
                       String shopno, String aadhaar, String locality,
                       String area, String landmark, String city,
                       String state,String category, String geoHash,String password,Integer role) {
        ContactNumber = contactNumber;
        Name = name;
        ImageUrl = imageUrl;
        Shopno = shopno;
        Aadhaar = aadhaar;
        Locality = locality;
        Area = area;
        Landmark = landmark;
        City = city;
        State = state;
        Category=category;
        this.geoHash=geoHash;
        this.password=password;
        this.role=role;
    }

    public userdetails() {
    }
}

