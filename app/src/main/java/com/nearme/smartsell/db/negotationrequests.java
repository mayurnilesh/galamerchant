package com.nearme.smartsell.db;

import com.google.gson.annotations.SerializedName;
import com.nearme.smartsell.rest.AdPayLoad;
import com.orm.SugarRecord;

/**
 * Created by gopi.komanduri on 07/10/18.
 */

public class negotationrequests extends SugarRecord {
    public String customercontact;
    public String merchantid;
    public String geohash;
    public Integer minamount;
    public Integer maxamount;
    public Integer discountexpectation;
    public String shoppingprobabledates = "0";
    public String description;
    public Integer notificationid;
    public Integer idnegotations;
    public String receivedOn;

    public Integer response = 0;
    public String respondedOn = "0";
    public Integer revisedminamount = -1;
    public Integer revisedmaxamount = -1;
    public Integer reviseddiscount = 0;
    public String revisedshoppingalloweddates = "0";
    public Integer advance=0;
    public Integer advancepayment=0;
    public String imgurl;
    public String responded = "-1";
    public AdPayLoad adObj;

    public negotationrequests(String customercontact,
                              String merchantid,
                              String geohash, Integer minamount,
                              Integer maxamount, Integer discountexpectation,
                              String shoppingprobabledates, String description,
                              Integer notificationid, Integer idnegotations,
                              String receivedOn, Integer response,String imgurl) {
        this.imgurl = imgurl;
        this.customercontact = customercontact;
        this.merchantid = merchantid;
        this.geohash = geohash;
        this.minamount = minamount;
        this.maxamount = maxamount;
        this.discountexpectation = discountexpectation;
        this.shoppingprobabledates = shoppingprobabledates;
        this.description = description;
        this.notificationid = notificationid;
        this.idnegotations = idnegotations;
        this.receivedOn = receivedOn;
        this.response = response;
        this.receivedOn = "0";
        this.revisedmaxamount = -1;
        this.reviseddiscount = 0;
        this.revisedshoppingalloweddates = "0";
    }

    public negotationrequests() {

    }

    public String getResponded() {
        return responded;
    }

    public void setResponded(String responded) {
        this.responded = responded;
    }

    public String getCustomercontact() {
        return customercontact;
    }

    public void setCustomercontact(String customercontact) {
        this.customercontact = customercontact;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public Integer getMinamount() {
        return minamount;
    }

    public void setMinamount(Integer minamount) {
        this.minamount = minamount;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public void setMaxamount(Integer maxamount) {
        this.maxamount = maxamount;
    }

    public Integer getDiscountexpectation() {
        return discountexpectation;
    }

    public void setDiscountexpectation(Integer discountexpectation) {
        this.discountexpectation = discountexpectation;
    }

    public String getShoppingprobabledates() {
        return shoppingprobabledates;
    }

    public void setShoppingprobabledates(String shoppingprobabledates) {
        this.shoppingprobabledates = shoppingprobabledates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(Integer notificationid) {
        this.notificationid = notificationid;
    }

    public Integer getIdnegotations() {
        return idnegotations;
    }

    public void setIdnegotations(Integer idnegotations) {
        this.idnegotations = idnegotations;
    }

    public String getReceivedOn() {
        return receivedOn;
    }

    public void setReceivedOn(String receivedOn) {
        this.receivedOn = receivedOn;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public String getRespondedOn() {
        return respondedOn;
    }

    public void setRespondedOn(String respondedOn) {
        this.respondedOn = respondedOn;
    }

    public Integer getRevisedminamount() {
        return revisedminamount;
    }

    public void setRevisedminamount(Integer revisedminamount) {
        this.revisedminamount = revisedminamount;
    }

    public Integer getRevisedmaxamount() {
        return revisedmaxamount;
    }

    public void setRevisedmaxamount(Integer revisedmaxamount) {
        this.revisedmaxamount = revisedmaxamount;
    }

    public Integer getReviseddiscount() {
        return reviseddiscount;
    }

    public void setReviseddiscount(Integer reviseddiscount) {
        this.reviseddiscount = reviseddiscount;
    }

    public String getRevisedshoppingalloweddates() {
        return revisedshoppingalloweddates;
    }

    public void setRevisedshoppingalloweddates(String revisedshoppingalloweddates) {
        this.revisedshoppingalloweddates = revisedshoppingalloweddates;
    }

    public Integer getAdvance() {
        return advance;
    }

    public void setAdvance(Integer advance) {
        this.advance = advance;
    }

    public Integer getAdvancepayment() {
        return advancepayment;
    }

    public void setAdvancepayment(Integer advancepayment) {
        this.advancepayment = advancepayment;
    }
}
