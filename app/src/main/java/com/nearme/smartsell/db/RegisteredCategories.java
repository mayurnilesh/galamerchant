package com.nearme.smartsell.db;

import com.orm.SugarRecord;

public class RegisteredCategories extends SugarRecord {
    Integer CategoryId;
    Integer IsActive;
    String DateRegistered;

    public RegisteredCategories(Integer categoryId, Integer isActive, String dateRegistered) {
        CategoryId = categoryId;
        IsActive = isActive;
        DateRegistered = dateRegistered;
    }

    public RegisteredCategories() {
    }
}
