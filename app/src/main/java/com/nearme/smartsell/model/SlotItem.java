package com.nearme.smartsell.model;

public class SlotItem {
    String fromTime;
    String toTime;
    String date;
    String maxToken;

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMaxToken() {
        return maxToken;
    }

    public void setMaxToken(String maxToken) {
        this.maxToken = maxToken;
    }
}
