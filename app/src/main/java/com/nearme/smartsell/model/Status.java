package com.nearme.smartsell.model;

/**
 * Created by gopikomanduri on 05/05/18.
 */
public class Status{
    public boolean isOnline;
    public long timestamp;

    public Status(){
        isOnline = false;
        timestamp = 0;
    }
}
