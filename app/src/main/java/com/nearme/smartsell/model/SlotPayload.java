package com.nearme.smartsell.model;

import java.util.List;

public class SlotPayload {
    public  String MerchantID;
    public  String FromDate;
    public  String ToDate;
    public  List<String> FromTime;
    public  List<String> ToTime;
    public  List<Integer> MaxToken;
}
