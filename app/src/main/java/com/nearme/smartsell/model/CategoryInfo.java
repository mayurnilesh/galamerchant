package com.nearme.smartsell.model;

public class CategoryInfo {
    public String imgIcon;
    public String txtTitle;
    public String txtId;
    public boolean chkSelect;
}