package com.nearme.smartsell.model;

import com.google.gson.annotations.SerializedName;

public class CounterData {
    @SerializedName("counterID")
    public String counterID;
    @SerializedName("tokenID")
    public String tokenID;

    public String getCounterID() {
        return counterID;
    }

    public void setCounterID(String counterID) {
        this.counterID = counterID;
    }

    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }
}
