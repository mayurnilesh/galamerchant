package com.nearme.smartsell.model;

public class Invoice {

    public String MerchantShopName;
    public String BillNumber;
    public String CustomerName;
    public String CustomerID;
    public String BillAmount;
    public String BillPeriod;
    public String MerchantAddress;
    public String MerchantContact;

}
