package com.nearme.smartsell.model;

import com.nearme.smartsell.rest.HistoryResponsePayload;

public class AdPayLoadResponse extends HistoryResponsePayload {
    public int Id;
    public Integer getID() {
        return Id;
    }
}