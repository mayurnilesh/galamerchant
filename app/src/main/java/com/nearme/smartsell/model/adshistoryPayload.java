package com.nearme.smartsell.model;

import com.nearme.smartsell.rest.LastReceivedAdStruct;

public class adshistoryPayload {
    String merchantid;
    LastReceivedAdStruct[] geohash;

    public adshistoryPayload(String merchantid, LastReceivedAdStruct[] geohash) {
        this.geohash = geohash;
        this.merchantid = merchantid;
    }
}
