package com.nearme.smartsell.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences constants
    private static final String PREF_NAME = "MyPreference";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_REG_DONE_CATEGORY_SELECTED = "IsRegistrationDoneAndCategorySelected";
    private static final String SELECTED_CATEGORIES = "SelectedCategories";
    private static final String LOGIN_TYPE = "LoginType";
    private static final String COUNTER_NUMBER = "counterNumber";
    private static final String MERCHANT_ID = "MerchantID";
    private static final String MERCHANT_FIRE_UID = "MerchantFireUID";
    private static final String MERCHANT_NAME = "MerchantName";
    private static final String MERCHANT_GHASH = "MerchantGHash";
    private static final String MERCHANT_ADDRESS = "MerchantAddress";
    private static final String MERCHANT_CONTACT = "MerchantContact";
    private static final String MERCHANT_LOGO_URI = "MerchantLogo";
    private static final String BILL_DATA = "billdata";
    private static final String POSTEDTOMAIN = "postetdtomain";
    private static final String LATITUDE = "LATITUDE";
    private static final String LONGITUDE = "LONGITUDE";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }
    public void setPostedToMain(boolean isnavigatedtomain) {
        editor.putBoolean(POSTEDTOMAIN, isnavigatedtomain);
        editor.commit();
    }

    public void setCategorySelected(boolean isFirstTime) {
        editor.putBoolean(IS_REG_DONE_CATEGORY_SELECTED, isFirstTime);
        editor.commit();
    }

    public void setLatitude(String lat) {
        editor.putString(LATITUDE, lat);
        editor.commit();
    }

    public void setLongitude(String lng) {
        editor.putString(LONGITUDE, lng);
        editor.commit();
    }

    public void setSelectedCategory(long selectedCategories) {
        editor.putLong(SELECTED_CATEGORIES, selectedCategories);
        editor.commit();
    }

    public void setLoginType(String loginType) {
        editor.putString(LOGIN_TYPE, loginType);
        editor.commit();
    }

    public void setCounterNumber(String CounterNum) {
        editor.putString(COUNTER_NUMBER, CounterNum);
        editor.commit();
    }

    public void setBillData(String data) {
        editor.putString(BILL_DATA, data);
        editor.commit();
    }

    public void setMerchantID(String MerchantID) {
        editor.putString(MERCHANT_ID, MerchantID);
        editor.commit();
    }
    public void setMerchantFireUID(String MerchantFireUID) {
        editor.putString(MERCHANT_FIRE_UID, MerchantFireUID);
        editor.commit();
    }
    public void setMerchantName(String MerchantName) {
        editor.putString(MERCHANT_NAME, MerchantName);
        editor.commit();
    }

    public void setMerchantGeoHash (String MerchantGHash) {
        editor.putString(MERCHANT_GHASH, MerchantGHash);
        editor.commit();
    }

    public void setMerchantAddress(String MerchantAddress) {
        editor.putString(MERCHANT_ADDRESS, MerchantAddress);
        editor.commit();
    }

    public void setMerchantContact(String MerchantContact) {
        editor.putString(MERCHANT_CONTACT, MerchantContact);
        editor.commit();
    }

    public void setMerchantLogoURI(String MerchantLogoUri) {
        editor.putString(MERCHANT_LOGO_URI, MerchantLogoUri);
        editor.commit();
    }

    public String getMerchantGeoHash() {
        return pref.getString(MERCHANT_GHASH, "-1");
    }
    public String getLatitude() {
        return pref.getString(LATITUDE, "-1");
    }

    public String getLongitude() {
        return pref.getString(LONGITUDE, "-1");
    }

    public boolean getPostedtomain() {
        return pref.getBoolean(POSTEDTOMAIN, false);
    }

    public String getMerchantLogoURI() {
        return pref.getString(MERCHANT_LOGO_URI, "-1");
    }

    public String getMerchantContact() {
        return pref.getString(MERCHANT_CONTACT, "-1");
    }

    public String getMerchantName() {
        return pref.getString(MERCHANT_NAME, "-1");
    }

    public String getMerchantAddress() {
        return pref.getString(MERCHANT_ADDRESS, "-1");
    }

    public String getMerchantID() {
        return pref.getString(MERCHANT_ID, "-1");
    }

    public String getMerchantFireUID() {
        return pref.getString(MERCHANT_FIRE_UID, "-1");
    }

    public String getCounterNumber() {
        return pref.getString(COUNTER_NUMBER, "-1");
    }

    public String getLoginType() {
        return pref.getString(LOGIN_TYPE, "-1");
    }

    public String getBillData() {
        return pref.getString(BILL_DATA, "[]");
    }

    public long getSelectedCategory() {
        return pref.getLong(SELECTED_CATEGORIES, 0);
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean isCategorySelected() {
        return pref.getBoolean(IS_REG_DONE_CATEGORY_SELECTED, false);
    }
}
