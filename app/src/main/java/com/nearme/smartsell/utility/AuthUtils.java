package com.nearme.smartsell.utility;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;

import com.cloudinary.utils.ObjectUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.nearme.smartsell.R;
import com.nearme.smartsell.firebasedata.StaticConfig;
import com.nearme.smartsell.startup.PhoneActivity;
import com.nearme.smartsell.startup.UserDetailsActivity;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import static android.content.ContentValues.TAG;
import static com.nearme.smartsell.startup.PhoneActivity.user;

/**
 * Created by gopi.komanduri on 10/08/18.
 */

public class AuthUtils {
    /**
     * Action register
     *
     * @param email
     * @param password
     *
     */
    public ProgressBar waitingDialog = null;
    public FirebaseAuth mAuth = null;
    public PhoneActivity curActivity = null;

    public void createUser(String email, String password, final FirebaseAuth mAuth,
                           final ProgressBar waitingDialog, final Context context) {

        waitingDialog.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(curActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            waitingDialog.setVisibility(View.VISIBLE);

                            Object []params = new Object[5];
                            params[0] = curActivity.cloudinary;
                            params[1] = PhoneActivity.enteredImageUri;
                            params[2] = ObjectUtils.emptyMap();
                            params[3] = curActivity;
                            params[4] = 0;
                            new ImageUploader(context).execute(params);
                            Object []proofparams = new Object[5];
                            proofparams[0] = curActivity.cloudinary;
                            proofparams[1] = PhoneActivity.enteredIDProofUri;
                            proofparams[2] = ObjectUtils.emptyMap();
                            proofparams[3] = curActivity;
                            proofparams[4] = -1;
                            new ImageUploader(context).execute(proofparams);


                        } else {

                            user = mAuth.getCurrentUser();

                            waitingDialog.setVisibility(View.VISIBLE);
                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            Object []params = new Object[5];
                            params[0] = curActivity.cloudinary;
                            params[1] = PhoneActivity.enteredImageUri;
                            params[2] = ObjectUtils.emptyMap();
                            params[3] = curActivity;

                             params[4] = 0;
                            new ImageUploader(context).execute(params);

                            Object []proofparams = new Object[5];
                            proofparams[0] = curActivity.cloudinary;
                            proofparams[1] = PhoneActivity.enteredIDProofUri;
                            proofparams[2] = ObjectUtils.emptyMap();
                            proofparams[3] = curActivity;
                            proofparams[4] = -1;
                            new ImageUploader(context).execute(proofparams);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        waitingDialog.setVisibility(View.GONE);
                    }
                })
        ;
    }



    /**
     * Action reset password
     *
     * @param email
     */
    public void resetPassword(final String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        new LovelyInfoDialog(curActivity) {
                            @Override
                            public LovelyInfoDialog setConfirmButtonText(String text) {
                                findView(com.yarolegovich.lovelydialog.R.id.ld_btn_confirm).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dismiss();
                                    }
                                });
                                return super.setConfirmButtonText(text);
                            }
                        }
                                .setTopColorRes(com.nearme.smartsell.R.color.colorPrimary)
                                .setIcon(com.nearme.smartsell.R.drawable.ic_pass_reset)
                                .setTitle("Password Recovery")
                                .setMessage("Sent contact to " + email)
                                .setConfirmButtonText("Ok")
                                .show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        new LovelyInfoDialog(curActivity) {
                            @Override
                            public LovelyInfoDialog setConfirmButtonText(String text) {
                                findView(com.yarolegovich.lovelydialog.R.id.ld_btn_confirm).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dismiss();
                                    }
                                });
                                return super.setConfirmButtonText(text);
                            }
                        }
                                .setTopColorRes(R.color.colorPrimaryDark)
                                .setIcon(com.nearme.smartsell.R.drawable.ic_pass_reset)
                                .setTitle("False")
                                .setMessage("False to sent contact to " + email)
                                .setConfirmButtonText("Ok")
                                .show();
                    }
                });
    }


    public void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
        curActivity.mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    StaticConfig.UID = user.getUid();
                    ServiceUtils.Uid = user.getUid();
                } else {
                    curActivity.finish();
                    // User is signed in
                    //Gopi ..

                    curActivity.startActivity(new Intent(curActivity, UserDetailsActivity.class));
                    //   startActivity(new Intent(MainActivity.this, PhoneActivity.class));
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }

    public void  uploadUserImg(Context context) {

            Object[] params = new Object[5];
            params[0] = curActivity.cloudinary;
            params[1] = PhoneActivity.enteredImageUri;
            params[2] = ObjectUtils.emptyMap();
            params[3] = curActivity;
        params[4] = 0;
            new ImageUploader(context).execute(params);

        if (PhoneActivity.enteredIDProofUri != null) {
            Object[] proofparams = new Object[5];
            proofparams[0] = curActivity.cloudinary;
            proofparams[1] = PhoneActivity.enteredIDProofUri;
            proofparams[2] = ObjectUtils.emptyMap();
            proofparams[3] = curActivity;
            params[4] = -1;
            new ImageUploader(context).execute(proofparams);
        }
    }
}