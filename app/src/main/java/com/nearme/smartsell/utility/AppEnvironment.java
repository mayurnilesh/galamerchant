package com.nearme.smartsell.utility;

public enum AppEnvironment {

    SANDBOX {
        @Override
        public String merchant_Key() {
            //return "JkvPCcUc";
            return "LZHOotes";
        }

        @Override
        public String merchant_ID() {
            //return "6692430";
            return "6750984";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            //return "bYtZxrYdpD";
            return "LBcmBkaXUr";
        }

        @Override
        public boolean debug() {
            return false;
        }
    },
    PRODUCTION {
        @Override
        public String merchant_Key() {
            return "JkvPCcUc";
        }

        @Override
        public String merchant_ID() {
            return "6692430";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "bYtZxrYdpD";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();


}
