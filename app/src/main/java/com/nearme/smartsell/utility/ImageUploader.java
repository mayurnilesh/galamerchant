package com.nearme.smartsell.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.cloudinary.Cloudinary;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.nearme.smartsell.R;
import com.nearme.smartsell.db.adshistory;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.firebasedata.SharedPreferenceHelper;
import com.nearme.smartsell.location.MyLocationListener;
import com.nearme.smartsell.rest.AdPayLoad;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.startup.PhoneActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.nearme.smartsell.fragments.AnnouncementBizSendingFragment.QRcodeWidth;
import static com.nearme.smartsell.fragments.AnnouncementBizSendingFragment.mImageUrl;
import static com.nearme.smartsell.fragments.AnnouncementBizSendingFragment.mOfferImageUrl;
import static org.greenrobot.eventbus.EventBus.TAG;


/**
 * Created by gopikomanduri on 03/02/18.
 */

public class ImageUploader extends AsyncTask {
    private static final String IMAGE_DIRECTORY = "/Gala";
    private Integer typeReceived = 0;
    private Integer tilldate; // offer valid till
    private Integer tillmonth; // offer valid till
    private Integer tillyear; // offer valid till
    private String category; // category
    private String offercode; //offercode
    private Integer nego; //offercode
    private String offerdesc; //item description
    private String merchantId = "";
    private String Contact = "";
    private String billNo = "";
    private String BillAmt = "";
    private Activity curActivity;
    private Integer minBusiness;
    private PrefManager prefManager;
    private String qrImagePath = "";
    private CountDownTimer countdowntimer;
    private Context mContext;
    private String geoHash, lat, lng, eventId;
    private String isOtherLocation = "no";
    private String price;
    private String quantity;

    public ImageUploader(Context context) {
        mContext = context;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        curActivity = (Activity) objects[3];
        if (objects.length > 4 && objects[4] != null) {
            typeReceived = Integer.parseInt(objects[4].toString());
            if (typeReceived == 2) {
                if (objects[5] != null)
                    merchantId = objects[5].toString();
                if (objects[6] != null)
                    Contact = objects[6].toString();
                if (objects[7] != null)
                    billNo = objects[7].toString();
                BillAmt = objects[8].toString();
            }
        }

        if (typeReceived == 1 && objects.length > 5) //typeReceived is 2 means it is from invoice
        {
            tilldate = Integer.parseInt(objects[5].toString());
            tillmonth = Integer.parseInt(objects[6].toString());
            tillyear = Integer.parseInt(objects[7].toString());
            category = objects[8].toString();
            offercode = objects[9].toString();
            nego = Integer.parseInt(objects[14].toString());
            offerdesc = objects[10].toString();
            eventId = objects[18].toString();
            price = objects[20].toString();
            quantity = objects[19].toString();
            if (objects.length > 11) {
                merchantId = objects[11].toString();
                minBusiness = Integer.parseInt(objects[13].toString());
            }
            if (objects[2].equals("yes")) {
                geoHash = objects[17].toString();
                lat = objects[15].toString();
                lng = objects[16].toString();
                isOtherLocation = "yes";
            }

            Bitmap bitmap;
            try {
                bitmap = TextToImageEncode(merchantId, offercode);
                qrImagePath = saveImage(bitmap);

            } catch (WriterException e) {
                e.printStackTrace();
            }

        }

        Map x = null;
        Map qrX = null;
        Cloudinary cloudinaryObj = (Cloudinary) objects[0];
        Uri imgPath = (Uri) objects[1];

        try {
            Map<String, String> options = new HashMap<>();
            if (merchantId.length() > 0)
                options.put("folder", merchantId);
            if (objects[1] != null) {
                InputStream in = null;
                if (!imgPath.getLastPathSegment().endsWith(".pdf"))
                    in = curActivity.getContentResolver().openInputStream(imgPath);
                else
                    in = new FileInputStream(imgPath.getPath());
                if (in != null) {
                    x = cloudinaryObj.uploader().upload(in, options);
                    if (x != null && x.containsKey("url") == true) {
                        mImageUrl = x.get("url").toString();
                    }

                    if (qrImagePath.length() > 0) {
                        qrX = cloudinaryObj.uploader().upload(qrImagePath, options);
                    }
                    if (qrX != null && qrX.containsKey("url") == true) {
                        mOfferImageUrl = qrX.get("url").toString();
                    }
                }
                int y = 10;
            }
        } catch (IOException e) {
            if (objects.length > 4)
                e.printStackTrace();
        }

        if (x != null)
            Log.i("uploaded", x.toString());
        return x;
    }


    @SuppressLint("WrongConstant")
    private void showNotification(String str) {

        NotificationCompat.Builder mBuilder;
        String NOTIFICATION_CHANNEL_ID = "GALAMERCHANT";
        NotificationManager notificationManager;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent resultIntent = new Intent();

            PendingIntent requestPendingIntent = PendingIntent.getActivity(curActivity, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder = new NotificationCompat.Builder(curActivity, NOTIFICATION_CHANNEL_ID);
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);

            NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
            textStyle.bigText(str);

            mBuilder.setContentTitle("Gala Mercahnt")
                    .setBadgeIconType(R.mipmap.ic_launcher)
                    .setContentText(str)
                    .setStyle(textStyle)
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .setAutoCancel(false)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(requestPendingIntent);
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NotificationChannel", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(100);
            notificationManager = (NotificationManager) curActivity.getSystemService(NOTIFICATION_SERVICE);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(0, mBuilder.build());
        } else {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(curActivity, "0");
            builder.setContentTitle("Gala Mercahnt");
            builder.setContentText(str);
            builder.setAutoCancel(true);
            builder.setSmallIcon(R.mipmap.ic_launcher_round);
            builder.setSound(uri);
            builder.setOnlyAlertOnce(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setLargeIcon(BitmapFactory.decodeResource(curActivity.getResources(), R.mipmap.ic_launcher_round));
                builder.setColor(curActivity.getResources().getColor(R.color.white));
            }
            Intent resultIntent = new Intent();
            PendingIntent pendingIntent = PendingIntent.getActivity(curActivity, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            notificationManager = (NotificationManager) curActivity.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify((int) System.currentTimeMillis(), builder.build());
        }
    }

    private Bitmap TextToImageEncode(String merchantId, String offercode) throws WriterException {
        BitMatrix bitMatrix;
        String Value = merchantId + ":" + offercode;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        curActivity.getResources().getColor(R.color.BrightBlack)
                        : curActivity.getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.

        if (!wallpaperDirectory.exists()) {
            Log.d("dirrrrrr", "" + wallpaperDirectory.mkdirs());
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();   //give read write permission
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(curActivity,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";

    }

    @Override
    protected void onPostExecute(Object o) {

        if (mImageUrl.length() > 0) {

            if (typeReceived == 1) {
                Gson gson = new Gson();
                if (MyLocationListener.postingLocation.getLatitude() != 0.0 && MyLocationListener.postingLocation.getLongitude() != 0.0) {
                    MyLocationListener.postingLocation.setLatitude(0.0);
                    MyLocationListener.postingLocation.setLongitude(0.0);
                }

                List<userdetails> temp = userdetails.listAll(userdetails.class);
                final String merchantId = temp.get(0).merchantId;
                if (prefManager == null) {
                    prefManager = new PrefManager(mContext);
                }
                Calendar calendar = Calendar.getInstance();

                if (!isOtherLocation.equals("yes")) {
                    geoHash = prefManager.getMerchantGeoHash();
                    lat = prefManager.getLatitude();
                    lng = prefManager.getLongitude();
                }
                AdPayLoad obj = new AdPayLoad(geoHash, lat, lng, merchantId,
                        category, tilldate, tillmonth, tillyear,
                        calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR),
                        offercode, offerdesc, mImageUrl,
                        nego, minBusiness, "", "", "", price, quantity);

                String jsonConvertedlastReceivedAdStructList = gson.toJson(obj, AdPayLoad.class);


                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);
                long millisInFuture = 10000; //30 seconds
                long countDownInterval = 1000; //1 second

                countdowntimer = new CountDownTimer(millisInFuture, countDownInterval) {
                    @Override
                    public void onTick(long l) {
                        int x = 40;
                        ++x;
                    }

                    @Override
                    public void onFinish() {

                        String error = "Ad sent failed. Please re-try!";
                        showNotification(error);
                        Log.e(TAG, "Ad sent failed. Please re-try!");
                        curActivity.startActivity(new Intent(curActivity, ManageSlotsActivity.class));

                    }
                }.start();

                Call<AdPayLoad> call = apiService.pushAd(jsonConvertedlastReceivedAdStructList, eventId);

                call.enqueue(new Callback<AdPayLoad>() {
                    @Override
                    public void onResponse(Call<AdPayLoad> call, Response<AdPayLoad> response) {
                        countdowntimer.cancel();
                        String validity = tilldate + ":" + tillmonth + ":" + tillyear;

                        try {
                            showNotification("Ad sent!");
                            String adId = "";
                            if (response.body() != null) {
                                adId = response.body().geo;
                            }
                            Log.d(TAG, "onResponse: " + adId);

                            String curTime = DateTime.getTimeStamp();

                            final adshistory obj = new adshistory(offercode, offerdesc,
                                    validity, mImageUrl, mOfferImageUrl, curTime,
                                    adId, "0", "0", "0");
                            obj.save();

                            AlertDialog.Builder builder = new AlertDialog.Builder(curActivity);
                            builder.setTitle("Gala Merchant");
                            builder.setMessage("Ad sent Successfully");
                            builder.setCancelable(false);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    curActivity.startActivity(new Intent(curActivity, ManageSlotsActivity.class));
                                    curActivity.finish();
                                }
                            });
                            builder.show();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdPayLoad> call, Throwable t) {
                        // Log error here since request failed
                        //Gopi timer
                        //  stoptimertask();
                        countdowntimer.cancel();

                        String error = "Ad sent failed. " + t.getMessage() + " Please re-try!";
                        showNotification(error);
                        Log.e(TAG, t.toString());
                        curActivity.startActivity(new Intent(curActivity, ManageSlotsActivity.class));
                        curActivity.finish();
                    }
                });


            } else if (typeReceived == 2) {
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);

                Call<String> call = apiService.
                        posttospecificcontactResponse(merchantId, Contact, mImageUrl, "1", billNo, BillAmt);

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        try {
                            if (response.body() != null) {
                                Log.e(TAG, response.body().toString());
                                showNotification("Ad sent!");
                                AlertDialog.Builder builder = new AlertDialog.Builder(curActivity);
                                builder.setTitle("Gala Merchant");
                                builder.setMessage("Ad sent Successfully");
                                builder.setCancelable(false);
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        curActivity.startActivity(new Intent(curActivity, ManageSlotsActivity.class));
                                        curActivity.finish();
                                    }
                                });
                                builder.show();

                            }
                        } catch (Exception ex) {
                            Log.e(TAG, ex.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            } else {
                if (typeReceived == 0 && PhoneActivity.enteredIDProofUristrng != null) {
                    PhoneActivity.enteredImageUristrng = mImageUrl;
                    SharedPreferenceHelper.getInstance(PhoneActivity.baseCtx).saveusercontact("registered");
                    PhoneActivity.currentObj.initNewUserInfo(PhoneActivity.enteredImageUristrng);
                } else if (typeReceived == 0) {
                    PhoneActivity.enteredImageUristrng = mImageUrl;
                }
                if (typeReceived == -1 && PhoneActivity.enteredImageUristrng != null) {
                    PhoneActivity.enteredIDProofUristrng = mImageUrl;
                    SharedPreferenceHelper.getInstance(PhoneActivity.baseCtx).saveusercontact("registered");
                    PhoneActivity.currentObj.initNewUserInfo(PhoneActivity.enteredImageUristrng);
                } else if (typeReceived == -1) {
                    PhoneActivity.enteredIDProofUristrng = mImageUrl;
                }
            }
            // remove below line once firebase auth is working..


        } else if (typeReceived == 0) {
            int x = 30;
            Log.i("Gopi Error", "error while uploading into cloudinary");
            curActivity.startActivity(new Intent(curActivity, ManageSlotsActivity.class));
            curActivity.finish();
        }
    }

}
