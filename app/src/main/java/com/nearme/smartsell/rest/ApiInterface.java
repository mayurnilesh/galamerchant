package com.nearme.smartsell.rest;

import com.nearme.smartsell.model.AdPayLoadResponse;
import com.nearme.smartsell.model.MerchantCriteria;
import com.nearme.smartsell.model.MerchantEvents;
import com.nearme.smartsell.model.Merchantslot;
import com.nearme.smartsell.model.adshistoryPayload;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiInterface {
    @POST("pushad")
    Call<AdPayLoad> pushAd(@Query("ad") String lastReceivedList,@Query("eventID") String eventID);

    @POST("RegMerchant")
    Call<MerchantPayload> RegMerchant(@Query("merchant") String merchantdetails);

    @POST("pushjob")
    Call<String> getPushJob(@Query("job") String merchant);

    @POST("latestversionmerchant")
    Call<versionclass> getVersion();

    @POST("MerchantChangePwd")
    Call<String> MerchantChangePwd(@Query("merchantid") String merchantPhno, @Query("password") String password);

    @POST("fetchacategories")
    Call<CategoriesPayLoad[]> getCategories();

    @POST("fetchnegotiations")
    Call<NegotationPayLoad[]> fetchNegotiations(@Query("merchantid") String merchantId, @Query("geohash") String geoHash, @Query("lastid") String lastid);

    @POST("negotiationresponse")
    Call<String> negotiationresponse(@Query("merchantid") String merchantId, @Query("geohash") String geoHash,
                                     @Query("negotiationid") String negotiationid,
                                     @Query("negotiationresponsepayload") String negotiationresponsepayload);

    @POST("login")
    Call<String> loginresponse(@Query("merchantid") String merchantId, @Query("password") String password);

    @POST("createcounters")
    Call<String> createCountersresponse(@Query("merchantid") String merchantid, @Query("count") String count, @Query("helpers") String helpers);

    @POST("getnexttoken")
    Call<String> GenerateNextTokenresponse(@Query("merchantid") String merchantId,
                                           @Query("counter") String counter, @Query("existingtoken") Integer existingtoken,
                                           @Query("timeserved") Double timeserved, @Query("starttime") String starttime,
                                           @Query("endTime") String endTime);

    @POST("getmerchantdetails")
    Call<String> getMerchantDetails(@Query("merchantcontact") String merchantcontact);

    @POST("gettokenlogforcounter")
    Call<String> gettokenlogforcounterResponse(@Query("merchantid") String merchantid, @Query("counterid") String counterid);

    @POST("getmerchantcounterdetails")
    Call<String> getmerchantcounterdetailsResponse(@Query("merchantcontact") String merchantcontact);

    @POST("renewtoken")
    Call<String> renewtokenResponse(@Query("merchantid") String merchantid, @Query("token") String token);

    @POST("posttospecificcontact")
    Call<String> posttospecificcontactResponse(@Query("merchantid") String merchantid, @Query("contact") String contact, @Query("msgurl") String msgurl,
                                               @Query("type") String type, @Query("receiptid") String receiptid, @Query("billamount") String billamount);

    //getbdaycount
    @POST("getbdaycount")
    Call<String> getbdaycountResponse(@Query("merchantid") String merchantid);

    @POST("registerfortoken")
    Call<String> registerfortokenResponse(@Query("merchantid") String merchantid, @Query("consumercontact") String consumercontact);

    @POST("registerfortokenthruhelper")
    Call<String> registerfortokenthruhelper(@Query("merchantid") String merchantid, @Query("consumercontact") String consumercontact);


    @POST("closecounters")
    Call<String> closecountersResponse(@Query("merchantid") String merchantid);

    @POST("pendingtokens")
    Call<String[]> pendingtokensResponse(@Query("merchantid") String merchantid);

    @POST("getnextreceiptid")
    Call<String> getnextreceiptidResponse(@Query("merchantid") String merchantid);

    @POST("getreceipthistory")
    Call<ContactSpecificInfo[]> getreceipthistoryResponse(@Query("merchantid") String merchantid, @Query("lastreceiptid") String lastreceiptid);

    @POST("currenttokenforcounterswithmerchantid")
    Call<String> getcurrenttokenforcounterwithmerchantid(@Query("merchantid") String merchantid, @Query("counter") String counter);


    @POST("currenttokenforallcounterswithmerchantid")
    Call<String> getcurrenttokenforallcounterswithmerchantid(@Query("merchantid") String merchantid);

    @POST("anypendingtokens")
    Call<String> getanypendingtokens(@Query("merchantid") String merchantid);

    @POST("adshistory")
    Call<HistoryResponsePayload[]> getAdsHistory(@Query("geohash") String geohash, @Query("merchantid") String merchantid);

    @POST("registerMerchantSlots")
    Call<Merchantslot[]> addSlots(@Query("slotDetails") String slotDetails);

    @POST("getMerchantSlots")
    Call<Merchantslot[]> getSlots(@Query("MerchantID") String MerchantID, @Query("FromTime") String FromTime, @Query("toTime") String toTime);

    @POST("registerMyCategories")
    Call<String> registerMyCategories(@Query("selectedCatVal") String selectedCatVal, @Query("merchantID") String merchantID);

    @POST("getMyCategories")
    Call<String> getMyCategories(@Query("merchantID") String merchantID);

    @POST("getMerchantEventCriterias")
    Call<MerchantCriteria[]> getMerchantEventCriteria();

    @POST("getMerchantEventOperations")
    Call<String[]> getMerchantEventOperations();

    @POST("registerMerchantEvent")
    Call<String> registerMerchantEvent(@Query("eventCondition") String eventCondition, @Query("merchantID") String merchantID);

    @POST("getMerchantEvents")
    Call<MerchantEvents[]> getMerchantEvents(@Query("merchantID") String merchantID);

}