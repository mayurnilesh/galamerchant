package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gopi.komanduri on 07/10/18.
 */

public class NegotationPayLoad {
    public String getCustomercontact() {
        return customercontact;
    }

    public void setCustomercontact(String customercontact) {
        this.customercontact = customercontact;
    }

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public Integer getMinamount() {
        return minamount;
    }

    public void setMinamount(Integer minamount) {
        this.minamount = minamount;
    }

    public Integer getMaxamount() {
        return maxamount;
    }

    public void setMaxamount(Integer maxamount) {
        this.maxamount = maxamount;
    }

    public Integer getDiscountExpectation() {
        return DiscountExpectation;
    }

    public void setDiscountExpectation(Integer discountExpectation) {
        DiscountExpectation = discountExpectation;
    }

    public String getShoppingProbableDates() {
        return ShoppingProbableDates;
    }

    public void setShoppingProbableDates(String shoppingProbableDates) {
        ShoppingProbableDates = shoppingProbableDates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDelieverd() {
        return delieverd;
    }

    public void setDelieverd(Integer delieverd) {
        this.delieverd = delieverd;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public Integer getNotificationid() {
        return notificationid;
    }

    public void setNotificationid(Integer notificationid) {
        this.notificationid = notificationid;
    }

    public Integer getIdnegotations() {
        return idnegotations;
    }

    public void setIdnegotations(Integer idnegotations) {
        this.idnegotations = idnegotations;
    }

    public Integer getAdvance() {
        return advance;
    }

    public void setAdvance(Integer advance) {
        this.advance = advance;
    }

    public String getRespondedOn() {
        return respondedOn;
    }

    public void setRespondedOn(String respondedOn) {
        this.respondedOn = respondedOn;
    }

    /*

    idnegotations int(11) AI PK
    customercontact varchar(15)
    merchantid varchar(128)
    geohash varchar(6)
    minamount int(4)
    maxamount int(11)
    DiscountExpectation int(3)
    ShoppingProbableDates varchar(45)
    description varchar(512)
    delivered int(1)
    adnotification int(11)
     */
    @SerializedName("customercontact")
    public String customercontact;

    @SerializedName("merchantid")
    public String merchantid;

    @SerializedName("geohash")
    public String geohash;

    @SerializedName("minamount")
    public Integer minamount;

    @SerializedName("maxamount")
    public Integer maxamount;

    @SerializedName("DiscountExpectation")
    public Integer DiscountExpectation;

    @SerializedName("ShoppingProbableDates")
    public String ShoppingProbableDates;

    @SerializedName("description")
    public String description;

    @SerializedName("delieverd")
    public Integer delieverd;

    @SerializedName("response")
    public Integer response;

    @SerializedName("notificationid")
    public Integer notificationid;

    @SerializedName("idnegotations")
    public Integer idnegotations;

    @SerializedName("advance")
    public Integer advance;

    @SerializedName("respondedOn")
    public String respondedOn;

    @SerializedName("imgurl")
    public String imgurl;

    @SerializedName("adObj")
    public AdPayLoad adObj;


}

