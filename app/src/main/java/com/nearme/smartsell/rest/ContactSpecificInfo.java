package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

public class ContactSpecificInfo {

    @SerializedName("Id")
    public Integer Id;
    @SerializedName("merchantreceiptid")
    public String merchantreceiptid;
    @SerializedName("imgurl")
    public String imgURL;
    @SerializedName("type")
    public Integer type;
    @SerializedName("merchantname")
    public String merchantname;
    @SerializedName("date")
    public String date;

    public String getimgurl() {
        return imgURL;
    }

    public void setimgurl(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getMerchantName() {
        return merchantname;
    }

    public void setMerchantName(String merchantName) {
        this.merchantname = merchantName;
    }

    public String getMerchantReceiptId() {
        return merchantreceiptid;
    }

    public void setMerchantReceiptId(String merchantreceiptid) {
        this.merchantreceiptid = merchantreceiptid;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
