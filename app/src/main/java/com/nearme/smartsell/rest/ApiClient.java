package com.nearme.smartsell.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ApiClient {

//    public static String BASE_URL = "http://3.138.94.77:5556/";
    public static String BASE_URL = "http://18.218.226.33:5556/";
//    public static String BASE_URL = "http://18.116.27.4:5556/";
    //    public static String BASE_URL = "http://3.142.173.148:5556/";
    //   public static final String BASE_URL = "http://35.196.161.59:5557/";
    //   public static final String BASE_URL = "http://34.73.19.99:5556/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
