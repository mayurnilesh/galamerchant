package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

public class LastReceivedAdStruct {
    public List<String> getGeoHash() {
        return geoHash;
    }

    public void setGeoHash(List<String> geoHash) {
        this.geoHash = geoHash;
    }

    public int getLastReceivedAdId() {
        return lastReceivedAdId;
    }

    public void setLastReceivedAdId(int lastReceivedAdId) {
        this.lastReceivedAdId = lastReceivedAdId;
    }

    @SerializedName("geoHash")
    public List<String> geoHash;

    @SerializedName("lastReceivedAdId")
    public int lastReceivedAdId;

    @SerializedName("Category")
    public Integer Category;
}
