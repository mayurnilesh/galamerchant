package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

public class counteremppayload {
    @SerializedName("username")
    public String username;
    @SerializedName("password")
    public String password;

    public boolean status = false;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public counteremppayload(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
