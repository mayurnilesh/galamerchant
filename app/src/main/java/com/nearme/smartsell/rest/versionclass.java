package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gopi.komanduri on 21/08/18.
 */

public class versionclass {
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @SerializedName("version")
    public Integer version=0;

    @Override
    public String toString() {
        return "version is "+version;
    }
}
