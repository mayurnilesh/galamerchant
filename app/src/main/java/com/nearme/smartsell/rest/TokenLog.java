package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

public class TokenLog {
    @SerializedName("token")
    public Integer token;
    @SerializedName("counter")
    public Integer counter;
    @SerializedName("timeServed")
    public Double timeServed;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;

    public String getstartTime() {
        return startTime;
    }

    public void setstartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getendTime() {
        return endTime;
    }

    public void setendTime(String endTime) {
        this.endTime = endTime;
    }

    public Double gettimeServed() {
        return timeServed;
    }

    public void settimeServed(Double timeServed) {
        this.timeServed = timeServed;
    }

    public Integer gettoken() {
        return token;
    }

    public void settoken(Integer token) {
        this.token = token;
    }

    public Integer getcounter() {
        return counter;
    }

    public void setcounter(Integer counter) {
        this.counter = counter;
    }
}
