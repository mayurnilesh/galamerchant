package com.nearme.smartsell.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gopi.komanduri on 14/11/18.
 */

public class counterandhelper {
    public List<counteremppayload> getCounterslist() {
        return counterslist;
    }

    public void setCounterslist(List<counteremppayload> counterslist) {
        this.counterslist = counterslist;
    }

    public List<counteremppayload> getHelperslist() {
        return helperslist;
    }

    public void setHelperslist(List<counteremppayload> helperslist) {
        this.helperslist = helperslist;
    }

    @SerializedName("counterslist")
    List<counteremppayload> counterslist;
    @SerializedName("helperslist")
    List<counteremppayload> helperslist;
}
