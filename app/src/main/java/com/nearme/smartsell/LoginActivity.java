package com.nearme.smartsell;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.wifi.hotspot2.pps.Credential;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cloudinary.api.exceptions.ApiException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.nearme.smartsell.db.userdetails;
import com.nearme.smartsell.location.LocationListener;
import com.nearme.smartsell.rest.ApiClient;
import com.nearme.smartsell.rest.ApiInterface;
import com.nearme.smartsell.rest.MerchantPayload;
import com.nearme.smartsell.startup.CategorySelectionActivity;
import com.nearme.smartsell.startup.IdVerification;
import com.nearme.smartsell.startup.ManageSlotsActivity;
import com.nearme.smartsell.startup.PhoneActivity;
import com.nearme.smartsell.startup.WelcomeActivity;
import com.nearme.smartsell.utility.ConnectionDetector;
import com.nearme.smartsell.utility.PrefManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener /*implements GoogleApiClient.OnConnectionFailedListener*/ {

    EditText userNameTxt, PasswordTxt;
    Button RegBtn;
    TextView loginBtn;
    ApiInterface apiService;
    private PrefManager prefManager;
    Gson gson;
    Context curContext;
    Integer loginRes;
    boolean isMerchantManger = true;
    Integer mer_role = 0;
    CheckBox rememberme;
    TextView frgtCheckBox;
    AlertDialog.Builder builder;
    Activity curActivity;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_t);
        curContext = this.getApplicationContext();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        loginBtn = findViewById(R.id.signin);
        RegBtn = findViewById(R.id.signup);
        pb = findViewById(R.id.pb);
        loginBtn.setOnClickListener(this);
        RegBtn.setOnClickListener(this);
        userNameTxt = findViewById(R.id.email);
        PasswordTxt = findViewById(R.id.password);
        gson = new Gson();
        builder = new AlertDialog.Builder(this);
        prefManager = new PrefManager(this);
        rememberme = findViewById(R.id.rememberme);
        frgtCheckBox = findViewById(R.id.frgtCheckBox);
        curActivity = this;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
        if (!pref.getString("username", "").equalsIgnoreCase("")) {
            userNameTxt.setText(pref.getString("username", ""));
            PasswordTxt.setText(pref.getString("password", ""));
        }

        if (checkInternetOrGps())
            this.startService(new Intent(getApplicationContext(), LocationListener.class));

        frgtCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, IdVerification.class));
            }
        });


    }


    public boolean checkInternetOrGps() {

        boolean isInternetandGPSStatusOK = false;
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!ConnectionDetector.isConnectingToInternet(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("Please Check your Internet Connection");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            if (!statusOfGPS) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle("Alert");
                builder.setMessage("Please Enable GPS");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsOptionsIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(gpsOptionsIntent);
                    }
                });
                builder.show();

            } else {
                isInternetandGPSStatusOK = true;
            }
        }
        return isInternetandGPSStatusOK;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signin:
                if (!userNameTxt.getText().toString().equals("") && !PasswordTxt.getText().toString().equals("")) {
                    pb.setVisibility(View.VISIBLE);
                    //Auth with firebase
                    Call<String> call = apiService.loginresponse(userNameTxt.getText().toString(), String.valueOf(PasswordTxt.getText()));

                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            try {
                                final String[] counterArr = userNameTxt.getText().toString().trim().split("_").clone();
                                loginRes = Integer.valueOf(response.body().toString());

                                if (loginRes != -1) {
                                    String uname = "+91";
                                    try {
//                                            (0/91)?[7-9][0-9]{9}
                                        Pattern p = Pattern.compile("[7-9][0-9]{9}");
                                        Pattern p1 = Pattern.compile("(0/91)?[7-9][0-9]{9}");
                                        Matcher m = p.matcher(userNameTxt.getText().toString());
                                        Matcher m1 = p1.matcher(userNameTxt.getText().toString());
                                        boolean isvalidnum = (m.find() && m.group().equals(userNameTxt.getText().toString()));
                                        isvalidnum |= (m1.find() && m1.group().equals(userNameTxt.getText().toString()));

                                        if (userNameTxt.getText().toString().length() == 10 && isvalidnum) {
                                            StringBuffer sb = new StringBuffer(uname);
                                            sb.append(userNameTxt.getText().toString());
//                                        uname+=userNameTxt.getText().toString();
                                            uname = sb.toString();
                                        } else if (!isvalidnum) {
                                            uname = counterArr[0] + '_' + counterArr[1];
                                            if (counterArr.length > 2)
                                                isMerchantManger = false;
                                        }
                                    } catch (Exception ex) {
                                        pb.setVisibility(View.GONE);
                                    }
                                    Call<String> getMerchantDetailscall = apiService.getMerchantDetails(uname);

                                    getMerchantDetailscall.enqueue(new Callback<String>() {
                                        @Override
                                        public void onResponse(Call<String> getMerchantDetailscall, Response<String> response) {
                                            try {
                                                pb.setVisibility(View.GONE);
                                                MerchantPayload mp = gson.fromJson(response.body(), MerchantPayload.class);
//                                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                                                    if (user == null) {
                                                // Get auth credentials from the user for re-authentication. The example below shows
                                                // email and password credentials but there are multiple possible providers,
                                                // such as GoogleAuthProvider or FacebookAuthProvider.
                                                AuthCredential credential = EmailAuthProvider
                                                        .getCredential(mp.merchantPhn + "@smartsell.com", mp.merchantPhn);

                                                FirebaseAuth.getInstance().signInWithCredential(credential)
                                                        .addOnCompleteListener(curActivity, new OnCompleteListener<AuthResult>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                                if (task.isSuccessful()) {
                                                                    //Got user back

                                                                }
                                                            }
                                                        });
//                                                    }
                                                if (mp.role != null) {
                                                    mer_role = mp.role;
                                                    if (!isMerchantManger)
                                                        mer_role = loginRes;
                                                }
                                                userdetails.deleteAll(userdetails.class);
                                                final userdetails obj = new userdetails(mp.merchantPhn, mp.merchantName,
                                                        mp.imgurl, mp.shopNo, "", mp.locality, mp.area, mp.landMark,
                                                        mp.city, mp.state, String.valueOf(prefManager.getSelectedCategory()), mp.geoHash, mp.password, mp.role);
                                                obj.merchantId = mp.merchantId;
                                                obj.save();
                                                if (mp.merchantId != null) {
                                                    switch (loginRes) {
                                                        //for 0 and 1 case call getMerchantDetails service and fill details
                                                        case 1://store if 0
                                                        case 2:
                                                            prefManager.setCounterNumber(counterArr[counterArr.length - 1]);

                                                            String merID = counterArr[0] + "_" + counterArr[1];
                                                            prefManager.setMerchantID(merID);
                                                            prefManager.setLoginType(mer_role.toString());

                                                            startActivity(new Intent(LoginActivity.this, ManageSlotsActivity.class));
                                                            break;
                                                        case 3:
                                                            String mercID = counterArr[0] + "_" + counterArr[1];
                                                            prefManager.setMerchantID(mercID);
                                                            prefManager.setLoginType(mer_role.toString());

                                                            startActivity(new Intent(LoginActivity.this, ManageSlotsActivity.class));
                                                            break;
                                                        case 0:
                                                            prefManager.setCounterNumber("-1");
                                                            prefManager.setMerchantID(mp.merchantId);
                                                            prefManager.setLoginType(mer_role.toString());
                                                            SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                                            SharedPreferences.Editor editor = pref.edit();
                                                            if (rememberme.isChecked()) {
                                                                editor.putString("username", userNameTxt.getText().toString());
                                                                editor.putString("password", PasswordTxt.getText().toString());
                                                                editor.apply();
                                                            } else {
                                                                editor.putString("username", "");
                                                                editor.putString("password", "");
                                                                editor.apply();
                                                            }
                                                            startActivity(new Intent(LoginActivity.this, ManageSlotsActivity.class));
                                                            break;
                                                        case -1:
                                                            PasswordTxt.setText("");
                                                            Toast.makeText(curContext, "Merchant ID or Password doesn't match. Please Try Again", Toast.LENGTH_LONG).show();
                                                            break;
                                                    }
                                                }
                                                if (!rememberme.isChecked()) {
                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("username", "");
                                                    editor.putString("password", "");
                                                    userNameTxt.setText(pref.getString("username", ""));
                                                    PasswordTxt.setText(pref.getString("password", ""));
                                                    editor.apply();
                                                }
                                            } catch (Exception ex) {
                                                Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
                                                if (!rememberme.isChecked()) {
                                                    pb.setVisibility(View.GONE);
                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("username", "");
                                                    editor.putString("password", "");
                                                    userNameTxt.setText(pref.getString("username", ""));
                                                    PasswordTxt.setText(pref.getString("password", ""));
                                                    editor.apply();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<String> getMerchantDetailscall, Throwable t) {
                                            Toast.makeText(curContext, "No Details Found. Please Try Again", Toast.LENGTH_LONG).show();
                                            if (!rememberme.isChecked()) {
                                                SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.putString("username", "");
                                                editor.putString("password", "");
                                                userNameTxt.setText(pref.getString("username", ""));
                                                PasswordTxt.setText(pref.getString("password", ""));
                                                editor.apply();
                                            }
                                        }
                                    });
                                } else {
                                    pb.setVisibility(View.GONE);
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                    SharedPreferences.Editor editor = pref.edit();
//                                    Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
                                    builder.setMessage("Wrong Password! If not Sign Up First").setTitle("Login Failure");
                                    AlertDialog alert = builder.create();
                                    //Setting the title manually
                                    alert.setTitle("Login Failure");
                                    alert.show();
                                    if (!rememberme.isChecked()) {

                                        editor.putString("username", "");
                                        editor.putString("password", "");
                                        editor.apply();
                                    }
                                    userNameTxt.setText(pref.getString("username", ""));
                                    PasswordTxt.setText(pref.getString("password", ""));
                                }

                            } catch (Exception ex) {
                                pb.setVisibility(View.GONE);
                                if (!rememberme.isChecked()) {
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("galamerchant", 0); // 0 - for private mode
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("username", "");
                                    editor.putString("password", "");
                                    userNameTxt.setText(pref.getString("username", ""));
                                    PasswordTxt.setText(pref.getString("password", ""));
                                    editor.apply();
                                }
                            }
                        }


                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            pb.setVisibility(View.GONE);
                            Toast.makeText(curContext, "Login Failed. Please Try Again", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    if (userNameTxt.getText().toString().equals(""))
                        userNameTxt.setError("Please enter Valid Username");
                    if (PasswordTxt.getText().toString().equals(""))
                        PasswordTxt.setError("Please enter Valid Password");
                }
                break;
            case R.id.signup:
                userdetails.deleteAll(userdetails.class);
                prefManager.setPostedToMain(false);
                prefManager.setMerchantID(null);
                startActivity(new Intent(LoginActivity.this, CategorySelectionActivity.class));
                break;
        }
    }
}

