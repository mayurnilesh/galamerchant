package com.nearme.smartsell.neversleep;

import android.content.Context;
import android.content.Intent;
import androidx.legacy.content.WakefulBroadcastReceiver;

import com.nearme.smartsell.location.MyLocationListener;

public class WakefulReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, MyLocationListener.class);
        startWakefulService(context,service);
    }
}
