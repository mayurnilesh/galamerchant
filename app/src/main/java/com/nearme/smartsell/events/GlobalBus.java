package com.nearme.smartsell.events;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by gopikomanduri on 05/05/18.
 */

public class GlobalBus {
    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}