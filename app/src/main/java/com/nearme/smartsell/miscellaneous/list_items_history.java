package com.nearme.smartsell.miscellaneous;

import com.nearme.smartsell.utility.DateTime;

/**
 * Created by gopi.komanduri on 04/08/18.
 */

public class list_items_history {
    public String imageUrl;
    public String qrImageUrl;
    public String validTill;
    public String shopName;
    public String offerCode;
    public String location;
    public String offerDescription;
    public int notificationid;
    public String validFrom;
    public int ViewsCount = 0;
    public int deliveredCount = 0;
    public int usageCount = 0;


    public list_items_history(String imageUrl, String qrImageUrl, String validTill, String shopName,
                              String offerCode, String location,
                              String offerDescription, int notificationid,
                              int viewsCount, int deliveredCount, int usageCount) {
        this.imageUrl = imageUrl;
        this.validTill = validTill;
        this.shopName = shopName;
        this.offerCode = offerCode;
        this.location = location;
        this.offerDescription = offerDescription;
        this.notificationid = notificationid;
        this.qrImageUrl = qrImageUrl;
        validFrom = DateTime.getTodaysDate();
        this.ViewsCount = viewsCount;
        this.deliveredCount = deliveredCount;
        this.usageCount = usageCount;
    }
}
